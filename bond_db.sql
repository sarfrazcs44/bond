-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 05, 2021 at 09:11 AM
-- Server version: 5.7.33
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qrschopf_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `ActivityID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `SubTitle` varchar(255) NOT NULL,
  `SubTitleAr` varchar(255) NOT NULL,
  `ActivityImage` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`ActivityID`, `SortOrder`, `Title`, `TitleAr`, `SubTitle`, `SubTitleAr`, `ActivityImage`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(4, 0, 'Wedding', 'Wedding', 'Invitation', 'Invitation', 'uploads/images/activities/4899307333520210325070917Group_3130@3x.png', 0, 1, '2021-03-25 07:17:09', '2021-03-25 07:17:09', 1, 1),
(5, 1, 'Social', 'Social', 'Account Links', 'Account Links', 'uploads/images/activities/8992145823520210325074917Group_3134@3x.png', 0, 1, '2021-03-25 07:17:49', '2021-03-25 07:17:49', 1, 1),
(6, 2, 'Personal', 'Personal', 'VCard', 'VCard', 'uploads/images/activities/5533667257520210325071118Group_3150@3x.png', 0, 1, '2021-03-25 07:18:11', '2021-03-25 07:18:11', 1, 1),
(7, 3, 'SMS', 'SMS', '', '', 'uploads/images/activities/8458369635520210325073618Group_3135@3x.png', 0, 1, '2021-03-25 07:18:36', '2021-03-25 07:18:36', 1, 1),
(8, 4, 'Store', 'Store', 'Links', 'Links', 'uploads/images/activities/1904874904120210325070819Group_3243@3x.png', 0, 1, '2021-03-25 07:19:08', '2021-03-25 07:19:08', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `activity_fixtures`
--

CREATE TABLE `activity_fixtures` (
  `ActivityFixtureID` int(11) NOT NULL,
  `ActivityID` int(11) NOT NULL,
  `FixtureTitle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `BookingID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `MerchantActivityID` int(11) NOT NULL,
  `CheckInTime` varchar(255) NOT NULL,
  `CheckOutTime` varchar(255) NOT NULL,
  `Fixture` varchar(255) NOT NULL COMMENT 'comma seperated',
  `FixturePrice` text NOT NULL,
  `Equipment` varchar(255) NOT NULL,
  `EquipmentQuantity` varchar(255) NOT NULL,
  `Guest` int(11) NOT NULL,
  `Tax` varchar(250) NOT NULL,
  `Total` varchar(255) NOT NULL,
  `AdditionalCharges` varchar(255) NOT NULL,
  `GrandTotal` varchar(255) NOT NULL,
  `EquipmentPrice` text NOT NULL,
  `BookingStatus` enum('Pending','Complete','Upcoming','') NOT NULL DEFAULT 'Pending',
  `UserID` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`BookingID`, `SortOrder`, `Title`, `MerchantActivityID`, `CheckInTime`, `CheckOutTime`, `Fixture`, `FixturePrice`, `Equipment`, `EquipmentQuantity`, `Guest`, `Tax`, `Total`, `AdditionalCharges`, `GrandTotal`, `EquipmentPrice`, `BookingStatus`, `UserID`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 'abc', 1, '', '', '21,23,25', '26,29,30', '3,4,5', '7,9,10', 0, '250', '15000', '', '1250000', '120,2,2', '', '13', 0, 1, '2021-02-01 04:19:53', '2021-02-01 04:19:53', 4, 4),
(2, 0, 'abc', 1, '', '', '21,23,25', '26,29,30', '3,4,5', '7,9,10', 0, '250', '15000', '', '1250000', '120,2,2', '', '14', 0, 1, '2021-02-01 04:20:45', '2021-02-01 04:20:45', 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `CityID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `DescriptionAr` text NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `Email_templateID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `SubjectAr` varchar(255) NOT NULL,
  `DescriptionAr` text NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`Email_templateID`, `Title`, `Subject`, `Description`, `TitleAr`, `SubjectAr`, `DescriptionAr`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'Testdsafdasf', 'dsfdasf', '<p>dfadsfasfdfadsfa</p>', 'sdfasfdsfdasfdas', 'dsafasdfdsfdasf', '<p>dsfadsfsafdsfada</p>', '', 0, 0, 1, '2020-12-24 07:05:53', '2020-12-24 07:06:42', 1, 1),
(2, 'scdc', 'dsfadsf', '<p>safadsf</p>', 'dsfdsf', 'dsfadsfd', '<p>fadsfas</p>', '', 1, 0, 1, '2020-12-24 07:31:49', '2020-12-24 07:31:49', 1, 1),
(4, 'Forgot Password', 'ForgotPassowrd', '<p>Hi,</p>\r\n{{link}}', 'dsfdsf', 'dsfadsfd', '<p>fadsfas</p>', '', 1, 0, 1, '2020-12-24 07:31:49', '2020-12-24 07:31:49', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `equipments`
--

CREATE TABLE `equipments` (
  `EquipmentID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `DescriptionAr` text NOT NULL,
  `MinPrice` varchar(100) NOT NULL,
  `MaxPrice` varchar(100) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equipments`
--

INSERT INTO `equipments` (`EquipmentID`, `SortOrder`, `Title`, `TitleAr`, `Description`, `DescriptionAr`, `MinPrice`, `MaxPrice`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 'Fisher', 'Fisher', '', '', '20', '50', 0, 1, '2021-01-19 07:36:56', '2021-01-19 07:38:31', 1, 1),
(2, 1, 'Jacket', 'Jacket', '', '', '50', '200', 0, 1, '2021-01-19 07:39:06', '2021-01-19 07:39:06', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `FeedbackID` int(11) NOT NULL,
  `OrderNumber` varchar(255) NOT NULL COMMENT 'optional field',
  `FullName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `MobileNo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Message` text CHARACTER SET utf8 NOT NULL,
  `CreatedAt` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`FeedbackID`, `OrderNumber`, `FullName`, `Email`, `MobileNo`, `Message`, `CreatedAt`) VALUES
(1, '', 'ahsan', 'ahsan@zynq.net', '+923075172531', 'chfgu', '1571654625'),
(2, '', 'ahsan', 'ahsan@zynq.net', '+923075172531', 'chfgu', '1571654630'),
(3, '00016', 'Muhammad Salman', 'm.salman@zynq.net', 'm.salman@zynq.net', 'Sadasd', '1574423296');

-- --------------------------------------------------------

--
-- Table structure for table `fleets`
--

CREATE TABLE `fleets` (
  `FleetID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `DescriptionAr` text NOT NULL,
  `FleetImage` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fleets`
--

INSERT INTO `fleets` (`FleetID`, `SortOrder`, `Title`, `TitleAr`, `Description`, `DescriptionAr`, `FleetImage`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 'First Fleet', 'First Fleet', '', '', '', 0, 1, '2021-02-19 10:46:09', '2021-02-19 10:46:09', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invitations`
--

CREATE TABLE `invitations` (
  `InvitationID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `DescriptionAr` text NOT NULL,
  `Logo` varchar(255) NOT NULL,
  `TextColor` varchar(255) NOT NULL,
  `HostName` varchar(255) NOT NULL,
  `ContactInfo` varchar(255) NOT NULL,
  `YoutubeLink` varchar(255) NOT NULL,
  `GoogleLink` varchar(255) NOT NULL,
  `Background` varchar(255) NOT NULL,
  `IsBackgroundColor` tinyint(4) NOT NULL,
  `IsBackgroundImage` tinyint(4) NOT NULL,
  `IsPrivate` tinyint(4) NOT NULL,
  `PrivateCharges` float NOT NULL,
  `TotalAmount` float NOT NULL,
  `QRUrl` varchar(500) NOT NULL,
  `QRID` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` varchar(255) NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invitations`
--

INSERT INTO `invitations` (`InvitationID`, `SortOrder`, `UserID`, `Title`, `TitleAr`, `Description`, `DescriptionAr`, `Logo`, `TextColor`, `HostName`, `ContactInfo`, `YoutubeLink`, `GoogleLink`, `Background`, `IsBackgroundColor`, `IsBackgroundImage`, `IsPrivate`, `PrivateCharges`, `TotalAmount`, `QRUrl`, `QRID`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 31, 'Title', '', 'This is dummy test', '', 'uploads/invitations/432556678720210329111608Logo.jpg', '', 'Host', '+966123123', 'www.google.com', 'www.google.com', 'uploads/invitations/5875607629120210329111608Background.jpg', 0, 0, 1, 2, 200, 'http://s3.amazonaws.com/v2img/71a2c6b9-12e1-2528-32cb-00000dde8919.jpeg', '', 0, 0, '1617016091', '2021-03-29 11:08:00', 31, 31),
(2, 0, 31, 'new title', '', 'This is dummy test', '', 'uploads/invitations/6364349280720210329110218Logo.jpg', '', 'Host', '+966123123', 'www.google.com', 'www.google.com', 'uploads/invitations/6666391955620210329110218Background.jpg', 0, 0, 1, 2, 200, 'http://s3.amazonaws.com/v2img/572bccf4-e329-8cc9-ba5c-000061d6dc72.jpeg', '', 0, 0, '1617016676', '2021-03-29 11:18:00', 31, 31),
(3, 0, 31, 'QR with ID', '', 'This is dummy test', '', 'uploads/invitations/3697766481520210329114720Logo.jpg', '', 'Host', '+966123123', 'www.google.com', 'www.google.com', 'uploads/invitations/1327597364820210329114720Background.jpg', 0, 0, 1, 2, 200, 'http://s3.amazonaws.com/v2img/37a8e324-9da7-b249-b39e-000026903ab7.jpeg', '18546992', 0, 0, '1617016841', '2021-03-29 11:21:00', 31, 31),
(4, 0, 31, 'Weding', '', 'Wedding Ceremony', '', 'uploads/invitations/6273157540820210329110731logo.jpg', '', 'Frank', '03312343977', 'Audio mp3 or Youtube Link (Optional)', 'Google Map Location Share Link', 'uploads/invitations/1690666603020210329110731bacground.jpg', 0, 1, 0, 20, 200, '', '', 0, 0, '', '0000-00-00 00:00:00', 31, 0),
(5, 0, 31, 'Birthday', '', 'Birthday Celebrations', '', 'uploads/invitations/5291218446120210329110139logo.jpg', '', 'Ngolo', '03312343877', 'Audio mp3 or Youtube Link (Optional)', 'Google Map Location Share Link', 'uploads/invitations/3619523467520210329110139bacground.jpg', 0, 1, 0, 20, 200, 'http://s3.amazonaws.com/v2img/2819bc22-61f0-5c48-32ad-000053b97fd0.png', '18546996', 0, 0, '1617017983', '2021-03-29 11:39:00', 31, 31),
(6, 0, 35, 'ttt', '', 're', '', 'uploads/invitations/156075117920210403071624logo.jpg', '', 'z', '00000', 'Audio mp3 or Youtube Link (Optional)', 'Google Map Location Share Link', 'uploads/invitations/3576986998820210403071624bacground.jpg', 0, 1, 0, 20, 200, 'http://s3.amazonaws.com/v2img/65f18f4a-95d0-e328-62a8-000077c60b9b.png', '18548196', 0, 0, '1617477877', '2021-04-03 19:24:00', 35, 35),
(7, 0, 34, 'Wedding of Sulaiman', '', 'Wedding invit of Taha from abc to def in marriot', '', 'uploads/invitations/9008396613820210403071924Logo.jpg', '', 'Taha Haider invites you wedding of his son', '+966 55 1234567', 'www.google.com', 'www.google.com', 'uploads/invitations/560554148120210403071924Background.jpg', 0, 0, 1, 2, 200, 'http://s3.amazonaws.com/v2img/13f159c2-8ff0-a268-2ace-0000455cb5d0.jpeg', '18548195', 0, 0, '1617477831', '2021-04-03 19:24:00', 34, 34);

-- --------------------------------------------------------

--
-- Table structure for table `merchantactivities`
--

CREATE TABLE `merchantactivities` (
  `MerchantactivityID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `ActivityID` int(11) NOT NULL,
  `Equipment` text NOT NULL,
  `MaxCapacity` int(11) NOT NULL,
  `Description` text NOT NULL,
  `DescriptionAr` text NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `EquipmentPrice` text NOT NULL,
  `Fixture` varchar(255) NOT NULL,
  `FixturePrice` text NOT NULL,
  `UserID` int(11) NOT NULL,
  `Merchant_fleetID` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `merchantactivities`
--

INSERT INTO `merchantactivities` (`MerchantactivityID`, `SortOrder`, `Title`, `TitleAr`, `ActivityID`, `Equipment`, `MaxCapacity`, `Description`, `DescriptionAr`, `Hide`, `IsActive`, `CreatedBy`, `EquipmentPrice`, `Fixture`, `FixturePrice`, `UserID`, `Merchant_fleetID`, `UpdatedBy`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 0, 'bootcamp1', 'bootcamp1', 1, '3,4,5', 0, '33', 'dfadfasfadsfadsf', 0, 0, 0, '200,400', 'sdk', '33', 13, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 0, 'bootcamp1', 'bootcamp1', 45, '3,24,52', 0, '33s', 'dfadfasfadsfadsf', 0, 1, 4, '200,300', 'sdks', '33s', 4, 0, 4, '2021-01-26 12:35:42', '2021-01-26 12:35:42'),
(3, 0, 'bootcamp1', 'bootcamp1', 45, '3,24,52', 0, '33s', 'dfadfasfadsfadsf', 0, 1, 4, '200,300', 'sdks', '33s', 4, 0, 4, '2021-01-26 12:36:22', '2021-01-26 12:36:22'),
(4, 0, 'bootcamp1', 'bootcamp1', 45, '3,24,52', 0, '33s', 'dfadfasfadsfadsf', 0, 1, 4, '200,300', 'sdks', '33s', 13, 0, 4, '2021-01-26 12:36:37', '2021-01-26 12:36:37'),
(5, 0, '', '', 1, '2', 0, '', '', 0, 1, 5, '20', 'Divine Mega 2, Ghazi Rd, Gulshan e Ali Colony, Lahore, Punjab, Pakistan\n,Plot 1703, Sector T DHA Phase 8, Lahore, Punjab, Pakistan\n', '200,450', 13, 0, 5, '2021-02-02 06:55:44', '2021-02-02 06:55:44'),
(6, 0, '', '', 2, '2', 2, '', '', 0, 1, 14, '20', 'Plot 420, Sector T DHA Phase 8, Lahore, Punjab, Pakistan\n', '200', 14, 0, 14, '2021-02-08 11:42:42', '2021-02-08 11:42:42'),
(7, 0, '', '', 1, '2,1', 20, '', '', 0, 1, 19, '20,30', 'Nepal@Maldives@canada@Bangladesh@India@Pakistan', '600,500,400,300,200,100', 19, 0, 19, '2021-02-15 12:26:21', '2021-02-15 12:26:21'),
(8, 0, '', '', 3, 'Hunting', 80, '', '', 0, 1, 19, '80', 'Maldives', '100', 19, 0, 1, '2021-02-15 13:00:13', '2021-02-17 10:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `merchant_fleets`
--

CREATE TABLE `merchant_fleets` (
  `Merchant_fleetID` int(11) NOT NULL,
  `FleetID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `MaximumCapacity` varchar(255) NOT NULL,
  `LicenseNo` varchar(255) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mobileadses`
--

CREATE TABLE `mobileadses` (
  `MobileadsID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `AdsImage` text NOT NULL,
  `Description` text NOT NULL,
  `DescriptionAr` text NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mobileadses`
--

INSERT INTO `mobileadses` (`MobileadsID`, `SortOrder`, `Title`, `TitleAr`, `AdsImage`, `Description`, `DescriptionAr`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(2, 0, 'Ads', 'Ads', 'uploads/images/adsimages/69155519652202102081118492021-02-08.jpg', 'ads here', 'ads here', 0, 1, '2021-02-08 06:33:00', '2021-02-08 11:49:18', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `ModuleID` int(11) NOT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `IconClass` varchar(255) NOT NULL,
  `Slug` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreateTable` tinyint(4) NOT NULL,
  `CreateModel` tinyint(4) NOT NULL,
  `CreateView` tinyint(4) NOT NULL,
  `CreateController` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`ModuleID`, `ParentID`, `Title`, `TitleAr`, `IconClass`, `Slug`, `SortOrder`, `Hide`, `IsActive`, `CreateTable`, `CreateModel`, `CreateView`, `CreateController`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 'Miscellaneous', '', 'settings', '#', 12, 0, 1, 0, 0, 0, 0, '2018-05-09 00:00:00', '2018-11-30 10:32:35', 1, 1),
(18, 1, 'Module', '', ' mdi mdi-view-module', 'module', 1, 0, 1, 0, 0, 0, 0, '2018-04-19 15:22:06', '2018-04-19 15:22:06', 1, 1),
(22, 1, 'Roles', '', 'mdi mdi-yeast', 'role', 5, 0, 1, 0, 0, 0, 0, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(23, 0, 'Users', '', 'face', 'user', 6, 0, 1, 0, 0, 0, 0, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(36, 1, 'Site Setting', '', 'settings', 'Site_setting', 3, 0, 1, 0, 0, 0, 0, '2018-06-21 20:24:33', '2018-06-21 20:24:33', 1, 1),
(39, 1, 'Email Templates', '', ' mdi mdi-email', 'email_template', 4, 0, 1, 0, 0, 0, 0, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(63, 0, 'Feedbacks', '', 'feedback', 'feedback', 10, 0, 1, 0, 0, 0, 0, '2019-04-30 10:17:16', '2019-04-30 10:17:16', 1, 1),
(64, 0, 'Activities', 'Activities', 'list', 'activity', 11, 0, 1, 1, 1, 1, 1, '2021-01-01 06:12:28', '2021-01-01 06:12:28', 1, 1),
(65, 0, 'Equipments', 'Equipments', 'build', 'equipment', 12, 1, 0, 1, 1, 1, 1, '2021-01-14 10:33:44', '2021-01-14 10:33:44', 1, 1),
(66, 0, 'Merchant Activity', 'weneed to change', 'list', 'merchantactivity', 13, 1, 1, 1, 1, 1, 1, '2021-01-26 07:52:45', '2021-01-26 07:52:45', 1, 1),
(67, 0, 'Booking', 'weneed to change', 'list', 'booking', 14, 1, 1, 1, 1, 1, 1, '2021-02-01 03:19:49', '2021-02-01 03:20:33', 1, 1),
(68, 0, 'Mobile Ads', '', 'list', 'mobileads', 15, 0, 1, 1, 1, 1, 1, '2021-02-08 05:57:31', '2021-02-08 05:57:31', 1, 1),
(69, 0, 'City', 'City', 'list', 'city', 16, 0, 1, 1, 1, 1, 1, '2021-02-19 06:53:09', '2021-02-19 06:53:09', 1, 1),
(70, 0, 'Fleet Type', 'Fleet Type', 'list', 'fleet', 17, 1, 1, 1, 1, 1, 1, '2021-02-19 10:11:45', '2021-02-19 10:11:45', 1, 1),
(71, 0, 'Merchant Fleets', 'Merchant Fleets', 'list', 'merchant_fleet', 18, 1, 1, 1, 1, 1, 1, '2021-02-25 05:05:12', '2021-02-25 05:05:12', 1, 1),
(72, 0, 'Invitations', 'Invitations', 'list', 'invitation', 19, 0, 1, 1, 1, 1, 1, '2021-03-25 11:18:48', '2021-03-25 11:18:48', 1, 1),
(73, 0, 'Price', 'Price', 'list', 'price', 20, 0, 1, 1, 1, 1, 1, '2021-04-02 10:32:38', '2021-04-02 10:32:38', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_rights`
--

CREATE TABLE `modules_rights` (
  `ModuleRightID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `CanView` tinyint(4) NOT NULL,
  `CanAdd` tinyint(4) NOT NULL,
  `CanEdit` tinyint(4) NOT NULL,
  `CanDelete` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_rights`
--

INSERT INTO `modules_rights` (`ModuleRightID`, `ModuleID`, `RoleID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(13, 18, 1, 1, 1, 1, 1, '2018-04-19 15:22:06', '2020-12-24 08:19:44', 1, 1),
(17, 18, 2, 1, 1, 1, 1, '2018-05-02 11:37:19', '2018-05-02 11:37:19', 1, 1),
(20, 22, 1, 1, 1, 1, 1, '2018-05-02 12:10:33', '2020-12-24 08:19:44', 1, 1),
(21, 22, 2, 1, 1, 1, 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(22, 23, 1, 1, 1, 1, 1, '2018-05-02 12:44:19', '2020-12-24 08:19:44', 1, 1),
(23, 23, 2, 1, 1, 1, 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(75, 1, 2, 1, 1, 1, 1, '2018-05-09 12:20:50', '2018-05-09 12:20:50', 1, 1),
(76, 1, 1, 1, 1, 1, 1, '2018-05-09 12:20:50', '2020-12-24 08:19:44', 1, 1),
(77, 36, 1, 1, 1, 1, 1, '2018-02-12 16:32:10', '2020-12-24 08:19:44', 1, 1),
(78, 36, 2, 1, 1, 1, 1, '2018-06-29 13:51:40', '2018-06-29 13:51:40', 1, 1),
(86, 39, 1, 1, 1, 1, 1, '2018-07-02 16:26:28', '2020-12-24 08:19:44', 1, 1),
(87, 39, 2, 1, 1, 1, 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(163, 1, 4, 1, 1, 1, 1, '2019-02-11 20:07:49', '2019-03-03 12:06:20', 1, 1),
(164, 18, 4, 1, 1, 1, 1, '2019-02-11 20:07:49', '2019-03-03 12:06:20', 1, 1),
(165, 22, 4, 1, 1, 1, 1, '2019-02-11 20:07:49', '2019-03-03 12:06:20', 1, 1),
(166, 23, 4, 1, 1, 1, 1, '2019-02-11 20:07:49', '2019-03-03 12:06:20', 1, 1),
(169, 36, 4, 1, 1, 1, 1, '2019-02-11 20:07:49', '2019-03-03 12:06:20', 1, 1),
(170, 39, 4, 1, 1, 1, 1, '2019-02-11 20:07:49', '2019-02-11 20:07:49', 1, 1),
(189, 1, 5, 0, 0, 0, 0, '2019-03-05 17:43:51', '2019-03-06 07:20:40', 1, 1),
(190, 18, 5, 0, 0, 0, 0, '2019-03-05 17:43:51', '2019-03-06 07:20:40', 1, 1),
(191, 22, 5, 0, 0, 0, 0, '2019-03-05 17:43:51', '2019-03-06 07:20:40', 1, 1),
(192, 23, 5, 0, 0, 0, 0, '2019-03-05 17:43:51', '2019-03-06 07:20:40', 1, 1),
(195, 36, 5, 0, 0, 0, 0, '2019-03-05 17:43:51', '2019-03-06 07:20:40', 1, 1),
(196, 39, 5, 1, 1, 1, 1, '2019-03-05 17:43:51', '2019-03-05 17:43:51', 1, 1),
(219, 63, 1, 1, 1, 1, 1, '2019-04-30 10:17:16', '2020-12-24 08:19:44', 1, 1),
(220, 63, 2, 1, 1, 1, 1, '2019-04-30 10:17:16', '2019-04-30 10:17:16', 1, 1),
(221, 64, 1, 1, 1, 1, 1, '2021-01-01 06:12:28', '2021-01-01 06:12:28', 1, 1),
(222, 64, 2, 1, 1, 1, 1, '2021-01-01 06:12:28', '2021-01-01 06:12:28', 1, 1),
(223, 65, 1, 1, 1, 1, 1, '2021-01-14 10:33:44', '2021-01-14 10:33:44', 1, 1),
(224, 65, 2, 1, 1, 1, 1, '2021-01-14 10:33:44', '2021-01-14 10:33:44', 1, 1),
(225, 65, 3, 0, 0, 0, 0, '2021-01-14 10:33:44', '2021-01-14 10:33:44', 1, 1),
(226, 66, 1, 1, 1, 1, 1, '2021-01-26 07:52:45', '2021-01-26 07:52:45', 1, 1),
(227, 66, 2, 1, 1, 1, 1, '2021-01-26 07:52:45', '2021-01-26 07:52:45', 1, 1),
(228, 66, 3, 0, 0, 0, 0, '2021-01-26 07:52:45', '2021-01-26 07:52:45', 1, 1),
(229, 67, 1, 1, 1, 1, 1, '2021-02-01 03:19:49', '2021-02-01 03:19:49', 1, 1),
(230, 67, 2, 1, 1, 1, 1, '2021-02-01 03:19:49', '2021-02-01 03:19:49', 1, 1),
(231, 67, 3, 0, 0, 0, 0, '2021-02-01 03:19:49', '2021-02-01 03:19:49', 1, 1),
(232, 68, 1, 1, 1, 1, 1, '2021-02-08 05:57:31', '2021-02-08 05:57:31', 1, 1),
(233, 68, 2, 1, 1, 1, 1, '2021-02-08 05:57:31', '2021-02-08 05:57:31', 1, 1),
(234, 68, 3, 0, 0, 0, 0, '2021-02-08 05:57:31', '2021-02-08 05:57:31', 1, 1),
(235, 69, 1, 1, 1, 1, 1, '2021-02-19 06:53:09', '2021-02-19 06:53:09', 1, 1),
(236, 69, 2, 1, 1, 1, 1, '2021-02-19 06:53:09', '2021-02-19 06:53:09', 1, 1),
(237, 69, 3, 0, 0, 0, 0, '2021-02-19 06:53:09', '2021-02-19 06:53:09', 1, 1),
(238, 70, 1, 1, 1, 1, 1, '2021-02-19 10:11:45', '2021-02-19 10:11:45', 1, 1),
(239, 70, 2, 1, 1, 1, 1, '2021-02-19 10:11:45', '2021-02-19 10:11:45', 1, 1),
(240, 70, 3, 0, 0, 0, 0, '2021-02-19 10:11:45', '2021-02-19 10:11:45', 1, 1),
(241, 71, 1, 1, 1, 1, 1, '2021-02-25 05:05:12', '2021-02-25 05:05:12', 1, 1),
(242, 71, 2, 1, 1, 1, 1, '2021-02-25 05:05:12', '2021-02-25 05:05:12', 1, 1),
(243, 71, 3, 0, 0, 0, 0, '2021-02-25 05:05:12', '2021-02-25 05:05:12', 1, 1),
(244, 72, 1, 1, 1, 1, 1, '2021-03-25 11:18:48', '2021-03-25 11:18:48', 1, 1),
(245, 72, 2, 1, 1, 1, 1, '2021-03-25 11:18:48', '2021-03-25 11:18:48', 1, 1),
(246, 72, 3, 0, 0, 0, 0, '2021-03-25 11:18:48', '2021-03-25 11:18:48', 1, 1),
(247, 73, 1, 1, 1, 1, 1, '2021-04-02 10:32:38', '2021-04-02 10:32:38', 1, 1),
(248, 73, 2, 1, 1, 1, 1, '2021-04-02 10:32:38', '2021-04-02 10:32:38', 1, 1),
(249, 73, 3, 0, 0, 0, 0, '2021-04-02 10:32:38', '2021-04-02 10:32:38', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `PageID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `DescriptionAr` text NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Contact` varchar(255) NOT NULL,
  `Whatsapp` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Linkedin` varchar(255) NOT NULL,
  `Facebook` varchar(255) NOT NULL,
  `Instagram` varchar(255) NOT NULL,
  `Twitter` varchar(255) NOT NULL,
  `Snapchat` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`PageID`, `Title`, `TitleAr`, `Description`, `DescriptionAr`, `Image`, `Contact`, `Whatsapp`, `Email`, `Linkedin`, `Facebook`, `Instagram`, `Twitter`, `Snapchat`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'Terms and condition', '', 'Terms and condition', '', '', '', '', '', '', '', '', '', '', 0, 0, 1, '2018-12-19 06:00:56', '2019-09-14 22:21:49', 1, 1),
(2, 'About Us', '', '', '', 'uploads/images/959993002720190914104921beef-burger-with-deep-fried-bacon-and-thousand-island-dressing-50247463.jpg', '', '', 'info@qr-in.com', 'schopfen', 'schopfen', 'schopfencorp', 'schopfen', '', 1, 0, 1, '2018-12-19 06:02:00', '2019-09-14 22:25:30', 1, 1),
(3, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, 0, 1, '2018-12-19 07:34:39', '2019-02-25 09:25:05', 1, 1),
(4, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, 0, 1, '2018-12-19 12:25:43', '2019-10-21 16:46:28', 1, 1),
(5, '', '', '', '', '', '', '', '', '', '', '', '', '', 4, 0, 1, '2018-12-19 12:27:22', '2019-10-21 16:46:47', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `PriceID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleAr` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `PriceType` int(11) NOT NULL DEFAULT '1',
  `IsPaid` tinyint(4) NOT NULL,
  `IsFree` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`PriceID`, `SortOrder`, `Title`, `TitleAr`, `Hide`, `PriceType`, `IsPaid`, `IsFree`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 'Template', 'Template', 0, 1, 1, 0, 1, '2021-04-02 10:46:28', '2021-04-02 10:46:28', 1, 1),
(2, 1, 'Wedding Card', 'Wedding Card', 0, 1, 1, 1, 1, '2021-04-02 10:48:31', '2021-04-02 10:48:31', 1, 1),
(3, 2, 'Social Media', 'Social Media', 0, 1, 1, 0, 1, '2021-04-02 10:48:51', '2021-04-02 10:48:51', 1, 1),
(4, 3, 'Reports', 'Reports', 0, 2, 1, 0, 1, '2021-04-02 10:51:42', '2021-04-02 10:51:52', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `RoleID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `IsActive` tinyint(4) NOT NULL,
  `CreatedBy` datetime NOT NULL,
  `UpdatedBy` datetime NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`RoleID`, `Title`, `SortOrder`, `Hide`, `IsActive`, `CreatedBy`, `UpdatedBy`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 'Admin', 0, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-04-03 00:00:00', '2020-12-24 07:17:52'),
(2, 'Merchant', 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-02 11:37:19', '2019-04-16 13:15:32'),
(3, 'Customer', 1, 0, 1, '2021-01-08 00:00:00', '2021-01-08 00:00:00', '2018-05-02 11:37:19', '2019-04-16 13:15:32');

-- --------------------------------------------------------

--
-- Table structure for table `site_images`
--

CREATE TABLE `site_images` (
  `SiteImageID` int(11) NOT NULL,
  `ImageType` varchar(50) NOT NULL,
  `FileID` int(11) NOT NULL,
  `ImageName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_images`
--

INSERT INTO `site_images` (`SiteImageID`, `ImageType`, `FileID`, `ImageName`) VALUES
(1, 'user_document', 9, 'uploads/users/5307339889720210203062248Screenshot_2021-01-27_at_11.25.53_AM.png'),
(2, 'user_document', 10, 'uploads/users/197023388420210203060251Screenshot_2021-01-27_at_11.25.53_AM.png'),
(3, 'user_license', 10, 'uploads/users/5219557218420210203060251Screenshot_2021-01-27_at_11.25.53_AM.png'),
(4, 'user_document', 12, 'uploads/users/9895511475520210203072545Wed_Feb_03_12:44:42_GMT+05:00_2021.jpg'),
(5, 'user_document', 12, 'uploads/users/2496988275520210203072545Wed_Feb_03_12:44:46_GMT+05:00_2021.jpg'),
(6, 'user_license', 12, 'uploads/users/7402881219820210203072545Wed_Feb_03_12:44:27_GMT+05:00_2021.jpg'),
(7, 'user_license', 12, 'uploads/users/2262919424720210203072545Wed_Feb_03_12:44:34_GMT+05:00_2021.jpg'),
(8, 'user_document', 20, 'uploads/users/6713455769020210217112404Wed_Feb_17_16:04:02_GMT+05:00_2021.jpg'),
(9, 'user_license', 20, 'uploads/users/6216004650320210217112404Wed_Feb_17_16:03:57_GMT+05:00_2021.jpg'),
(10, 'user_document', 21, 'uploads/users/8314570049720210217111636Wed_Feb_17_16:36:01_GMT+05:00_2021.jpg'),
(11, 'user_license', 21, 'uploads/users/1395131846720210217111636Wed_Feb_17_16:35:56_GMT+05:00_2021.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `SiteSettingID` int(11) NOT NULL,
  `SiteName` varchar(255) NOT NULL,
  `SiteImage` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Whatsapp` varchar(255) NOT NULL,
  `Skype` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `Fax` varchar(255) NOT NULL,
  `ProductDisclaimer` text NOT NULL,
  `PromoCodeDisclaimer` text NOT NULL,
  `OpenTime` varchar(255) NOT NULL,
  `CloseTime` varchar(255) NOT NULL,
  `FacebookUrl` varchar(255) NOT NULL,
  `GoogleUrl` varchar(255) NOT NULL,
  `LinkedInUrl` varchar(255) NOT NULL,
  `TwitterUrl` varchar(255) NOT NULL,
  `SnapChatUrl` varchar(255) NOT NULL,
  `InstagramUrl` varchar(255) NOT NULL,
  `VatNumber` varchar(255) NOT NULL,
  `VatPercentage` decimal(10,2) NOT NULL,
  `LoyaltyFactor` int(11) NOT NULL COMMENT 'Defines 1 SAR is equal to how many loyalty points',
  `InvitePoints` decimal(10,2) NOT NULL COMMENT 'No of points a user gets when his invited person sign up on booth app',
  `OrderCancelDays` int(11) NOT NULL,
  `OrderDisputedCancelDays` int(11) NOT NULL,
  `IOSAppVersion` decimal(10,2) NOT NULL,
  `AndroidAppVersion` decimal(10,2) NOT NULL COMMENT 'This will be version code',
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`SiteSettingID`, `SiteName`, `SiteImage`, `Email`, `Whatsapp`, `Skype`, `PhoneNumber`, `Fax`, `ProductDisclaimer`, `PromoCodeDisclaimer`, `OpenTime`, `CloseTime`, `FacebookUrl`, `GoogleUrl`, `LinkedInUrl`, `TwitterUrl`, `SnapChatUrl`, `InstagramUrl`, `VatNumber`, `VatPercentage`, `LoyaltyFactor`, `InvitePoints`, `OrderCancelDays`, `OrderDisputedCancelDays`, `IOSAppVersion`, `AndroidAppVersion`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 'QR', 'uploads/6429307990120210217112210boatylogo@3x.png', 'info@boaty.com', '', '', '12345678', '', '<h3>This is product disclaimer</h3>', '<h3>This is promo code disclaimer</h3>', '1564552800', '1564596000', 'www.facebook.com/schopfen', 'www.google.com/schopfen', 'www.linkedin.com/schopfen', 'www.twitter.com/schopfen', '', '', '0000000000000000', 0.00, 0, 5.00, 5, 5, 1.00, 1.00, '2018-04-03 14:33:23', '2021-03-09 10:15:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `FullName` varchar(255) NOT NULL,
  `UserName` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Mobile` varchar(255) NOT NULL,
  `OTP` varchar(255) NOT NULL COMMENT 'This is OTP that will be used for just once for verification',
  `Gender` enum('Male','Female','') DEFAULT NULL,
  `Dob` varchar(100) NOT NULL,
  `CityID` int(11) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Image` varchar(500) NOT NULL,
  `Bio` text NOT NULL,
  `LogoImage` varchar(255) NOT NULL,
  `CoverImage` varchar(255) NOT NULL,
  `LastState` enum('user','booth') NOT NULL DEFAULT 'user',
  `LastLangState` enum('EN','AR') NOT NULL,
  `IsEmailVerified` tinyint(4) NOT NULL DEFAULT '0',
  `IsMobileVerified` tinyint(4) NOT NULL DEFAULT '0',
  `Verification` varchar(255) NOT NULL,
  `PaymentAccountNumber` varchar(255) NOT NULL,
  `PaymentAccountHolderName` varchar(255) NOT NULL,
  `PaymentAccountBankBranch` varchar(255) NOT NULL,
  `IsProfileCustomized` tinyint(4) NOT NULL COMMENT 'It will have 1 if profile is fully customized',
  `HideContactNo` tinyint(4) NOT NULL,
  `Location` varchar(500) NOT NULL,
  `Points` decimal(10,2) NOT NULL COMMENT 'Points a user gets when his invited person sign up',
  `DeviceType` varchar(50) NOT NULL,
  `DeviceToken` varchar(255) NOT NULL,
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `Hide` tinyint(4) NOT NULL,
  `Notification` enum('on','off') NOT NULL DEFAULT 'on',
  `OnlineStatus` enum('Offline','Online','Busy') NOT NULL DEFAULT 'Offline',
  `OS` varchar(255) NOT NULL,
  `AppStatus` tinyint(4) NOT NULL COMMENT '0 means background, 1 means foreground',
  `SortOrder` int(11) NOT NULL,
  `CreatedAt` varchar(255) NOT NULL,
  `UpdatedAt` varchar(255) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL,
  `Lat` varchar(255) NOT NULL,
  `Long` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `RoleID`, `FullName`, `UserName`, `Email`, `Mobile`, `OTP`, `Gender`, `Dob`, `CityID`, `Password`, `Image`, `Bio`, `LogoImage`, `CoverImage`, `LastState`, `LastLangState`, `IsEmailVerified`, `IsMobileVerified`, `Verification`, `PaymentAccountNumber`, `PaymentAccountHolderName`, `PaymentAccountBankBranch`, `IsProfileCustomized`, `HideContactNo`, `Location`, `Points`, `DeviceType`, `DeviceToken`, `IsActive`, `Hide`, `Notification`, `OnlineStatus`, `OS`, `AppStatus`, `SortOrder`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`, `Lat`, `Long`) VALUES
(1, 1, '', '', 'admin@qr.com', '+9233312345678', '0', '', '', 1, '25d55ad283aa400af464c76d713c07ad', '', '', '', '', 'user', 'EN', 1, 1, '', '', '', '', 0, 0, '', 0.00, 'Android', 'eqUJfShNw38:APA91bF7r2tD3WoDxJwtE033TVCseXxUkeexMLROOXWPQtR9WyCaB7o0Wa6he10UKqTP0Ah-yaBC83-tZrvQgiYUa89pZQS1lqpa9VERe5_YaIHoqm0d4Dzl5IUbAWvSsqUGA0Irza9P', 1, 0, 'on', 'Offline', '', 0, 0, '1543388192', '2021-03-11 15:58:33', 0, 1, '', ''),
(31, 2, 'Muhammad Salmann', 'salmancs123', 'salman@gmail.com', '+966123123', '', NULL, '', 0, '25d55ad283aa400af464c76d713c07ad', 'uploads/users/3743082118920210331110320Image.jpg', 'This is my Bio This is my Bio This is my Bio This is This is my Bio my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is This is my Bio my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is This is my Bio my Bio This is my Bio This is my Bio This is my Bio This is my Bio This is my Bio ', '', '', 'user', 'EN', 1, 1, '', '', '', '', 0, 0, '', 0.00, 'iOS', '', 1, 0, 'on', 'Online', '14.0.0', 0, 0, '1616655599', '1617367149', 0, 0, '', ''),
(32, 2, 'Sarfraz', 'sarfraz11', 'sarfraz@gmail.om', '+966554433', '', NULL, '', 0, '25d55ad283aa400af464c76d713c07ad', 'uploads/users/4156690055820210331113248Image.jpg', '', '', '', 'user', 'EN', 1, 1, '', '', '', '', 0, 0, '', 0.00, 'iOS', '', 1, 0, 'on', 'Offline', '14.0.0', 0, 0, '1617191312', '1617191312', 0, 0, '', ''),
(33, 2, 'basit', 'basitc', 'me@zynq.net', '+966581057444', '', NULL, '', 0, 'e807f1fcf82d132f9bb018ca6738a19f', 'assets/user.png', '', '', '', 'user', 'EN', 0, 0, '', '', '', '', 0, 0, '', 0.00, 'iOS', '', 1, 0, 'on', 'Online', '14.4.1', 0, 0, '1617477431', '1617477431', 0, 0, '', ''),
(34, 2, 'basitcccc', 'abdulbaset', 'abdulbaset@schopfen.com', '+966581057443', '', NULL, '', 0, '25f9e794323b453885f5181f1b624d0b', 'assets/user.png', 'Hello', '', '', 'user', 'EN', 0, 0, '', '', '', '', 0, 0, '', 0.00, 'iOS', '', 1, 0, 'on', 'Online', '14.4.1', 0, 0, '1617477518', '1617477968', 0, 0, '', ''),
(35, 2, 'taha', 'test', 'taha@zynq.net', '055555', '', NULL, '', 0, '25d55ad283aa400af464c76d713c07ad', 'uploads/users/6918097544320210403075421Screenshot_20210403-004353_One_UI_Home.jpg', 'e', '', '', 'user', 'EN', 0, 0, '', '', '', '', 0, 0, '', 0.00, 'Android', '', 1, 0, 'on', 'Online', '10', 0, 0, '1617477714', '1617478862', 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_rating`
--

CREATE TABLE `users_rating` (
  `RatingID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Rating` int(11) NOT NULL,
  `BookingID` int(11) NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_rating`
--

INSERT INTO `users_rating` (`RatingID`, `UserID`, `Rating`, `BookingID`, `CreatedAt`) VALUES
(1, 13, 3, 1, '2021-02-16 08:41:33'),
(3, 16, 4, 2, '2021-02-16 08:41:33');

-- --------------------------------------------------------

--
-- Table structure for table `user_followers`
--

CREATE TABLE `user_followers` (
  `UserFollowerID` int(11) NOT NULL,
  `Follower` int(11) NOT NULL,
  `Following` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_followers`
--

INSERT INTO `user_followers` (`UserFollowerID`, `Follower`, `Following`) VALUES
(11, 31, 32),
(14, 34, 32);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`ActivityID`);

--
-- Indexes for table `activity_fixtures`
--
ALTER TABLE `activity_fixtures`
  ADD PRIMARY KEY (`ActivityFixtureID`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`BookingID`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`CityID`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`Email_templateID`);

--
-- Indexes for table `equipments`
--
ALTER TABLE `equipments`
  ADD PRIMARY KEY (`EquipmentID`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`FeedbackID`);

--
-- Indexes for table `fleets`
--
ALTER TABLE `fleets`
  ADD PRIMARY KEY (`FleetID`);

--
-- Indexes for table `invitations`
--
ALTER TABLE `invitations`
  ADD PRIMARY KEY (`InvitationID`);

--
-- Indexes for table `merchantactivities`
--
ALTER TABLE `merchantactivities`
  ADD PRIMARY KEY (`MerchantactivityID`);

--
-- Indexes for table `merchant_fleets`
--
ALTER TABLE `merchant_fleets`
  ADD PRIMARY KEY (`Merchant_fleetID`);

--
-- Indexes for table `mobileadses`
--
ALTER TABLE `mobileadses`
  ADD PRIMARY KEY (`MobileadsID`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`ModuleID`);

--
-- Indexes for table `modules_rights`
--
ALTER TABLE `modules_rights`
  ADD PRIMARY KEY (`ModuleRightID`),
  ADD KEY `fk_ModuleID` (`ModuleID`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`PageID`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`PriceID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indexes for table `site_images`
--
ALTER TABLE `site_images`
  ADD PRIMARY KEY (`SiteImageID`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`SiteSettingID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`),
  ADD KEY `users_ibfk_1` (`RoleID`);

--
-- Indexes for table `users_rating`
--
ALTER TABLE `users_rating`
  ADD PRIMARY KEY (`RatingID`);

--
-- Indexes for table `user_followers`
--
ALTER TABLE `user_followers`
  ADD PRIMARY KEY (`UserFollowerID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `ActivityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `activity_fixtures`
--
ALTER TABLE `activity_fixtures`
  MODIFY `ActivityFixtureID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `BookingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `CityID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `Email_templateID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `equipments`
--
ALTER TABLE `equipments`
  MODIFY `EquipmentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `FeedbackID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fleets`
--
ALTER TABLE `fleets`
  MODIFY `FleetID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invitations`
--
ALTER TABLE `invitations`
  MODIFY `InvitationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `merchantactivities`
--
ALTER TABLE `merchantactivities`
  MODIFY `MerchantactivityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `merchant_fleets`
--
ALTER TABLE `merchant_fleets`
  MODIFY `Merchant_fleetID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mobileadses`
--
ALTER TABLE `mobileadses`
  MODIFY `MobileadsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `ModuleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `modules_rights`
--
ALTER TABLE `modules_rights`
  MODIFY `ModuleRightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `PageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `PriceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `RoleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `site_images`
--
ALTER TABLE `site_images`
  MODIFY `SiteImageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `SiteSettingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `users_rating`
--
ALTER TABLE `users_rating`
  MODIFY `RatingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_followers`
--
ALTER TABLE `user_followers`
  MODIFY `UserFollowerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `modules_rights`
--
ALTER TABLE `modules_rights`
  ADD CONSTRAINT `fk_ModuleID` FOREIGN KEY (`ModuleID`) REFERENCES `modules` (`ModuleID`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
