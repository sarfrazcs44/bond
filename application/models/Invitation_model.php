<?php
    Class Invitation_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("invitations");

        }



        public function getInvitations($where = false){

        	$this->db->select('invitations.*,users.FullName,users.UserName,users.LogoImage');
        	$this->db->from('invitations');
        	$this->db->join('users','users.UserID = invitations.UserID');
        	if($where){
			$this->db->where($where);
			}
			return $this->db->get()->result_array();


        }
        public function getCountInvitation($where=false){
            
            
            if(!$where){
                return $this->db->count_all_results('invitations');
            }
            else{
                $this->db->where($where);
                $this->db->from('invitations');
                return $this->db->count_all_results();
            }
        }

        public function getLastestInvitations(){

            
            $type = array();
            for($i=1; $i<=5; $i++ ){
                $this->db->select('invitations.*,users.FullName,users.UserName,users.LogoImage');
                $this->db->from('invitations');
                $this->db->join('users','users.UserID = invitations.UserID');
                $this->db->where('Type',$i);
                $this->db->order_by('invitations.InvitationID','DESC');
                $this->db->limit(10);
                $type[] = $this->db->get()->result_array();
            }
            
            return $type;
//             $query = "SELECT InvitationID, Type, FirstName, LastName 
// FROM 
// (
//   SELECT InvitationID, Type, FirstName, LastName 
//   FROM   invitations 
//   WHERE (Type='1' OR Type='2' OR Type='3')
//   ORDER BY InvitationID DESC
//   Limit 50
// ) largest_cities
// ORDER BY Type, FirstName DESC";

//             $ret = $this->db->query($query)->result_array();
//             print_rm($ret);
//             exit();


        }


    }