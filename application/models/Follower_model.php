<?php

Class Follower_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_followers");

    }

    public function getFollowing($id, $start = false, $limit = false, $search = false, $lang = 'EN')
    {
        $todaydate = date('Y-m-d');
        $query = "SELECT users.* FROM user_followers
                JOIN users ON user_followers.Following = users.UserID
                WHERE user_followers.Follower = " . $id . "  ";

        if ($search) {
            $query .= " AND (users.FullName LIKE '%$search%' OR users.UserName LIKE '%$search%') ";
        }

        if ($start && $limit) {
            $query .= " LIMIT $start,$limit";
        }
        $query = $this->db->query($query);
        //echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {
            return false;
        }

    }

    public function getTotalFollowing($id)
    {
         $todaydate = date('Y-m-d');
        $sql = "Select Count(Follower) as Total from user_followers JOIN users ON user_followers.Following = users.UserID where Follower = " . $id . " ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0]['Total'];
        } else {
            return NULL;
        }

    }

    /*public function getFollower($id, $type, $start = false, $limit = false, $search = false)
    {
        $query = "Select users.*,users_text.* from user_followers JOIN users ON user_followers.Follower = users.UserID JOIN users_text ON users.UserID = users_text.UserID JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID where user_followers.Following = " . $id . " AND user_followers.Type = '" . $type . "' AND system_languages.ShortCode = 'EN'  ";

        if ($search) {
            $query .= " AND users_text.FullName LIKE '%$search%' OR users_text.BoothName LIKE '%$search%' ";
        }

        if ($start && $limit) {
            $query .= " LIMIT $start,$limit";
        }

        // echo $query;exit();

        $query = $this->db->query($query);


        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return false;
        }

    }*/

    public function getFollower($id, $start = false, $limit = false, $search = false, $lang = 'EN')
    {
        $query = "SELECT users.* FROM user_followers
        JOIN users ON user_followers.Follower = users.UserID
  
       WHERE user_followers.Following = " . $id . "";

         if ($search) {
            $query .= " AND (users.FullName LIKE '%$search%' OR users.UserName LIKE '%$search%') ";
        }

        if ($start && $limit) {
            $query .= " LIMIT $start,$limit";
        }
        $query = $this->db->query($query);
        // echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {
            return false;
        }

    }

    public function getTotalFollower($id)
    {
        $sql = "Select Count(Following) as Total from user_followers where Following = " . $id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0]['Total'];
        } else {

            return NULL;
        }

    }

    public function getUserFollowedBooths($UserID)
    {
        $sql = "Select Count(Following) as Total from user_followers where Follower = " . $UserID . "  AND Type = 'booth'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return (int)$result[0]['Total'];
        } else {

            return 0;
        }
    }



}