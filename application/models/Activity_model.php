<?php
    Class Activity_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("activities");

        }



        function getActivities($where=false)
			{
			

				$this->db->select('activities.*');
				$this->db->from('activities');
			    if($where){
				$this->db->where($where);
				}
				return $this->db->get()->result_array();
			}


    }