<?php
    Class Merchant_fleet_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("merchant_fleets");

        }




        public function getMerchantFleetData($where = false){

        	$this->db->select('merchant_fleets.*,fleets.Title,fleets.TitleAr,fleets.FleetImage,users.CompanyName,users.MerchantName,users.Image as UserImage');
        	$this->db->from('merchant_fleets');
        	$this->db->join('fleets','fleets.FleetID = merchant_fleets.FleetID');
        	$this->db->join('users','users.UserID = merchant_fleets.UserID');


        	if($where){
        		$this->db->where($where);
        	}


        	return $this->db->get()->result_array();

        }


    }