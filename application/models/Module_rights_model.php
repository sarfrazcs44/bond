<?php
Class Module_rights_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("modules_rights");

    }
    
    
    
    
    public function getModulesWithRights($role_id,$where = false){
        
            
            $this->db->select('modules.ModuleID,modules.Slug,modules.IconClass,modules.ParentID,modules.Title as ModuleTitle,roles.RoleID,roles.Title as RoleTitle,modules_rights.*');
            $this->db->from('modules');
           
            
            $this->db->join('modules_rights','modules.ModuleID = modules_rights.ModuleID');
            $this->db->join('roles','roles.RoleID = modules_rights.RoleID');
            
            
            
           
            
            $this->db->where('modules.Hide','0');
            $this->db->where('roles.RoleID',$role_id);
            
            
            if($where){
                $this->db->where($where);
            }
            
            $this->db->group_by('modules.ModuleID');
            
            $this->db->order_by('modules.SortOrder','ASC');
            $result = $this->db->get();
           //echo $this->db->last_query();exit;
            return $result->result();
    }

    
}