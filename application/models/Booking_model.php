<?php
    Class Booking_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("bookings");

        }


		function getBookingDetails($where=false)
			{
			

				$this->db->select('bookings.*,customer.FullName as CustomerFullName,customer.Email as CustomerEmail,customer_city.Title as CustomerCityTitle,merchant_city.Title as MerchantCityTitle,customer.Mobile as CustomerMobile,customer.Image as CustomerImage,merchant.Image as MerchantImage ,merchant.FullName as MerchantFullName,merchant.Email as MerchantEmail,merchant.Mobile as MerchantMobile,merchant.CompanyName,merchant.MerchantName,activities.Title,activities.TitleAr');
				$this->db->from('bookings');
			    $this->db->join('users as customer','customer.UserID = bookings.UserID');
			    $this->db->join('merchantactivities','merchantactivities.MerchantactivityID = bookings.MerchantActivityID');
			    $this->db->join('users as merchant','merchant.UserID = merchantactivities.UserID');
			    $this->db->join('cities as customer_city','customer_city.CityID = customer.CityID','left');
			    $this->db->join('cities as merchant_city','merchant_city.CityID = merchant.CityID','left');



			    $this->db->join('activities','activities.ActivityID = merchantactivities.ActivityID');
				if($where){
				$this->db->where($where);
				}
				return $this->db->get()->result_array();
			}
    }