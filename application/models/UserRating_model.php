<?php
Class UserRating_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users_rating");

    }
	
	public function save($post_data){
	
		   $insertrecord= $this->db->insert('users_rating',$post_data);
			return ($this->db->affected_rows() != 1) ? false : true;
		}

    
}