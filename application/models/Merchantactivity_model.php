<?php
                Class Merchantactivity_model extends Base_Model
                {
                    public function __construct()
                    {
                        parent::__construct("merchantactivities");

                    }

					
					function getMerchentActivity($where=false)
						{
						

							$this->db->select('*,activities.Title as ActivityTitle,activities.TitleAr as ActivityTitleAr,users.
								*,,cities.Title as CityTitle,cities.TitleAr as CityTitleAr');
							$this->db->from('merchantactivities');
							$this->db->join('activities','activities.ActivityID = merchantactivities.ActivityID');
							$this->db->join('users','users.UserID=merchantactivities.UserID','left');
							 $this->db->join('cities','cities.CityID=users.CityID','left');
							
							if($where){
							$this->db->where($where);
							}
							$query = $this->db->get()->result_array();
							//echo $this->db->last_query();die; 
							return $query;
						}
						function getEquiqpmentByID($get_ids){
						$this->db->select('equipments.EquipmentID,equipments.Title as EquipmentTitle,equipments.TitleAr as EquipmentTitleAr');
						$this->db->from('equipments');
						
						$this->db->where_in("EquipmentID",explode(',',$get_ids));

						$query = $this->db->get()->result_array();
							return $query;

							}
					
						function getRowsCountForMerchantActivity($where)
						{

							$this->db->select('*');
							$this->db->from('merchantactivities');
							$this->db->where($where);
							$query = $this->db->get();
							return $query->num_rows();
						}
                }