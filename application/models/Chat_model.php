<?php
Class Chat_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("chats");
    }

    public function getChatRooms($user_id, $start, $limit)
    {

       
        /*$sql = "SELECT cm.*, c.* FROM chat_messages cm 
                JOIN chats c ON cm.ChatID = c.ChatID 
                WHERE ((cm.SenderID = $user_id OR cm.ReceiverID = $user_id) AND c.Type = '".$type."' ) OR ((cm.SenderID = $user_id OR cm.ReceiverID = $user_id) AND c.ReceiverType = '".$type."')
                GROUP BY cm.ChatID 
                ORDER BY c.LastActivityAt DESC LIMIT $start, $limit";*/

         $sql = "SELECT chat_messages.*, chats.*,su.UserID as ConversationSenderID,ru.UserID as ConversationReceiverID,su.FullName as ConversationSenderName,ru.FullName as ConversationReceiverName,su.Image as ConversationSenderImage,ru.Image as ConversationReceiverImage
                    FROM chats 
                    LEFT JOIN chat_messages ON chat_messages.ChatID = chats.ChatID 
                    LEFT JOIN users su ON chats.SenderID = su.UserID 
                    LEFT JOIN users ru ON chats.ReceiverID = ru.UserID
                    WHERE chats.SenderID = $user_id OR chats.ReceiverID = $user_id
                    GROUP BY chats.ChatID 
                    ORDER BY chats.LastActivityAt DESC LIMIT $start, $limit";       
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}
