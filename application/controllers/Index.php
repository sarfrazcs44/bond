<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function test_notification()
    {
        $result = sendNotification("Test", "Test Notification", array(), 4);
        dump($result);
    }

    public function index()
    {
        redirect(base_url('cms/account/login'));
    }

    public function qr_page(){
        echo 'Coming soon';exit;
    }

    public function verifyEmail()
    {
        if (isset($_GET['verification_token']) && $_GET['verification_token'] != '') {
            $UserID = base64_decode($_GET['verification_token']);
            $user_info = $this->User_model->get($UserID,true,'UserID');
            if ($user_info) {
                $this->User_model->update(array('IsEmailVerified' => 1), array('UserID' => $UserID));
                echo "Your email is verified successfully. You can get back to the app to continue using it.";
                exit();
            } else {
                echo "Sorry! We couldn't verify you as your verification process failed.";
                exit();
            }
        } else {
            echo "Sorry! We couldn't verify you as your verification process failed.";
            exit();
        }
    }

    public function forgotPassword()
    {
        if (isset($_GET['verification_token']) && $_GET['verification_token'] != '') {
            $UserID = base64_decode($_GET['verification_token']);
            $user_info = $this->User_model->getJoinedData(true, 'UserID', 'users.UserID =' . $UserID);
            if ($user_info) {
                $data['UserID'] = $UserID;
                $this->load->view('forgot_password', $data);
            } else {
                echo "Sorry! We couldn't verify you as your verification process failed.";
                exit();
            }
        } else {
            echo "Sorry! We couldn't verify you as your verification process failed.";
            exit();
        }
    }

    public function changePassword()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $password = str_replace(' ', '', $post_data['new_password']);
            $password_length = strlen($password);
            if ($post_data['new_password'] !== $post_data['confirm_password']) {
                $response['status'] = false;
                $response['message'] = "Password and confirm password doesn't match!";
                echo json_encode($response);
                exit();
            } elseif ($password_length < 6) {
                $response['status'] = false;
                $response['message'] = "Password field must have atleast 6 characters!";
                echo json_encode($response);
                exit();
            } elseif ($post_data['new_password'] == $post_data['confirm_password'] && $password_length >= 6) {
                $UserID = base64_decode($post_data['UserID']);
                $user = $this->User_model->get($UserID, true, 'UserID');
                if ($user) {
                    $update['Password'] = md5($post_data['new_password']);
                    $update['OnlineStatus'] = 'Offline';
                    $update_by['UserID'] = $UserID;
                    $this->User_model->update($update, $update_by);
                    $response['status'] = true;
                    $response['message'] = "Your password is updated successfully. You can use this password for login.";
                    echo json_encode($response);
                    exit();
                } else {
                    $response['status'] = false;
                    $response['message'] = "Something went wrong. Please try again later.";
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response['status'] = false;
                $response['message'] = "Something went wrong. Please try again later.";
                echo json_encode($response);
                exit();
            }
        } else {
            $response['status'] = false;
            $response['message'] = "Something went wrong. Please try again later.";
            echo json_encode($response);
            exit();
        }
    }

    public function test()
    {
        $where = "users.UserID = 2";
        $user_info = $this->User_model->getUserInfo($where);
        $this->sendVerificationForEmail($user_info);
    }

    private function sendVerificationForEmail($user_info)
    {
        if ($user_info['Email'] !== '') {
            $verify_link = base_url() . 'index/verifyEmail?verification_token=' . base64_encode($user_info['UserID']) . '&lang=' . $this->language;
            $email_template = get_email_template(7, $this->language);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $user_info['FullName'], $message);
            $message = str_replace("{{link}}", $verify_link, $message);
            $data['to'] = $user_info['Email'];
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            $email_sent = sendEmail($data);
            if ($email_sent) {
                echo "Email sent";
            } else {
                echo "Email not sent";
            }
        }
    }

    public function invitation($id)
    {

        
        $data[] = array();

        $this->db->select('invitations.*,users.FullName,users.UserName,users.LogoImage');
        $this->db->from('invitations');
        $this->db->join('users','users.UserID = invitations.UserID');
        $this->db->where("InvitationID",$id);

        $this->data['results'] = $this->db->get()->result_array();
        
        //echo $this->session->redirection;
        //Invitation Private Re-enter Password Confirmation
        if(!empty($this->data['results'])){

            if($this->data['results'][0]['IsPrivate']==1){

                $uri = uri_string();
                $redirection_session_data = $this->session->userdata('redirection');
                if(!isset($redirection_session_data[$uri])){
                    
                    $this->session->set_userdata('redirect_back',$uri);
                    redirect(base_url('cms/account/loginForPrivate'));
                    
                }
            }//end password confirmation
        

            $invitation_type = $this->data['results'][0]['Type'];
            if ($invitation_type == 2 || $invitation_type == 3) {
                
                $this->load->view('frontend/smsAppLink',$this->data);

             } else{
                $this->load->view('frontend/invitation',$this->data);
            }

        } //end if

        else
            echo "Sorry! Invitation doesn't exist";
        
        
        

    }

    public function profile($id){
        $this->load->model('Invitation_model');
        $where = 'invitations.InvitationID = '.$id;
        $this->data['user'] = $this->Invitation_model->getInvitations($where);
        $this->load->view('frontend/profile', $this->data);
        
    }

    public function enterdetails(){
        $this->load->view('frontend/enterDetails');
    }

    public function finalize(){
        $this->load->view('frontend/finalize');
    }

    public function frontpage(){
        $this->load->view('frontend/front-page');
    }

    public function paynow(){
        $this->load->view('frontend/paynow');
    }

    public function createinvitation(){
        $this->load->view('frontend/create_invitation_1');
    }

    public function createqr(){
        $this->load->view('frontend/create_QR_code');
    }



}