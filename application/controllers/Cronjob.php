<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');

class Cronjob extends CI_Controller
{
    protected $language;

    public function __construct()
    {
        parent::__construct();
        $this->load->Model('Order_model');
        $this->load->Model('Order_request_model');
        $this->load->Model('User_model');
        $this->load->Model('User_text_model');
        $this->load->Model('Product_model');
        $this->language = 'EN';
    }

    public function index()
    {
        echo 'This is Booth cronjob controller called';
    }



     public function sendNotificationForExpiry($day = 10)
    {
        $users = $this->User_model->getUsersForExpiryNotification($day);
        $data = array();
        //print_rm($users);
        if(!empty($users)){

            foreach ($users as $user) {

                $data['LoggedInUserID'] = $user['UserID']; // the person who will receive this notification
                $data['UserType'] = 'booth';
                $data['Type'] = 'expiry';
                $data['UserID'] = 1;//admin // the person who is making this notification
               
                
                
                $data['NotificationTextEn'] = 'Your package will be expire after '.$day.' day(s).';
                $data['NotificationTextAr'] = 'Your package will be expire after '.$day.' day(s).';
                $data['CreatedAt'] = time();
                $mentioned_user_ids = array();
                $mentioned_user_names = array();
                $mentioned_user_types = array();
                log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $user['UserID']);

                $this->sendExpiryEmailBefore($user);


            }
            
        }
        
    }

    public function sendNotificationOnExpiry()
    {
        $users = $this->User_model->getUsersForExpiryNotification(-1);
        //print_rm($users);
        $data = array();
        //print_rm($users);
        if(!empty($users)){
            foreach ($users as $user) {

                $data['LoggedInUserID'] = $user['UserID']; // the person who will receive this notification
                $data['UserType'] = 'booth';
                $data['Type'] = 'expiry';
                $data['UserID'] = 1;//admin // the person who is making this notification
               
                
                
                $data['NotificationTextEn'] = 'Your package is expired.';
                $data['NotificationTextAr'] = 'Your package is expired.';
                $data['CreatedAt'] = time();
                $mentioned_user_ids = array();
                $mentioned_user_names = array();
                $mentioned_user_types = array();
                log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $user['UserID']);
                 $this->sendExpiryEmailAfter($user);

                
            }
            
        }
        
    }



    private function sendExpiryEmailBefore($user_info,$day)
    {
       
        if($user_info['LastLangState'] == 'AR'){
            $this->language = 'AR';
        }
        $email_template = get_email_template(21, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['BoothUserName'], $message);
        $message = str_replace("{{day}}", $day, $message);
        
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
       // sendSms($user_info['Mobile'], $message);
    }


     private function sendExpiryEmailAfter($user_info)
    {
       
        if($user_info['LastLangState'] == 'AR'){
            $this->language = 'AR';
        }
        $email_template = get_email_template(22, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['BoothUserName'], $message);
        
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
       // sendSms($user_info['Mobile'], $message);
    }

    public function markOrderAsCancelledCronjob()
    {

        // email testing to check cron job
        $e_data = array();
        $subject = 'Cron job';
        $message = 'Cron job is running bro';
        $e_data['to'] = 'sarfraz@zynq.net';
        $e_data['subject'] = $subject;
        $e_data['message'] = email_format($message);
        //sendEmail($e_data);

        // testing end
        $today = date('Y-m-d H:i');
        $site_settings = site_settings();
        $OrderCancelDays = $site_settings->OrderCancelDays;

        $where = 'orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2';
        $order_requests = $this->Order_request_model->getOrderRequestsForCronjob($where);

        foreach ($order_requests as $order_request) {
            $OrderCreatedAt = $order_request->CreatedAt;
            $OrderCreatedAt = date('Y-m-d H:i', $OrderCreatedAt);
            $OrderCancelDate = date('Y-m-d H:i', strtotime($OrderCreatedAt . " +$OrderCancelDays days"));
            if ($today >= $OrderCancelDate) {
                if ($order_request->OrderStatusID == 1) {
                    $update['OrderRequestRating'] = 1;
                    $update['OrderRequestReview'] = "Cancelled due to seller not responding";

                     // Sending notification To Customer
                    $data['LoggedInUserID'] = $order_request->UserID; // the person who will receive this notification
                    $data['UserType'] = 'user';
                    $data['Type'] = 'order';
                    $data['UserID'] = $order_request->BoothID; // the person who is making this notification
                    $data['OrderID'] = $order_request->OrderID;
                    $data['OrderRequestID'] = $order_request->OrderRequestID;
                    $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                    $UserInfo = getUserInfo($data['UserID'], $this->language);
                    $data['NotificationTextEn'] = '@'.$UserInfo->BoothUserName . " has cancelled your order request with order track number " . $order_request->OrderTrackID;
                    $data['NotificationTextAr'] = " قام البائع بإلغاء طلب طلبك مع رقم تتبع الطلب " . $order_request->OrderTrackID;
                    $data['CreatedAt'] = time();
                    $mentioned_user_ids = array($UserInfo->UserID);
                    $mentioned_user_names = array($UserInfo->BoothUserName);
                    $mentioned_user_types = array('booth');
                    log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                    $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->UserID);
                    $this->SendOrderCancelEmailToCustomer($order_request->UserID, $order_request->OrderTrackID,$UserInfo->BoothUserName);


                } elseif ($order_request->OrderStatusID == 2) {
                    $update['UserOrderRequestRating'] = 1;
                    $update['UserOrderRequestReview'] = "Cancelled due to buyer not responding";



                    // Sending notification To Booth
                    $data['LoggedInUserID'] = $order_request->BoothID; // the person who will receive this notification
                    $data['UserType'] = 'booth';
                    $data['Type'] = 'order';
                    $data['UserID'] = $order_request->UserID; // the person who is making this notification
                    $data['OrderID'] = $order_request->OrderID;
                    $data['OrderRequestID'] = $order_request->OrderRequestID;
                    $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                    $UserInfo = getUserInfo($data['UserID'], $this->language);
                    $data['NotificationTextEn'] = '@'.$UserInfo->UserName . " has cancelled an order request with order track number " . $order_request->OrderTrackID;
                    $data['NotificationTextAr'] = "قام المشتري بإلغاء طلب طلب برقم تتبع الطلب " . $order_request->OrderTrackID;
                    $data['CreatedAt'] = time();
                    $mentioned_user_ids = array($UserInfo->UserID);
                    $mentioned_user_names = array($UserInfo->UserName);
                    $mentioned_user_types = array('user');
                    log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                    $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->BoothID);
                    $this->SendOrderCancelEmailToBooth($order_request->BoothID, $order_request->OrderTrackID,$UserInfo->UserName);
                }


                $update['OrderLastStatusID'] = $order_request->OrderStatusID;
                $update['OrderStatusID'] = 9;//cancelled by system
                $update['OrderCancellationReasonID'] = 1;
                $update_by['OrderRequestID'] = $order_request->OrderRequestID;
                $this->Order_request_model->update($update, $update_by);

               


                

            }
        }
        $response['status'] = true;
        $response['message'] = "Cronjob run successfully";
        echo json_encode($response);
        exit();
    }

    public function testingEmailCronJob(){

        $email_template = get_email_template(12, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $data['to'] = 'sarfraz@zynq.net';
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);
         
    }

    public function markOrderAsCancelledCronjobBK()
    {
        $today = date('Y-m-d H:i');
        $site_settings = site_settings();
        $OrderCancelDays = $site_settings->OrderCancelDays;

        $where = 'orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2';
        $order_requests = $this->Order_request_model->getOrderRequestsForCronjob($where);
        foreach ($order_requests as $order_request) {
            $OrderCreatedAt = $order_request->CreatedAt;
            $OrderCreatedAt = date('Y-m-d H:i', $OrderCreatedAt);
            $OrderCancelDate = date('Y-m-d H:i', strtotime($OrderCreatedAt . " +$OrderCancelDays days"));
            if ($today >= $OrderCancelDate) {
                $update['OrderLastStatusID'] = $order_request->OrderStatusID;
                $update['OrderStatusID'] = 5;
                $update['OrderCancellationReasonID'] = 1;
                $update_by['OrderRequestID'] = $order_request->OrderRequestID;
                $this->Order_request_model->update($update, $update_by);

                // Sending notification To Customer
                $data['LoggedInUserID'] = $order_request->UserID; // the person who will receive this notification
                $data['UserType'] = 'user';
                $data['Type'] = 'order';
                $data['UserID'] = $order_request->BoothID; // the person who is making this notification
                $data['OrderID'] = $order_request->OrderID;
                $data['OrderRequestID'] = $order_request->OrderRequestID;
                $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                $UserInfo = getUserInfo($data['UserID'], $this->language);
                $data['NotificationTextEn'] = '@'.$UserInfo->BoothUserName . " has cancelled your order request with order track number " . $order_request->OrderTrackID;
                $data['NotificationTextAr'] = " قام البائع بإلغاء طلب طلبك مع رقم تتبع الطلب " . $order_request->OrderTrackID;
                $data['CreatedAt'] = time();
                $mentioned_user_ids = array($UserInfo->UserID);
                $mentioned_user_names = array($UserInfo->BoothUserName);
                $mentioned_user_types = array('booth');
                log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->UserID);
                $this->SendOrderCancelEmailToCustomer($order_request->UserID, $order_request->OrderTrackID);

                // Sending notification To Booth
                $data['LoggedInUserID'] = $order_request->BoothID; // the person who will receive this notification
                $data['UserType'] = 'booth';
                $data['Type'] = 'order';
                $data['UserID'] = $order_request->UserID; // the person who is making this notification
                $data['OrderID'] = $order_request->OrderID;
                $data['OrderRequestID'] = $order_request->OrderRequestID;
                $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                $UserInfo = getUserInfo($data['UserID'], $this->language);
                $data['NotificationTextEn'] = '@'.$UserInfo->UserName . " has cancelled an order request with order track number " . $order_request->OrderTrackID;
                $data['NotificationTextAr'] = "قام المشتري بإلغاء طلب طلب برقم تتبع الطلب " . $order_request->OrderTrackID;
                $data['CreatedAt'] = time();
                $mentioned_user_ids = array($UserInfo->UserID);
                $mentioned_user_names = array($UserInfo->UserName);
                $mentioned_user_types = array('user');
                log_notification($data, $mentioned_user_ids, $mentioned_user_names, $mentioned_user_types);
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->BoothID);
                $this->SendOrderCancelEmailToBooth($order_request->BoothID, $order_request->OrderTrackID,$UserInfo->UserName);

            }
        }
        $response['status'] = true;
        $response['message'] = "Cronjob run successfully";
        echo json_encode($response);
        exit();
    }

    private function SendOrderCancelEmailToCustomer($UserID, $OrderTrackID,$seller_user_name)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID);
        if($user_info['LastLangState'] == 'AR'){
            $this->language = 'AR';
        }
        $email_template = get_email_template(18, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{user_name}}", $user_info['UserName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $message = str_replace("{{seller_user_name}}", $seller_user_name, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
       // sendSms($user_info['Mobile'], $message);
    }

    private function SendOrderCancelEmailToBooth($UserID, $OrderTrackID,$buyer_user_name)
    {
        
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID);
         if($user_info['LastLangState'] == 'AR'){
            $this->language = 'AR';
        }

        $email_template = get_email_template(19, $this->language);

        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{user_name}}", $user_info['BoothName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $message = str_replace("{{buyer_user_name}}", $buyer_user_name, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
        //sendSms($user_info['Mobile'], $message);
    }

    public function markOrderAsDisputedCancelledCronjob()
    {
        $today = date('Y-m-d H:i');
        $site_settings = site_settings();
        $OrderDisputedCancelDays = $site_settings->OrderDisputedCancelDays;

        $where = 'orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2';
        $order_requests = $this->Order_request_model->getOrderRequestsForCronjob($where);
        foreach ($order_requests as $order_request) {
            $OrderCreatedAt = $order_request->CreatedAt;
            $OrderCreatedAt = date('Y-m-d H:i', $OrderCreatedAt);
            $OrderDisputedCancelDays = date('Y-m-d H:i', strtotime($OrderCreatedAt . " +$OrderDisputedCancelDays days"));
            if ($today >= $OrderDisputedCancelDays) {
                $update['OrderLastStatusID'] = $order_request->OrderStatusID;
                $update['OrderStatusID'] = 8;
                $update['OrderCancellationReasonID'] = 1;
                $update_by['OrderRequestID'] = $order_request->OrderRequestID;
                $this->Order_request_model->update($update, $update_by);

                // Sending notification To Customer
                $data['LoggedInUserID'] = $order_request->UserID; // the person who will receive this notification
                $data['UserType'] = 'user';
                $data['Type'] = 'order';
                $data['UserID'] = $order_request->BoothID; // the person who is making this notification
                $data['OrderID'] = $order_request->OrderID;
                $data['OrderRequestID'] = $order_request->OrderRequestID;
                $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                $UserInfo = getUserInfo($data['UserID'], $this->language);
                $data['NotificationTextEn'] = "Your order request is cancelled by system as disputed order with order track number " . $order_request->OrderTrackID;
                $data['NotificationTextAr'] = " قام البائع بإلغاء طلب طلبك مع رقم تتبع الطلب " . $order_request->OrderTrackID;
                $data['CreatedAt'] = time();
                log_notification($data);
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->UserID);
                $this->SendOrderDisputedCancelEmailToCustomer($order_request->UserID, $order_request->OrderTrackID);

                // Sending notification To Booth
                $data['LoggedInUserID'] = $order_request->BoothID; // the person who will receive this notification
                $data['UserType'] = 'booth';
                $data['Type'] = 'order';
                $data['UserID'] = $order_request->UserID; // the person who is making this notification
                $data['OrderID'] = $order_request->OrderID;
                $data['OrderRequestID'] = $order_request->OrderRequestID;
                $LoggedInUserInfo = getUserInfo($data['LoggedInUserID'], $this->language);
                $UserInfo = getUserInfo($data['UserID'], $this->language);
                $data['NotificationTextEn'] = "Your order request is cancelled by system as disputed order with order track number " . $order_request->OrderTrackID;
                $data['NotificationTextAr'] = " قام البائع بإلغاء طلب طلبك مع رقم تتبع الطلب " . $order_request->OrderTrackID;
                $data['CreatedAt'] = time();
                log_notification($data);
                $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->BoothID);
                $this->SendOrderDisputedCancelEmailToBooth($order_request->BoothID, $order_request->OrderTrackID);

            }
        }
        $response['status'] = true;
        $response['message'] = "Cronjob run successfully";
        echo json_encode($response);
        exit();
    }

    private function SendOrderDisputedCancelEmailToCustomer($UserID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID);
         if($user_info['LastLangState'] == 'AR'){
            $this->language = 'AR';
        }
        $email_template = get_email_template(14, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FullName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
        //sendSms($user_info['Mobile'], $message);
    }

    private function SendOrderDisputedCancelEmailToBooth($UserID, $OrderTrackID)
    {
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID);
         if($user_info['LastLangState'] == 'AR'){
            $this->language = 'AR';
        }
        $email_template = get_email_template(14, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['BoothName'], $message);
        $message = str_replace("{{order_track_id}}", $OrderTrackID, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

        // Sending sms to customer
        //sendSms($user_info['Mobile'], $message);
    }

    public function markProductsPromotionInactiveCronjob()
    {
        return false;
        $fetch_by['IsPromotedProduct'] = 1;
        $fetch_by['ProductPromotionExpiresAt <'] = time();
        $products = $this->Product_model->getMultipleRows($fetch_by);
        if ($products) {
            foreach ($products as $product) {
                $update['IsPromotedProduct'] = 0;
                $update['IsPromotionApproved'] = 0;
                $update_by['ProductID'] = $product->ProductID;
                $this->Product_model->update($update, $update_by);
            }
        }
        $response['status'] = true;
        $response['message'] = "Cronjob run successfully";
        echo json_encode($response);
        exit();
    }

    public function notifyUsersOrderAboutToCancelCronjob()
    {
        $today = date('Y-m-d H:i');
        $site_settings = site_settings();
        $OrderCancelDays = $site_settings->OrderCancelDays - 2;
        $where = 'orders_requests.OrderStatusID = 1 OR orders_requests.OrderStatusID = 2';
        $order_requests = $this->Order_request_model->getOrderRequestsForCronjob($where);
        foreach ($order_requests as $order_request) {
            $OrderCreatedAt = $order_request->CreatedAt;
            $OrderCreatedAt = date('Y-m-d H:i', $OrderCreatedAt);
            $OrderCancelDate = date('Y-m-d H:i', strtotime($OrderCreatedAt . " +$OrderCancelDays days"));
            if ($today >= $OrderCancelDate) {

                if ($order_request->OrderStatusID == 1) {
                    // Sending notification To Booth
                    $data['LoggedInUserID'] = $order_request->BoothID; // the person who will receive this notification
                    $data['UserType'] = 'booth';
                    $data['Type'] = 'order';
                    $data['UserID'] = $order_request->UserID; // the person who is making this notification
                    $data['OrderID'] = $order_request->OrderID;
                    $data['OrderRequestID'] = $order_request->OrderRequestID;
                    $data['NotificationTextEn'] = 'Your order request with order track number ' . $order_request->OrderTrackID . ' is about to get cancelled.';
                    $data['NotificationTextAr'] = 'طلب الحصول على رقم الطلب ' . $order_request->OrderTrackID . ' على وشك الإلغاء';
                    $data['CreatedAt'] = time();
                    log_notification($data);
                    $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->BoothID);
                } elseif ($order_request->OrderStatusID == 2) {
                    // Sending notification To Customer
                    $data['LoggedInUserID'] = $order_request->UserID; // the person who will receive this notification
                    $data['UserType'] = 'user';
                    $data['Type'] = 'order';
                    $data['UserID'] = $order_request->BoothID; // the person who is making this notification
                    $data['OrderID'] = $order_request->OrderID;
                    $data['OrderRequestID'] = $order_request->OrderRequestID;
                    $data['NotificationTextEn'] = 'Your order request with order track number ' . $order_request->OrderTrackID . ' is about to get cancelled.';
                    $data['NotificationTextAr'] = 'طلب الحصول على رقم الطلب ' . $order_request->OrderTrackID . ' على وشك الإلغاء';
                    $data['CreatedAt'] = time();
                    log_notification($data);
                    $res = sendNotification('Booth', $data['NotificationTextEn'], $data, $order_request->UserID);
                }
            }
        }
        $response['status'] = true;
        $response['message'] = "Cronjob run successfully";
        echo json_encode($response);
        exit();
    }


    public function deleteInActiveUsers(){
        $email_data = array();
        $email_data['to'] = 'sarfraz.cs10@gmail.com';
        $email_data['message'] = 'delete user email is working';
        $email_data['subject'] = 'cron job checking';
        sendEmail($email_data);
         $where = 'DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(users.CreatedAt), "%Y-%m-%d %H:%i:%s"), NOW()) <= -2 AND users.RoleID != 1 AND users.IsEmailVerified = 0'; 
         $users = $this->User_model->getAllJoinedData(false,'UserID',$this->language,$where);
         //echo $this->db->last_query();
        // print_rm($users);
         if($users){
            foreach ($users as $key => $value) {
               $this->User_text_model->delete(array('UserID' => $value->UserID));
               $this->User_model->delete(array('UserID' => $value->UserID));
            }
         }
    }

}