<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchantactivity extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            'Activity_fixtures_model'
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['TableKey'] = 'MerchantactivityID';
                $this->data['Table'] = 'merchantactivities';
       
        
    }
     
    
    public function index($user_id = '')
    {
          $parent                             = $this->data['Parent_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
          $where = false;
          if($user_id != ''){
            $where = 'merchantactivities.UserID = '.$user_id;
          }
	
          $this->data['results'] = $this->$parent->getMerchentActivity($where);
          
          $this->load->view('backend/layouts/default',$this->data);
    }
    public function add()
    {
         if(!checkRightAccess(66,$this->session->userdata['admin']['RoleID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
            
			redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent  = $this->data['Parent_model'];
		
		$this->data['activities']=$this->db->get('activities')->result();
      

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
       
        
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
        if(!checkRightAccess(66,$this->session->userdata['admin']['RoleID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $where = "merchantactivities.MerchantactivityID = " .$id; 
        $this->data['result'] = $this->Merchantactivity_model->getMerchentActivity($where);
        if($this->data['result'][0]['Fixture'] != ''){
            $this->data['result'][0]['Fixture'] = $this->Activity_fixtures_model->getMultipleRowsWhere('activity_fixtures.ActivityFixtureID IN ('.$this->data['result'][0]['Fixture'].')',true);
         }else{
            $this->data['result'][0]['Fixture'] = array();
         }

        $getEququiments=$this->Merchantactivity_model->getEquiqpmentByID($this->data['result'][0]['Equipment']);
            
        $this->data['result'][0]['equipments'] = $getEququiments; 

         //print_rm($this->data['result']);
        $this->data['result'] = $this->data['result'][0];
        
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
       
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
              //  $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->update();
          break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

      //  $this->form_validation->set_rules('Title', lang('title'), 'required');
      
        



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
       //print_r($this->input->post());
	  //die;
        if(!checkRightAccess(66,$this->session->userdata['admin']['RoleID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
	
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $save_parent_data                   = array();

        unset($post_data['form_type']);
	    $LocationName=implode(',',array_filter($this->input->post('Location')));
		$LocationPrice=implode(',',array_filter($this->input->post('LocationPrice')));
		$EquipmentName=implode(',',array_filter($this->input->post('Equipment')));
		$EquipmentPrice=implode(',',array_filter($this->input->post('EquipmentPrice')));
        $save_parent_data = $post_data;
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
         $save_parent_data['Equipment']=$EquipmentName;
		 $save_parent_data['EquipmentPrice']=$EquipmentPrice;
		 $save_parent_data['Fixture']=$LocationName;
		 $save_parent_data['FixturePrice']=$LocationPrice;
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
        


        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
                
            
            
            
                
                
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'';
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }
    
        private function update()
    {
        


                if(!checkRightAccess(66,$this->session->userdata['admin']['RoleID'],'CanEdit')){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/'.$this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
                if(isset($post_data[$this->data['TableKey']])){
                    $id = base64_decode($post_data[$this->data['TableKey']]);
                    $this->data['result']          = $this->$parent->get(base64_decode($post_data[$this->data['TableKey']]),false,$this->data['TableKey']);
    
        
                if(!$this->data['result']){
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $errors['redirect'] = true;
                   $errors['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }

             $LocationName=implode(',',array_filter($this->input->post('Location')));
		$LocationPrice=implode(',',array_filter($this->input->post('LocationPrice')));
		$EquipmentName=implode(',',array_filter($this->input->post('Equipment')));
		$EquipmentPrice=implode(',',array_filter($this->input->post('EquipmentPrice')));
                
            unset($post_data['form_type']);
            $save_parent_data                   = array();
            $save_parent_data = $post_data;
            $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                   
		$save_parent_data['Equipment']=$EquipmentName;
		 $save_parent_data['EquipmentPrice']=$EquipmentPrice;
		 $save_parent_data['Fixture']=$LocationName;
		 $save_parent_data['FixturePrice']=$LocationPrice; 
				   $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    
                    unset($save_parent_data[$this->data['TableKey']]);
                    
                    $this->$parent->update($save_parent_data,$update_by);
                
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
                
                
              
              
           
        
        


        
    }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    
    
   
    
    private function delete(){
        
         if(!checkRightAccess(66,$this->session->userdata['admin']['RoleID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    public function getEqiupmentsForActivity(){
		$post_data = $this->input->post();
		$EqpID=$post_data['ActivityID'];
		$this->db->select('*');
		$this->db->from('activities');
		$this->db->join('equipments', 'activities.EquipmentID=equipments.EquipmentID');
		$this->db->where('ActivityID', $EqpID); 
		
		$data_array = $this->db->get()->result();
	//echo  $sql = $this->db->last_query();die;
		foreach($data_array as $result){
			
			echo "<option value='$result->EquipmentID'>$result->Title</option>";
			}
		
		
		exit;
		}
    public function getMaxCapacityForActivity(){
		$post_data = $this->input->post();
		$EqpID=$post_data['ActivityID'];
		$this->db->select('NumberOfGuests');
		$this->db->from('activities');
		$this->db->where('ActivityID', $EqpID); 
		echo $this->db->get()->row()->NumberOfGuests;
	
		
		exit;
		}

}