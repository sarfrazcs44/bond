<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            'Role_model',
            'User_model',
            'Module_model',
			'Site_images_model'

        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
       
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';


    }


    public function index()
    {
        
        $parent = $this->data['Parent_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        //$this->data['admins'] = $this->$parent->getUsers("users.RoleID = 1", $this->language);
        $this->data['admins'] = $this->$parent->getMultipleRows(array('RoleID' => 1));
        // $this->data['merchant_users'] = $this->$parent->getMultipleRows(array('RoleID' => 2,'IsActive' => 1));
        // $this->data['merchant_not_approved'] = $this->$parent->getMultipleRows(array('RoleID' => 2,'IsActive' => 0));
        $this->data['customer_users'] = $this->$parent->getMultipleRows(array('RoleID' => 2));

        $where = '(RoleID = 2) AND IsMobileVerified = 0 ';

        $this->data['mobile_unverified_users'] = $this->$parent->getMultipleRows($where);
       
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';

        $fetch_by = array();
        $fetch_by['IsActive'] = 1;
        $fetch_by['RoleID != '] = 2;
        $fetch_by['RoleID != '] = 3;
        $this->data['roles'] = $this->Role_model->getMultipleRows($fetch_by);
        if (!$this->data['roles']) {
            $this->session->set_flashdata('message', lang('please_add_role_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        

        $this->load->view('backend/layouts/default', $this->data);
    }

    

    public function edit($id = '')
    {
        if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->get($id, false,'UserID');


        if (!$this->data['result']) {
            $this->session->set_flashdata('message', lang('some_thing_went_wrong'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


       $fetch_by = array();
        $fetch_by['IsActive'] = 1;
        $fetch_by['RoleID != '] = 2;
        $fetch_by['RoleID != '] = 3;
        $this->data['roles'] = $this->Role_model->getMultipleRows($fetch_by);
        if (!$this->data['roles']) {
            $this->session->set_flashdata('message', lang('please_add_role_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';

        $this->load->view('backend/layouts/default', $this->data);

    }
	
	public function profile($id = ''){
		
		 if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->get($id, false,'UserID');


        if (!$this->data['result']) {
            $this->session->set_flashdata('message', lang('some_thing_went_wrong'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
		
		$where='UserID="'.$id.'"';
        $this->data['userinfo'] = $this->$parent->getUserInfo($where);
		$fetch_by = array();
        $fetch_by['FileID'] = $id;
		// $fetch_by['ImageType'] = 'user_document';
		$this->data['userdoc'] = $this->Site_images_model->getMultipleRows($fetch_by);
		$fetch_by1 = array();
        $fetch_by1['FileID'] = $id;
		// $fetch_by1['ImageType'] = 'user_license';
		$this->data['userlic'] = $this->Site_images_model->getMultipleRows($fetch_by1);

		
		$this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/profile';

        $this->load->view('backend/layouts/default', $this->data);
		
		
		}

    


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                //$this->validate();
                $this->delete();
                break;

            case 'save_rights':
                $this->saveRights();
                break;

        }
    }


    private function validate($check = true)
    {

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]');
        // $this->form_validation->set_rules('Mobile', 'Mobile', 'required|is_unique[users.Mobile]');
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
        $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
        $this->form_validation->set_rules('Mobile', 'Mobile No.', 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }


    private function save()
    {

        if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
       
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
        $save_parent_data['RoleID'] = $post_data['RoleID'];
        $save_parent_data['Password'] = md5($post_data['Password']);
        $save_parent_data['Email'] = $post_data['Email'];
        $save_parent_data['Mobile'] = $post_data['Mobile'];
        $save_parent_data['FullName'] = $post_data['Title'];
        

        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {


           


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->get($id,false,'UserID');


            if (!$this->data['result']) {

                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $success['redirect'] = true;
                $success['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_parent_data['Mobile'] = $post_data['Mobile'];
            $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
            $save_parent_data['RoleID'] = $post_data['RoleID'];
            $save_parent_data['FullName'] = $post_data['Title'];
            //$save_parent_data['CenterID']     = $post_data['CenterID'];
            if ($post_data['RoleID'] == 3) {
                $save_parent_data['UserType'] = $post_data['UserType'];
                $save_parent_data['CityID'] = $post_data['CityID'];
                $save_parent_data['Mobile'] = $post_data['Mobile'];

            }
            $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

            $update_by = array();
            $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);


            $this->$parent->update($save_parent_data, $update_by);


            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {

            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    


    private function delete()
    {

        if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $parent = $this->data['Parent_model'];
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');

      
       
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    public function updateIsActive()
    {
        $post_data = $this->input->post();
        $update['IsActive'] = $post_data['IsActive'];
        $update_by['UserID'] = $post_data['UserID'];
        $this->User_model->update($update, $update_by);
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;
    }

     public function updateMaroofVerify()
    {
        $post_data = $this->input->post();
       
        $update_by['UserID'] = $post_data['UserID'];
        unset($post_data['UserID']);
        $this->User_model->update($post_data, $update_by);
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;
    }

    public function verifyAllUsers()
    {
        $users = $this->User_model->getAll();
        foreach ($users as $user) {
            $update['IsEmailVerified'] = 1;
            $update['IsMobileVerified'] = 1;
            $update_by['UserID'] = $user->UserID;
            $this->User_model->update($update, $update_by);
        }
        $success['error'] = false;
        $success['success'] = "Users verified.";
        echo json_encode($success);
        exit;
    }

    public function verifyUser()
    {
        $post_data = $this->input->post();
        if($post_data['Verification'] == 'email'){
            $update['IsEmailVerified'] = 1;
        }else if($post_data['Verification'] == 'mobile'){
             $update['IsMobileVerified'] = 1;
        }
        
       
        $update_by['UserID'] = $post_data['UserID'];
        $this->User_model->update($update, $update_by);
        $success['error'] = false;
        $success['success'] = "User verified.";
        $success['reload'] = true;
        echo json_encode($success);
        exit;
    }


    

    public function logout_from_all_devices(){
         $post_data = $this->input->post();
        $this->User_model->update(array('OnlineStatus' => 'Offline', 'DeviceToken' => ''), array('UserID' => $post_data['UserID']));
        $success['error'] = false;
        $success['success'] = "Logout successfully";
        echo json_encode($success);
        exit;
    }


}