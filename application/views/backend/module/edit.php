<?php
$option = '';
if(!empty($modules)){
    foreach($modules as $module){
        $option .= '<option value="'.$module->ModuleID.'" '.((isset($result->ParentID) && $result->ParentID == $module->ModuleID) ? 'selected' : '').'>'.$module->Title.' </option>';
    } }

?>
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                       
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">contacts</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h4>
                                    <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="<?php echo $TableKey; ?>" value="<?php echo base64_encode($result->$TableKey); ?>">

                                                   
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="ParentID"><?php echo lang('choose_parent_module'); ?>:</label>
                                                                <select id="ParentID" class="selectpicker" data-style="select-with-transition" required name="ParentID">
                                                                    <option value="0"><?php echo lang('parent_module'); ?></option>
                                                                    <?php echo $option; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title" value="<?php echo $result->Title; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="TitleAr"><?php echo lang('title_ar'); ?></label>
                                                                <input type="text" name="TitleAr" parsley-trigger="change" required  class="form-control" id="TitleAr" value="<?php echo $result->TitleAr; ?>">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>



                                                    
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Slug"><?php echo lang('slug'); ?></label>
                                                                <input type="text" name="Slug" parsley-trigger="change" required  class="form-control" id="Slug" value="<?php echo $result->Slug; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="IconClass"><?php echo lang('icon_class'); ?></label>
                                                                <input type="text" name="IconClass" parsley-trigger="change"  class="form-control" id="IconClass" value="<?php echo $result->IconClass; ?>">
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                    
                                                    <div class="row">

                                                        <div class="col-sm-4 checkbox-radios">
                                                            <div class="form-group label-floating">
                                                                <div class="checkbox">
                                                                    <label for="IsActive">
                                                                        <input name="IsActive" value="1" type="checkbox" id="IsActive" <?php echo ((isset($result->IsActive) && $result->IsActive == 1) ? 'checked' : ''); ?>/> <?php echo lang('is_active'); ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-sm-4 checkbox-radios">
                                                            <div class="form-group label-floating">
                                                                <div class="checkbox">
                                                                    <label for="CreateTable">
                                                                        <input name="CreateTable" value="1" type="checkbox" id="CreateTable" <?php echo ((isset($result->CreateTable) && $result->CreateTable == 1) ? 'checked' : ''); ?>/> <?php echo 'Create Table'; ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4 checkbox-radios">
                                                            <div class="form-group label-floating">
                                                                <div class="checkbox">
                                                                    <label for="CreateModel">
                                                                        <input name="CreateModel" value="1" type="checkbox" id="CreateModel" <?php echo ((isset($result->CreateModel) && $result->CreateModel == 1) ? 'checked' : ''); ?>/> <?php echo lang('create_model'); ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4 checkbox-radios">
                                                            <div class="form-group label-floating">
                                                                <div class="checkbox">
                                                                    <label for="CreateView">
                                                                        <input name="CreateView" value="1" type="checkbox" id="CreateView" <?php echo ((isset($result->CreateView) && $result->CreateView == 1) ? 'checked' : ''); ?>/> <?php echo lang('create_view'); ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4 checkbox-radios">
                                                            <div class="form-group label-floating">
                                                                <div class="checkbox">
                                                                    <label for="CreateController">
                                                                        <input name="CreateController" value="1" type="checkbox" id="CreateController" <?php echo ((isset($result->CreateController) && $result->CreateController == 1) ? 'checked' : ''); ?>/> <?php echo lang('create_controller'); ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            <?php echo lang('submit');?>
                                                        </button>
                                                        <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                          <?php echo lang('back');?>
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
</div>

