<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('title_ar');?></th>
                                    <th>Type</th>
                                    <th>Price</th>
                                    <th>Private Charges</th>

                                    <th><?php echo lang('is_active');?></th>
                                    <th> Is Free</th>
                                    <th>Is Paid</th>


                                    <?php if(checkRightAccess(73,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(73,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value->PriceID;?>">

                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo $value->TitleAr; ?></td>
                                            <td><?php echo ($value->PriceType == 1 ? 'Page Design' : 'Qr Design'); ?></td>
                                            <td><?php echo $value->Price; ?></td>
                                            <td><?php echo $value->PrivateCharges; ?></td>


                                            <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                             <td><?php echo ($value->IsFree ? lang('yes') : lang('no')); ?></td>
                                              <td><?php echo ($value->IsPaid ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkRightAccess(73,$this->session->userdata['admin']['RoleID'],'CanEdit') || checkRightAccess(73,$this->session->userdata['admin']['RoleID'],'CanDelete')){?> 
                                            <td>
                                                <?php if(checkRightAccess(73,$this->session->userdata['admin']['RoleID'],'CanEdit')){?>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->PriceID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                               
                                                <?php if(checkRightAccess(73,$this->session->userdata['admin']['RoleID'],'CanDelete')){?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->PriceID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>