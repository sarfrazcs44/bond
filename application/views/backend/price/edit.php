<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="<?php echo $TableKey; ?>" value="<?php echo base64_encode($result->$TableKey); ?>">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title" value="<?php echo $result->Title; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TitleAr"><?php echo lang('title_ar'); ?></label>
                                        <input type="text" name="TitleAr" required  class="form-control" id="TitleAr" value="<?php echo $result->TitleAr; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="PriceType" class="form-control" id="PriceType" >
                                            <option value="1" <?php echo ($result->PriceType == 1 ? 'select' : ''); ?>>Page Design</option>
                                            <option value="2" <?php echo ($result->PriceType == 2 ? 'select' : ''); ?>>Qr Design</option>
                                            
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Price">Price</label>
                                        <input type="number" name="Price" required  class="form-control" id="Price" value="<?php echo $result->Price;?>">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Private Charges">Private Charges</label>
                                        <input type="number" name="PrivateCharges" required  class="form-control" id="PrivateCharges" value="<?php echo $result->PrivateCharges;?>">
                                    </div>
                                </div>

                            </div>

                          
                            <div class="row">
                                 <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsFree">
                                                <input name="IsFree" value="1" type="checkbox" id="IsFree" <?php echo ($result->IsFree == 1 ? 'checked' : ''); ?>/> Is Free
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsPaid">
                                                <input name="IsPaid" value="1" type="checkbox" id="IsPaid" <?php echo ($result->IsPaid == 1 ? 'checked' : ''); ?>/> Is Paid
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" <?php echo ($result->IsActive == 1 ? 'checked' : ''); ?>/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>