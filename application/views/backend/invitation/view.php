<div class="content">
    <div class="container-fluid">

<?php if(isset($_GET['type'])) $type = $_GET['type'];?>
        <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="orange">
                                    <h4 class="card-title">User Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                            <th>Logo</th>
                                            <th>Full Name</th>
                                            <th>User Name</th>
                                            <?php echo ($type==5?"":"<th>Contact Info</th>")?>
                                            <th>Host</th>

                                        </tr></thead>
                                        <tbody>
                                            <tr>
                                               
                                                <td> <img style="width: 70px; height: 70px" src="<?php echo ImageExist($result['Logo']) ?>" ></td>
                                                <td><?php echo $result['FullName'];?></td>
                                                <td><?php echo $result['UserName'];?></td>
                                                <?php echo ($type==5)? "" : '<td> '.$result['ContactInfo'].' </td>' ?> 
                                                <td><?php echo $result['HostName'];?></td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <?php if($type==4 || $type==5) {?>
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="green">
                                    <h4 class="card-title">Personal Information</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-danger">
                                            <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th>Company</th>
                                            <th>Birthday</th>

                                        </tr></thead>
                                        <tbody>
                                            <tr>
                                               
                                                <td><?php echo $result['FirstName'];?> </td>
                                                <td><?php echo $result['LastName'];?></td>
                                                <td><?php echo $result['Email'];?></td>
                                                <td><?php echo $result['Address'];?></td>
                                                <td><?php echo $result['Company'];?></td>
                                                <td><?php echo $result['Birthday'];?></td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php } ?>


                           <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="blue">
                                    <h4 class="card-title">Invitations Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                            <th>Invitation ID</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>QR URL</th>
                                            
                                            
                                            <th>QR ID</th>
                                            <th>Is Private</th>
                                        </tr></thead>
                                        <tbody>
                                            <tr>
                                               
                                                <td><?php echo $result['InvitationID'];?></td>
                                                <td><?php echo $result['Title'];?></td>
                                                <td><?php echo $result['Description'];?></td>
                                                <td><?php echo $result['QRUrl'];?></td>
                                                
                                                <td><?php echo $result['QRID'];?></td>
                                                <td><?php echo ($result['IsPrivate']? "Yes":"No");?></td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                           <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="orange">
                                    <h4 class="card-title">Color & Background</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                            <th>TextColor</th>
                                            <th>Background</th>
                                        </tr></thead>
                                        <tbody>
                                        <?php  
                                        
                                            $backimg=ImageExist($result['Background']);
                                       
                                        ?>
                                            <tr>
                                               
                                                <td><?php echo $result['TextColor'];?></td>
                                                <td>
                                                <img style='width:70px; height:70px' src='<?php echo $backimg ?>' ></td>
                                                
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    
        <div class="row">
                 <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="rose">
                                    <h4 class="card-title">Social Media Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                                <th> Youtube </th>
                                                <th> Google </th>
                                                <th> Twitter </th>
                                                <th> Instagram </th>
                                                <th> Snapchat </th>
                                                <th> Linked In </th>
                                                <th> TikTok </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                            <tr>
                                                <td><?php echo $result['YoutubeLink'];?></td>
                                                <td><?php echo $result['GoogleLink'];?></td>
                                                <td><?php echo $result['TwitterLink'];?></td>
                                                <td><?php echo $result['InstagramLink'];?></td>
                                                <td><?php echo $result['SnapchatLink'];?></td>
                                                <td><?php echo $result['LinkedinLink'];?></td>
                                                <td><?php echo $result['TiktokLink'];?></td>
                                                 
                                            </tr>

                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                <?php if($type==3) {?>
                <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="purple">
                                    <h4 class="card-title">App Link</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                                <th>Website</th>

                                                <th>Android App Link</th>
                                                <th>iOS AppLink</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                            <tr>
                                                <td><?php echo $result['Website'];?></td>
                                                <td><?php echo $result['AndroidAppLink'];?></td>
                                                <td><?php echo $result['IosAppLink'];?></td>
                                                
                                            
                                                
                                            </tr>
                                            
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
              <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="purple">
                                    <h4 class="card-title">Other Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                                <th>Total Amount</th>
                                                <th>Private Charges</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                            <tr>
                                               
                                                <td><?php echo $result['TotalAmount'];?></td>
                                                <td><?php echo $result['PrivateCharges'];?></td>
                                                
                                            
                                                
                                            </tr>
                                            
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>




        </div>      
        
        
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>