<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <!-- <div class="toolbar">
                            

                        </div> -->

                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#wedding_invitation" data-toggle="tab" aria-expanded="true">Wedding Invitation</a>
                            </li>

                            <!-- <li class="">
                                <a href="#merchant_users" data-toggle="tab" aria-expanded="false">Merchant Users</a>
                            </li> -->

                             <!-- <li class="">
                                <a href="#merchant_not_approved" data-toggle="tab" aria-expanded="false">Merchant Not Approved</a>
                            </li> -->

                            <li class="">
                                <a href="#sms" data-toggle="tab" aria-expanded="false">SMS</a>
                            </li>
                            <li class="">
                                <a href="#app_link" data-toggle="tab" aria-expanded="false">App Link</a>
                            </li>

                            <li class="">
                                <a href="#vcard" data-toggle="tab" aria-expanded="false">V Card</a>
                            </li>

                            <li class="">
                                <a href="#sociallink" data-toggle="tab" aria-expanded="false">Social Link</a>
                            </li>
                           

                        </ul>
                        <div class="tab-content">

                        <div class="material-datatables tab-pane active" id="wedding_invitation">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    
                                    <th> Logo </th>
                                    <th> User Name </th>
                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('is_active');?></th>
                                    <th> Is Private </th>


                                    <?php if(checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){

                                    
                                    foreach($results as $value) if($value['Type']==1){ 
                                        
                                        ?>
                                        <tr id="<?php echo $value['InvitationID']?>">
                                            <td> <img style="width: 70px; height: 70px" src="<?php echo ImageExist($value['Logo']) ?>" ></td>
                                            <td><?php echo $value['FullName'] ?></td>
                                            <td><?php echo $value['Title'] ?></td>


                                            <td><?php echo ($value['IsActive'] ? lang('yes') : lang('no')); ?></td>

                                            <td><?php echo ($value['IsPrivate'] ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanDelete')){?> 
                                            <td>
                                              
                                               
                                                <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanView')){?>
                                                    <a href="<?php echo $ControllerName; ?>/view/<?php echo $value['InvitationID']?>?type=1" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view details<div class="ripple-container"></div></a>
                                                <?php } ?>
                                                <a href="<?php echo base_url(); ?>index/invitation/<?php echo $value['InvitationID']?>" target="_blank" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view QR<div class="ripple-container"></div></a>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                        <!-- end wedding invitaion tab -->

                        <!-- start sms tab-->
                        <div class="material-datatables tab-pane" id="sms"> 
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    
                                    <th> Logo </th>
                                    <th> User Name </th>
                                    <!-- <th><?php echo lang('title');?></th> -->
                                    <th> Mobile # </th>
                                    <th><?php echo lang('is_active');?></th>
                                    <th> Is Private </th>


                                    <?php if(checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){

                                    
                                    foreach($results as $value) if($value['Type']==2){ 
                                        
                                        ?>
                                        <tr id="<?php echo $value['InvitationID']?>">
                                            <td> <img style="width: 70px; height: 70px" src="<?php echo ImageExist($value['Logo']) ?>" ></td>
                                            <td><?php echo $value['FullName'] ?></td>
                                            <!-- <td><?php echo $value['Title'] ?></td> -->
                                            <td><?php echo $value['ContactInfo'] ?></td>


                                            <td><?php echo ($value['IsActive'] ? lang('yes') : lang('no')); ?></td>

                                            <td><?php echo ($value['IsPrivate'] ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanDelete')){?> 
                                            <td>
                                              
                                               
                                                <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanView')){?>
                                                    <a href="<?php echo $ControllerName; ?>/view/<?php echo $value['InvitationID']?>?type=2" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view<div class="ripple-container"></div></a>
                                                <?php } ?>
                                                <a href="<?php echo base_url(); ?>index/invitation/<?php echo $value['InvitationID']?>" target="_blank" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view QR<div class="ripple-container"></div></a>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                        <!-- end sms tab -->

                        <!-- start App Link tab -->
                        <div class="material-datatables tab-pane" id="app_link">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    
                                    <th> Logo </th>
                                    <th> User Name </th>
                                    <!-- <th><?php echo lang('title');?></th> -->
                                    <th> Android App Link </th>
                                    <th> iOS App Link </th>
                                    <th><?php echo lang('is_active');?></th>
                                    <th> Is Private </th>


                                    <?php if(checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){

                                    
                                    foreach($results as $value) if($value['Type']==3){ 
                                        
                                        ?>
                                        <tr id="<?php echo $value['InvitationID']?>">
                                            <td> <img style="width: 70px; height: 70px" src="<?php echo ImageExist($value['Logo']) ?>" ></td>
                                            <td><?php echo $value['FullName'] ?></td>
                                            <!-- <td><?php echo $value['Title'] ?></td> -->
                                            <td><?php echo $value['AndroidAppLink'] ?></td>
                                            <td><?php echo $value['IosAppLink'] ?></td>


                                            <td><?php echo ($value['IsActive'] ? lang('yes') : lang('no')); ?></td>

                                            <td><?php echo ($value['IsPrivate'] ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanDelete')){?> 
                                            <td>
                                              
                                               
                                                <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanView')){?>
                                                    <a href="<?php echo $ControllerName; ?>/view/<?php echo $value['InvitationID']?>?type=3" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view<div class="ripple-container"></div></a>
                                                <?php } ?>
                                                <a href="<?php echo base_url(); ?>index/invitation/<?php echo $value['InvitationID']?>" target="_blank" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view QR<div class="ripple-container"></div></a>
                                            </td>
                                            <?php } ?>
                                            
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                        <!-- end App Link tab -->

                        <!-- start V Card Link tab -->
                        <div class="material-datatables tab-pane" id="vcard">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    
                                    <th> Logo </th>
                                    <th> User Name </th>
                                    <th> First Name </th>
                                    <th> Last Name </th>
                                   <!--  <th><?php echo lang('title');?></th> -->
                                    <th> Mobile # </th>
                                    <th> Email </th>
                                    <th> Birthday </th>
                                    <th><?php echo lang('is_active');?></th>
                                    <th> Is Private </th>


                                    <?php if(checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){

                                    
                                    foreach($results as $value) if($value['Type']==4){ 
                                        
                                        ?>
                                        <tr id="<?php echo $value['InvitationID']?>">
                                            <td> <img style="width: 70px; height: 70px" src="<?php echo ImageExist($value['Logo']) ?>" ></td>
                                            <td><?php echo $value['FullName'] ?></td>
                                            <td><?php echo $value['FirstName'] ?></td>
                                            <td><?php echo $value['LastName'] ?></td>
                                           <!--  <td><?php echo $value['Title'] ?></td> -->
                                            <td><?php echo $value['ContactInfo'] ?></td>
                                            <td><?php echo $value['Email'] ?></td>
                                            <td><?php echo $value['Birthday'] ?></td>


                                            <td><?php echo ($value['IsActive'] ? lang('yes') : lang('no')); ?></td>

                                            <td><?php echo ($value['IsPrivate'] ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanDelete')){?> 
                                            <td>
                                              
                                               
                                                <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanView')){?>
                                                    <a href="<?php echo $ControllerName; ?>/view/<?php echo $value['InvitationID']?>?type=4" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view<div class="ripple-container"></div></a>
                                                <?php } ?>

                                                <a href="<?php echo base_url(); ?>index/invitation/<?php echo $value['InvitationID']?>" target="_blank" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view QR<div class="ripple-container"></div></a>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                        <!-- end V Card tab -->


                        <!-- start Social Link tab -->
                        <div class="material-datatables tab-pane" id="sociallink">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    
                                    <th> Logo </th>
                                    <th> User Name </th>
                                    <th> First Name </th>
                                    <th> Last Name </th>
                                    <th> Email </th>
                                    <th> Birthday </th>
                                    <th><?php echo lang('is_active');?></th>
                                    <th> Is Private </th>


                                    <?php if(checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){

                                    
                                    foreach($results as $value) if($value['Type']==5){ 
                                        
                                        ?>
                                        <tr id="<?php echo $value['InvitationID']?>">
                                            <td> <img style="width: 70px; height: 70px" src="<?php echo ImageExist($value['Logo']) ?>" ></td>
                                            <td><?php echo $value['FullName'] ?></td>
                                            <td><?php echo $value['FirstName'] ?></td>
                                            <td><?php echo $value['LastName'] ?></td>
                                            <td><?php echo $value['Email'] ?></td>
                                            <td><?php echo $value['Birthday'] ?></td>


                                            <td><?php echo ($value['IsActive'] ? lang('yes') : lang('no')); ?></td>

                                            <td><?php echo ($value['IsPrivate'] ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanEdit') || checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanDelete')){?> 
                                            <td>
                                              
                                               
                                                <?php if(checkRightAccess(72,$this->session->userdata['admin']['RoleID'],'CanView')){?>
                                                    <a href="<?php echo $ControllerName; ?>/view/<?php echo $value['InvitationID']?>?type=5" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view<div class="ripple-container"></div></a>
                                                <?php } ?>
                                                <a href="<?php echo base_url(); ?>index/invitation/<?php echo $value['InvitationID']?>" target="_blank" class="btn btn-simple btn-danger btn-icon"><i class="material-icons" >list</i>view QR<div class="ripple-container"></div></a>
                                            </td>
                                            <?php } ?>

                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                        <!-- end Social Link tab -->



                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>