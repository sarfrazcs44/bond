<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


							<div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ActivityID"><?php echo lang('activity_id'); ?></label>
                                        <select name="ActivityID" required  class="form-control" id="ActivityID" onChange="get_max_capacity_by_activityID(this)">
                                    	<option value='0'>Choose Activity</option>
										<?php 
										//print_r($activities);die;
										foreach($activities as $activity){ ?>
											
											<option value="<?php echo $activity->ActivityID;?>"><?php echo $activity->Title;?></option>
										<?php	}?>
										</select>
									</div>
                                </div>
								   <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Maxcap"><?php echo lang('max_capacity'); ?></label>
                                        <input type="number" name="MaxCapacity" required  class="form-control" id="MaxCapacity">
                                    </div>
                                </div>
                               
	</div>
		<hr>
<span style="font-size:22px;">Locations</span>	
<div class="row">
   <div class="col-md-2"></div>
   <div class="col-md-8">
      <div class="table-responsive">
         <table class="table table-bordered">
            <thead>
               <tr>
                  <th class="text-center">Location</th>
                  <th class="text-center">Price</th>
                  <th class="text-center">Remove Row</th>
               </tr>
            </thead>
            <tbody id="tbody"> 
            </tbody>
         </table>
      </div>
      <button class="btn btn-md btn-primary"
         id="addBtn" type="button"> 
      Add new Row 
      </button> 
   </div>
   <div class="col-md-2"></div>
</div>
<hr>
<hr>
<span style="font-size:22px;">Equipment Provided</span>	
<div class="row">
   <div class="col-md-2"></div>
   <div class="col-md-8">
      <div class="table-responsive">
         <table class="table table-bordered">
            <thead>
               <tr>
                  <th class="text-center">Equipment</th>
                  <th class="text-center">Equipment Price</th>
                  <th class="text-center">Remove Row</th>
               </tr>
            </thead>
            <tbody id="tbody1"> 
            </tbody>
         </table>
      </div>
      <button class="btn btn-md btn-primary"
         id="addBtn1" type="button" onclick="get_EqiupmentsByActivity(document.getElementById('ActivityID').value)"> 
      Add new Row 
      </button> 
   </div>
   <div class="col-md-2"></div>
</div>
<hr>
								
                         
                           



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script> 
	$(document).ready(function () { 

	// Denotes total number of rows 
	var rowIdx = 0; 

	// jQuery button click event to add a row 
	$('#addBtn').on('click', function () { 

		// Adding a row inside the tbody. 
		$('#tbody').append(`<tr id="R${++rowIdx}"> 
			<td class="row-index text-center"> 
			<p><input type="text" name="Location[]" class="form-control"/></p> 
			</td> 
			<td class="row-index text-center"> 
			<p><input type="text" name="LocationPrice[]" class="form-control"/></p> 
			</td>
			<td class="text-center"> 
				<button class="btn btn-danger remove"
				type="button">Remove</button> 
				</td> 
			</tr>`); 
	}); 

	// jQuery button click event to remove a row. 
	$('#tbody').on('click', '.remove', function () { 

		// Getting all the rows next to the row 
		// containing the clicked button 
		var child = $(this).closest('tr').nextAll(); 

		// Iterating across all the rows 
		// obtained to change the index 
		child.each(function () { 

		// Getting <tr> id. 
		var id = $(this).attr('id'); 

		// Getting the <p> inside the .row-index class. 
		var idx = $(this).children('.row-index').children('p'); 

		// Gets the row number from <tr> id. 
		var dig = parseInt(id.substring(1)); 

		// Modifying row index. 
		idx.html(`Row ${dig - 1}`); 

		// Modifying row id. 
		$(this).attr('id', `R${dig - 1}`); 
		}); 

		// Removing the current row. 
		$(this).closest('tr').remove(); 

		// Decreasing total number of rows by 1. 
		rowIdx--; 
	}); 
	}); 
	//second table
		$(document).ready(function () { 

	// Denotes total number of rows 
	var rowIdx = 0; 

	// jQuery button click event to add a row 
	$('#addBtn1').on('click', function () { 

		// Adding a row inside the tbody. 
		$('#tbody1').append(`<tr id="R${++rowIdx}"> 
			<td class="row-index text-center"> 
			<p><select  name="Equipment[]" class="form-control equ"></select></p> 
			</td> 
			<td class="row-index text-center"> 
			<p><input type="text" name="EquipmentPrice[]" class="form-control"/></p> 
			</td>
			<td class="text-center"> 
				<button class="btn btn-danger remove"
				type="button">Remove</button> 
				</td> 
			</tr>`); 
	}); 

	// jQuery button click event to remove a row. 
	$('#tbody1').on('click', '.remove', function () { 

		// Getting all the rows next to the row 
		// containing the clicked button 
		var child = $(this).closest('tr').nextAll(); 

		// Iterating across all the rows 
		// obtained to change the index 
		child.each(function () { 

		// Getting <tr> id. 
		var id = $(this).attr('id'); 

		// Getting the <p> inside the .row-index class. 
		var idx = $(this).children('.row-index').children('p'); 

		// Gets the row number from <tr> id. 
		var dig = parseInt(id.substring(1)); 

		// Modifying row index. 
		idx.html(`Row ${dig - 1}`); 

		// Modifying row id. 
		$(this).attr('id', `R${dig - 1}`); 
		}); 

		// Removing the current row. 
		$(this).closest('tr').remove(); 

		// Decreasing total number of rows by 1. 
		rowIdx--; 
	}); 
	});
</script> 