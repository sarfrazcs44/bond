<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="javascript:void(0);" method="post" onsubmit="return false;" class="" enctype="multipart/form-data" data-parsley-validate novalidate>
                           


                            <div class="row">

                               
							<div class="row">

                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ActivityTitle">Activity Title</label>
                                        <input type="text"  required  class="form-control" id="ActivityTitle" value="<?php echo $result['Title'];?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TitleAr">Activity Title Arabic</label>
                                        <input type="text"  required  class="form-control" id="TitleAr" value="<?php echo $result['TitleAr'];?>">
                                    </div>
                                </div>
								 
                               
							</div>
							<div class="row">
								<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Maxcap">Maximum Capacity</label>
                                        <input type="number" name="MaxCapacity" required  class="form-control" id="MaxCapacity" value="<?php echo $result['MaxCapacity'];?>">
                                    </div>
                                </div>
							</div>
<hr>
							<?php 

							$FixturePrice = explode(',',$result['FixturePrice']);
							foreach ($result['Fixture'] as $key => $value) { ?>

								<div class="row">
									<div class="col-md-6">
	                                    <div class="form-group label-floating">
	                                        <label class="control-label" for="<?php echo $key; ?>">Fixture</label>
	                                        <input type="text"  required  class="form-control" id="<?php echo $key; ?>" value="<?php echo $value['FixtureTitle'];?>">
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <div class="form-group label-floating">
	                                        <label class="control-label" for="<?php echo $key.$key; ?>">Fixture Price</label>
	                                        <input type="text"  required  class="form-control" id="<?php echo $key.$key; ?>" value="<?php echo $FixturePrice[$key];?>">
	                                    </div>
	                                </div>

								</div>


								<?php
							}

							?>

<hr>
							<?php 

							$EquipmentPrice = explode(',',$result['EquipmentPrice']);
							foreach ($result['equipments'] as $key => $value) { ?>

								<div class="row">
									<div class="col-md-6">
	                                    <div class="form-group label-floating">
	                                        <label class="control-label" for="<?php echo $key; ?>">Equipment</label>
	                                        <input type="text"  required  class="form-control" id="<?php echo $key; ?>" value="<?php echo $value['EquipmentTitle'];?>">
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <div class="form-group label-floating">
	                                        <label class="control-label" for="<?php echo $key.$key; ?>">Equipment Price</label>
	                                        <input type="text"  required  class="form-control" id="<?php echo $key.$key; ?>" value="<?php echo $EquipmentPrice[$key];?>">
	                                    </div>
	                                </div>

								</div>


								<?php
							}

							?>


							
								
								


                          
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script> 
	$(document).ready(function () { 

	// Denotes total number of rows 
	var rowIdx = 0; 

	// jQuery button click event to add a row 
	$('#addBtn').on('click', function () { 

		// Adding a row inside the tbody. 
		$('#tbody').append(`<tr id="R${++rowIdx}"> 
		
			<td class="row-index text-center"> 
			<p><input type="text" name="Location[]" class="form-control" /></p> 
			
			</td> 
			<td class="row-index text-center"> 
		
			<p><input type="text" name="LocationPrice[]" class="form-control" /></p> 
			
			</td><td class="text-center"> 
				<button class="btn btn-danger remove"
				type="button">Remove</button> 
				
			
					</td> 
			</tr>`); 
	}); 

	// jQuery button click event to remove a row. 
	$('#tbody').on('click', '.remove', function () { 

		// Getting all the rows next to the row 
		// containing the clicked button 
		var child = $(this).closest('tr').nextAll(); 

		// Iterating across all the rows 
		// obtained to change the index 
		child.each(function () { 

		// Getting <tr> id. 
		var id = $(this).attr('id'); 

		// Getting the <p> inside the .row-index class. 
		var idx = $(this).children('.row-index').children('p'); 

		// Gets the row number from <tr> id. 
		var dig = parseInt(id.substring(1)); 

		// Modifying row index. 
		idx.html(`Row ${dig - 1}`); 

		// Modifying row id. 
		$(this).attr('id', `R${dig - 1}`); 
		}); 

		// Removing the current row. 
		$(this).closest('tr').remove(); 

		// Decreasing total number of rows by 1. 
		rowIdx--; 
	}); 
	}); 
	//second table
		$(document).ready(function () { 

	// Denotes total number of rows 
	var rowIdx = 0; 

	// jQuery button click event to add a row 
	$('#addBtn1').on('click', function () { 

		// Adding a row inside the tbody. 
		$('#tbody1').append(`<tr id="R${++rowIdx}"> 
			<td class="row-index text-center"> 
			<p><select  name="Equipment[]" class="form-control equ"></select></p> 
			</td> 
			<td class="row-index text-center"> 
			<p><input type="text" name="EquipmentPrice[]" class="form-control"/></p> 
			</td>
			<td class="text-center"> 
				<button class="btn btn-danger remove"
				type="button">Remove</button> 
				</td> 
			</tr>`); 
	}); 

	// jQuery button click event to remove a row. 
	$('#tbody1').on('click', '.remove', function () { 

		// Getting all the rows next to the row 
		// containing the clicked button 
		var child = $(this).closest('tr').nextAll(); 

		// Iterating across all the rows 
		// obtained to change the index 
		child.each(function () { 

		// Getting <tr> id. 
		var id = $(this).attr('id'); 

		// Getting the <p> inside the .row-index class. 
		var idx = $(this).children('.row-index').children('p'); 

		// Gets the row number from <tr> id. 
		var dig = parseInt(id.substring(1)); 

		// Modifying row index. 
		idx.html(`Row ${dig - 1}`); 

		// Modifying row id. 
		$(this).attr('id', `R${dig - 1}`); 
		}); 

		// Removing the current row. 
		$(this).closest('tr').remove(); 

		// Decreasing total number of rows by 1. 
		rowIdx--; 
	}); 
	});
</script> 