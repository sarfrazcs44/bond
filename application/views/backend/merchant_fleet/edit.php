<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="#" method="post"  class="" enctype="multipart/form-data" data-parsley-validate novalidate>
                           


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MerchantName">Merchant Name</label>
                                        <input type="text" name="MerchantName" required  class="form-control" id="MerchantName" value="<?php echo $result['MerchantName']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TitleAr">Boat Type</label>
                                        <input type="text" name="Title" required  class="form-control" id="Title" value="<?php echo $result['Title']; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="LicenseNo">License No</label>
                                        <input type="text" name="LicenseNo" required  class="form-control" id="LicenseNo" value="<?php echo $result['LicenseNo']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TitleAr">Maximum Capacity</label>
                                        <input type="text" name="MaximumCapacity" required  class="form-control" id="MaximumCapacity" value="<?php echo $result['MaximumCapacity']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Fleet Documents</label>
                                    <ul>
                                  <?php 
                                  $i=1;
                                    if($result['FleetDocuments']){
                                  foreach($result['FleetDocuments'] as $doc){?>
                                  
                                <li>  <a  target="_blank" href="<?php echo base_url($doc->ImageName);?>">Document# <?php echo $i;?></a></li>
                                    <?php $i++;}} ?>
                                  </ul>
                                </div>
                            </div>
                           
                        </div> 

                           
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>