          <?php
			  
			 
			  ?>
	<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">perm_identity</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">View Profile -
                                    
                                    </h4>
                                    <form>
								
                                        <div class="row">
											<?php if(!empty($result->RoleID)){?>
										
											 <div class="col-md-5">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">User Name</label>
                                                    <input type="text" class="form-control" disabled value="<?php echo $result->FullName;?>">
                                                </div>
                                            </div>
											
									<?php	 } if(!empty($result->CompanyName)){?>
		
                                            <div class="col-md-3">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Company Name</label>
                                                    <input type="text" class="form-control"  disabled value="<?php echo $result->CompanyName;?>">
                                                </div>
									</div>							
									<?php } if(!empty($result->Email)){?>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Email address</label>
                                                    <input type="email" class="form-control" disabled value="<?php echo $result->Email;?>">
                                                </div>
                                            </div>
									<?php } ?>
                                        </div>
										<?php  if(!empty($result->Dob)){?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Date of Birth</label>
                                                    <input type="text" class="form-control" disabled value="<?php echo $result->Dob;?>">
                                                </div>
                                            </div>
                                           
                                        </div>
                                        <?php }?>
                                        <div class="row">
                                            <?php if(!empty($result->Mobile)){ ?>
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Mobile #</label>
                                                    <input type="text" class="form-control" disabled value="<?php echo $result->Mobile;?>">
                                                </div>
                                            </div>
                                            <?php } ?>
                                        
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Gender</label>
                                                    <input type="text" class="form-control" disabled value="<?php echo $result->Gender;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                       
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Email Verified</label>
                                                    <input type="text" class="form-control" disabled value="<?php echo ($result->IsEmailVerified? 'Yes':'No') ?>">
                                                </div>
                                            </div>

                                        
                                        <?php if(!empty($result->Mobile)){?>

                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Mobile Verified</label>
                                                    <input type="text" class="form-control" disabled value="<?php echo ($result->IsMobileVerified? 'Yes':'No') ?>">
                                                </div>
                                            </div>

                                           <?php } ?>

                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Device Type</label>
                                                    <input type="text" class="form-control" disabled value="<?php echo $result->DeviceType;?>">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                        <?php  if(!empty($userdoc)){?>
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">User Documents</label>
													<ul>
                                                  <?php 
												  $i=1;
												    if($userdoc){
												  foreach($userdoc as $doc){?>
												  
												<li>  <a  target="_blank" href="<?php echo base_url($doc->ImageName);?>">Document# <?php echo $i;?></a></li>
													<?php $i++;}} ?>
												  </ul>
                                                </div>
                                            </div>
                                           
										<?php }  ?>
                                        </div>  
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-profile">
                                <div class="card-avatar">
                                    <a href="#pablo">
                                        <img class="img" src="<?php echo ImageExist($result->Image);?>" />
                                    </a>
                                </div>
								<?php if(!empty($result->Bio)){?>
                                <div class="card-content">
                                    <h6 class="category text-gray">About</h6>
                                    <h4 class="card-title"></h4>
                                    <p class="description">
                                       <?php echo $result->Bio;?>
                                    </p>
                                    <p>Online Status : <?php echo $result->OnlineStatus;?></p>
                                  
                                                  
                                </div>
								<?php }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
