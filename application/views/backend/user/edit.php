<?php
$option = '';
if (!empty($districts)) {
    foreach ($districts as $district) {
        $option .= '<option value="' . $district->DistrictID . '" ' . ((isset($result[0]->DistrictID) && $result[0]->DistrictID == $district->DistrictID) ? 'selected' : '') . '>' . $district->Title . ' </option>';
    }
}


?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Add Backend User</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data" autocomplete="off"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="UserID" value="<?php echo base64_encode($result->UserID); ?>">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label"
                                               for="RoleID"><?php echo lang('choose_user_role'); ?> *</label>
                                        <select id="RoleID" class="selectpicker" data-style="select-with-transition"
                                                required name="RoleID">

                                            <?php if (!empty($roles)) {
                                                foreach ($roles as $role) { ?>
                                                    <option value="<?php echo $role->RoleID; ?>" <?php echo ($role->RoleID == $result->RoleID ? 'selected' : ''); ?>><?php echo $role->Title; ?> </option>
                                                <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('name'); ?></label>
                                        <input type="text" name="Title" parsley-trigger="change" required
                                               class="form-control" id="Title" value="<?php echo $result->FullName; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Email"><?php echo lang('email'); ?></label>
                                        <input type="text" name="Email" parsley-trigger="change" required
                                               class="form-control" id="Email" value="<?php echo $result->Email; ?>">
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Mobile">Mobile No.</label>
                                        <input type="text" name="Mobile" required class="form-control" id="Mobile" value="<?php echo $result->Mobile; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                       <?php echo ($result->IsActive == 1 ? 'checked' : ''); ?>/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        <?php echo lang('submit'); ?>
                                    </button>
                                    <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                            <?php echo lang('back'); ?>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>