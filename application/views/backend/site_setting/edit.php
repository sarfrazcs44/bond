<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('EditSiteSettings'); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/site_setting/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="SiteSettingID" value="<?php echo $SiteSettingID; ?>">


                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SiteName">Title  </label>
                                        <input type="text" class="form-control" name="SiteName" id="SiteName" required value="<?php echo $result->SiteName; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="PhoneNumber">Contact No. </label>
                                        <input type="text" class="form-control" name="PhoneNumber" id="PhoneNumber" value="<?php echo $result->PhoneNumber; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Email"><?php echo lang('email'); ?> </label>
                                        <input type="email" class="form-control" name="Email" id="Email" value="<?php echo $result->Email; ?>">
                                    </div>
                                </div>
                                <!-- <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Whatsapp"><?php echo lang('Whatsapp'); ?> </label>
                                        <input type="text" class="form-control" name="Whatsapp" id="Whatsapp" value="<?php echo $result->Whatsapp; ?>">
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Skype"><?php echo lang('Skype'); ?> </label>
                                        <input type="text" class="form-control" name="Skype" id="Skype" value="<?php echo $result->Skype; ?>">
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Fax"><?php echo lang('Fax'); ?> </label>
                                        <input type="text" class="form-control" name="Fax" id="Fax" value="<?php echo $result->Fax; ?>">
                                    </div>
                                </div> -->
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="ProductDisclaimer">Product Disclaimer</label>
                                        <textarea class="form-control textarea" name="ProductDisclaimer" id="ProductDisclaimer" style="height: 100px;"><?php echo $result->ProductDisclaimer; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="PromoCodeDisclaimer">Promo Code Disclaimer</label>
                                        <textarea class="form-control textarea" name="PromoCodeDisclaimer" id="PromoCodeDisclaimer" style="height: 100px;"><?php echo $result->PromoCodeDisclaimer; ?></textarea>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <?php if ($result->SiteImage != '') { ?>
                                        <img src="<?php echo base_url($result->SiteImage); ?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                    <br>
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Image"><?php echo lang('SiteLogo'); ?> </label>
                                        <input type="file" class="filestyle" id="Image" name="Image[]" data-placeholder="No Image" accept="image/*" style="height: 450px">
                                    </div>
                                </div>
                            </div>

                            <hr/>

                            <!--<div class="row"> -->
                                <!--<div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="OpenTime">Open Time <small style="color: red;">(GMT Time)</small></label>
                                        <input type="text" class="form-control my_timepicker" name="OpenTime" id="OpenTime" value="<?php /*echo date('h:i A', $result->OpenTime); */?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CloseTime">Close Time <small style="color: red;">(GMT Time)</small></label>
                                        <input type="text" class="form-control my_timepicker" name="CloseTime" id="CloseTime" value="<?php /*echo date('h:i A', $result->CloseTime); */?>">
                                    </div>
                                </div>-->
                                <!-- <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="OrderCancelDays">Order Automatically Cancel After Days </label>
                                        <input type="text" class="form-control" name="OrderCancelDays" id="OrderCancelDays" value="<?php echo $result->OrderCancelDays; ?>" min="1">
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="OrderDisputedCancelDays">Order Automatically Mark As Disputed Cancelled After Days </label>
                                        <input type="text" class="form-control" name="OrderDisputedCancelDays" id="OrderDisputedCancelDays" value="<?php echo $result->OrderDisputedCancelDays; ?>" min="1">
                                    </div>
                                </div> -->
                                <!--<div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="VatNumber">VAT Number </label>
                                        <input type="text" class="form-control" name="VatNumber" id="VatNumber" value="<?php /*echo $result->VatNumber; */?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="VatPercentage">VAT Percentage </label>
                                        <input type="text" class="form-control number-with-decimals" name="VatPercentage" id="VatPercentage" value="<?php /*echo $result->VatPercentage; */?>">
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="LoyaltyFactor">Loyalty Factor <small style="color:red;">(Defines 1 SAR is equal to how many loyalty points)</small> </label>
                                        <input type="text" class="form-control number-with-decimals" name="LoyaltyFactor" id="LoyaltyFactor" value="<?php echo $result->LoyaltyFactor; ?>">
                                    </div>
                                </div>-->
                                <!-- <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="InvitePoints">Invite Points <small style="color:red;">(No of points a user will get on invitee signup)</small> </label>
                                        <input type="text" class="form-control number-only" name="InvitePoints" id="InvitePoints" value="<?php echo $result->InvitePoints; ?>">
                                    </div>
                                </div> -->
                            <!--</div> -->

                            

                            <div class="row">
                                <h5>Please add username of accounts only.</h5>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FacebookUrl">Facebook </label>
                                        <input type="text" class="form-control" name="FacebookUrl" id="FacebookUrl" value="<?php echo $result->FacebookUrl; ?>">
                                    </div>
                                </div>
                               <!-- <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="GoogleUrl"><?php echo lang('GoogleUrl'); ?> </label>
                                        <input type="text" class="form-control" name="GoogleUrl" id="GoogleUrl" value="<?php echo $result->GoogleUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="LinkedInUrl"><?php echo lang('LinkedInUrl'); ?> </label>
                                        <input type="text" class="form-control" name="LinkedInUrl" id="LinkedInUrl" value="<?php echo $result->LinkedInUrl; ?>">
                                    </div>
                                </div>-->
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="InstagramUrl">Instagram </label>
                                        <input type="text" class="form-control" name="InstagramUrl" id="InstagramUrl" value="<?php echo $result->InstagramUrl; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TwitterUrl">Twitter </label>
                                        <input type="text" class="form-control" name="TwitterUrl" id="TwitterUrl" value="<?php echo $result->TwitterUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SnapChatUrl">YouTube</label>
                                        <input type="text" class="form-control" name="SnapChatUrl" id="SnapChatUrl" value="<?php echo $result->SnapChatUrl; ?>">
                                    </div>
                                </div>
                            </div>
                            <hr/>

                           <!--  <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <a class="btn btn-success btn-sm" onclick="verifyAllUsers();">
                                        Verify All Users
                                        <div class="ripple-container"></div>
                                    </a>
                                </div>
                            </div> -->

                            <div class="form-group label-floating">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        <?php echo lang('submit'); ?>
                                    </button>



                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    function verifyAllUsers() {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: base_url + '' + 'cms/user/verifyAllUsers',
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            /*setTimeout(function () {
                                window.location.reload();
                            }, 1000);*/
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }
</script>
<script src="<?php echo base_url();?>assets/backend/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/tinymce_custom.js"></script>