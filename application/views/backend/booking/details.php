<div class="content">
    <div class="container-fluid">

<?php //echo "<pre>";print_r($result);?>
        <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="orange">
                                    <h4 class="card-title">Customer Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                        </tr></thead>
                                        <tbody>
                                            <tr>
                                               
                                                <td><?php echo $result['CustomerFullName'];?></td>
                                                <td><?php echo $result['CustomerEmail'];?></td>
                                                <td><?php echo $result['CustomerMobile'];?></td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                           <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="blue">
                                    <h4 class="card-title">Merchant Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
											<th>Company</th>
                                        </tr></thead>
                                        <tbody>
                                            <tr>
                                               
                                                <td><?php echo $result['MerchantFullName'];?></td>
                                                <td><?php echo $result['MerchantEmail'];?></td>
                                                <td><?php echo $result['MerchantMobile'];?></td>
												 <td><?php echo $result['CompanyName'];?></td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
					
		<div class="row">
			     <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="rose">
                                    <h4 class="card-title">Equipment Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                            <th> Name</th>
                                            <th> Quantity</th>
											<th>Price</th>
                                            
                                        </tr></thead>
                                        <tbody>
                                          
                                               <?php 
											   $qty=explode (",", $result['EquipmentQuantity']);
											   $price=explode (",", $result['EquipmentPrice']);
											  $namearray=$result['equipmentsNames'];
											// print_rm( $namearray);
											   foreach($namearray as $key => $eqname){
												   // print_r($key);
												   
												   ?>
                                                 <tr> <td><?php echo  $eqname['EquipmentTitle'] ?></td>
                                              
                                                <td><?php echo  $qty[$key] ?></td> 
                                               
												<td><?php echo  $price[$key] ?></td></tr>
                                                <?php }  ?>
                                            
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
			  <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text" data-background-color="purple">
                                    <h4 class="card-title">Fixture Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                            <th>Interval</th>
                                          
											<th>Price</th>
                                            
                                        </tr></thead>
                                        <tbody>
										<?php 
											 
											   $LocationPrice=explode (",", $result['FixturePrice']);
											 
											
											   foreach($result['Fixture'] as $key => $value){
												    //print_r($locdata);
												   
												   ?>
                                            <tr>
                                               
                                                <td><?php echo $value['FixtureTitle'];?></td>
                                                <td><?php echo $LocationPrice[$key];?></td> 
											
                                                
                                            </tr>
											  <?php }  ?>
                                            
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
		</div>		
		
		<div class="row">
			     <div class="col-lg-12 col-md-12">
				  <div class="card">
                                <div class="card-header card-header-text" data-background-color="red">
                                    <h4 class="card-title">Transaction  Details</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <tr>
                                            <th>Tax</th>
                                          
											<th>Total</th>
                                            <th>Grand Total</th>
                                        </tr></thead>
                                        <tbody>
								
                                            <tr>
                                               
                                                <td><?php echo $result['Tax']?></td>
                                                <td><?php echo $result['Total'];?></td> 
											<td><?php echo $result['GrandTotal'];?></td> 
                                                
                                            </tr>
										
                                            
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
				 
				 
				</div>
				
		</div>		
		
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>