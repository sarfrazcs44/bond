<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th>Customer Name</th>
                                    <th>Merchant Name</th>
                                    <th>Check In Time</th>
                                    <th>Check Out Time</th>

                                    
                                    <th>Booking Status</th>
                                    <th>Created At</th>


                                    <?php if(checkRightAccess(67,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(67,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value['BookingID'];?>">

                                            <td><?php echo $value['CustomerFullName']; ?></td>
                                            <td><?php echo $value['MerchantName']; ?></td>
                                            <td><?php echo ($value['CheckInTime'] == '' ? 'N/A' : date('d-m-Y H:i',$value['CheckInTime'])); ?></td>
                                            <td><?php echo ($value['CheckOutTime'] == '' ? 'N/A' : date('d-m-Y H:i',$value['CheckOutTime'])); ?></td>


                                            
                                            <td><?php echo $value['BookingStatus']; ?></td>
                                            <td><?php echo $value['CreatedAt']; ?></td>

                                             <?php if(checkRightAccess(67,$this->session->userdata['admin']['RoleID'],'CanEdit') || checkUserRightAccess(67,$this->session->userdata['admin']['RoleID'],'CanDelete')){?> 
												<td>
											
                 <a href="<?php echo base_url('cms/'.$ControllerName.'/details/'.$value['BookingID']);?>" >View</a>

                                                
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>