<?php include 'layouts/header.php'; ?>
<?php foreach($results as $result) {	
?>
        <div class="brandPage d-flex align-items-center justify-content-center text-center" style="
        <?php echo($result['IsBackgroundImage']=0? 'background-color:'.$result['Background']:'background-image: url('.ImageExist($result['Background']).')' ) ?>">
        	
        ">
            <div class="container">
            	
                <div class="logoArea"><img src="<?php echo ImageExist($result['Logo']); ?>" alt="" height="20%" width="20%"></div>
                <h1><?php echo $result['Title'] ?></h1>
                <p><?php echo $result['Description']; ?></p>
            </div>
        </div>
<?php } ?>
<?php include 'layouts/footer.php'; ?>