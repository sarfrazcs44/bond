<?php include 'layouts/header.php'; ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
<link href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<?php $user = $user[0]; ?>

        <div class="d-flex align-items-center justify-content-center text-center" >
            <div class="container pb-5">
                <div class="topBar d-flex align-items-center justify-content-between">
                    <h2 class="text-white my-2">Create a Wedding Invitation</h2>
                    <img src="<?php echo base_url(); ?>assets/frontend/images/logoQR.png" class="img-fluid" alt="">
                </div>
                <div class="card p-4 mb-5 d-none">
                    <h1 class="headingEdCard">Enter Details</h1>
                    <div class="showingImgBox shadow px-5 mb-5" style="background-color: #fff; background-image: url('<?php echo base_url(); ?>assets/frontend/images/vector.jpg');">
                        <div class="textBox_one mt-5 mb-3 mx-auto"></div>
                        <div class="textBox_two d-flex align-items-center justify-content-center">Front View</div>
                        <div class="textBox_three mb-5 mt-3 mx-auto"></div>
                    </div>
                  
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <label for="txtLogo" class="form-control px-3 rounded-pill pointer shadhow m-0 text-left">Upload Wedding Logo (Optional)</label>
                                <input type="file" class="invisible position-absolute" id="txtLogo" name="txtLogo" placeholder="" style="top: 0; ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtTitle" name="txtTitle" placeholder="Title of invitation" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <label for="fileColor" class="form-control px-3 rounded-pill pointer shadhow m-0 text-left iconInBg">Select Background Color</label>
                                <input type="color" class="invisible position-absolute" id="fileColor" name="fileColor" placeholder="" style="top: 0; ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtHostName" name="txtHostName" placeholder="Enter Host Name" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                               <textarea class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtDescp" name="txtDescp" placeholder="Enter invitation Description and Venue" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtAAAAAAAAAA" name="txtAAAAAAAAAA" placeholder="Enter Contact Information" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <label for="txtLink" class="form-control px-3 rounded-pill pointer shadhow m-0 text-left iconInBg">Audio mp3 Youtube Link (optional)</label>
                                <input type="file" class="invisible position-absolute" id="txtLink" name="txtLink" placeholder="" style="top: 0; ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">



                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtGoogleMap" name="txtGoogleMap" placeholder="Google map Location Share link" >
                            </div>
                        </div>
                    </div>


                    <button type="button" class="btn edBtn rounded-pill w-100 mt-3">Proceed to next</button>
                </div>
                
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <!-- <div class="block-box user-about">
                        <div class="widget-heading d-flex align-items-center justify-content-between">
                            <h1 class="headingEdCard">Hobbies and Interests</h1>
                            <div class="dropdown">
                                <button class="btn btn-secondary headingEdCard border-0 p-0 bg-transparent" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                              </div>
                           
                        </div>
                        <ul class="user-info">
                            <li>
                                <label>My Hobbies:</label>
                                <p>when an unknown printer took a galley of type and scrambledmake a type specimen It has survived not only five centuries</p>
                            </li>
                            <li>
                                <label>Favourite Music Bands/Artists:</label>
                                <p>Iron Maid, DC/AC, Megablow, The Ill, Kung Fighters, System of a Revenge.</p>
                            </li>
                            <li>
                                <label>Favourite TV Shows:</label>
                                <p>Breaking Good, RedDevil, People of Interest, The Running Dead, Found,</p>
                            </li>
                            <li>
                                <label>Favourite Books:</label>
                                <p>The Crime of the Century, Egiptian Mythology 101, The Scarred Wizard, Lord of the Wings, Amongst Gods, The Oracle, A Tale of Air and Water.</p>
                            </li>
                            <li>
                                <label>Favourite Movies:</label>
                                <p>Idiocratic, The Scarred Wizard and the Fire Crown, Crime Squad, Ferrum Man.</p>
                            </li>
                            <li>
                                <label>Other Activities:</label>
                                <p>Swimming, Surfing, Scuba Diving, Anime, Photography, Tattoos, Street Art.</p>
                            </li>
                        </ul>
                    </div> -->
                    <!-- <div class="block-box user-about">
                        <div class="widget-heading d-flex align-items-center justify-content-between">
                            <h1 class="headingEdCard">Education and Employment</h1>
                            <div class="dropdown">
                                <button class="headingEdCard border-0 p-0 bg-transparent" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Close</a>
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </div>
                        <ul class="user-info">
                            <li>
                                <label>Education:</label>
                                <p>The New College of Design</p>
                            </li>
                            <li>
                                <label>Institution:</label>
                                <p>Radiustheme.com</p>
                            </li>
                            <li>
                                <label>Employment:</label>
                                <p>UI/UX Designer</p>
                            </li>
                            <li>
                                <label>Year:</label>
                                <p>2008-2020</p>
                            </li>
                        </ul>
                    </div> -->
                    <div class="block-box user-about">
                        <div class="widget-heading d-flex align-items-center justify-content-between">
                            <h1 class="headingEdCard">Contact Info</h1>
                            <div class="dropdown">
                                <button class="headingEdCard border-0 p-0 bg-transparent" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Close</a>
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </div>
                        <ul class="user-info">
                            <li>
                                <label>Fist Name:</label>
                                <p><?php echo $user['FirstName'] ?></p>
                            </li>
                            <li>
                                <label>Last Name:</label>
                                <p><?php echo $user['LastName'] ?></p>
                            </li>
                            <li>
                                <label>Username:</label>
                                <p><?php echo $user['UserName'] ?></p>
                            </li>
                            <li>
                                <label>Phone:</label>
                                <p><?php echo $user['ContactInfo'] ?></p>
                            </li>
                            <li>
                                <label>Company:</label>
                                <p><?php echo $user['Company'] ?></p>
                            </li>
                            <li>
                                <label>Phone Number Verified:</label>
                                <p><?php echo ($user['IsMobileVerified']=1? "Yes":"No") ?></p>
                            </li>
                            <li>
                                <label>Address:</label>
                                <p><?php echo $user['Address'] ?></p>
                            </li>
                            <li>
                                <label>Website:</label>
                                <p><a href="<?php echo $user['Website'] ?>"><?php echo $user['Website'] ?></a></p>
                            </li> 
                            <li>
                                <label>Total Amount:</label>
                                <p><?php echo $user['TotalAmount'] ?></p>
                            </li> 
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 widget-block widget-break-lg">
                    <div class="widget widget-user-about">
                        <div class="widget-heading d-flex align-items-center justify-content-between">
                            <h1 class="headingEdCard">About Me</h1>
                            <div class="dropdown">
                                <button class="headingEdCard border-0 p-0 bg-transparent" type="button" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Close</a>
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </div>
                        <div class="user-info">
                            <p><?php echo $user['Bio'] ?></p>
                            <ul class="info-list">
                                <li><span>Joined:</span><?php echo date("m/d/Y h:i:s A T",$user['CreatedAt']) ?></li>
                                <li><span>E-mail:</span><?php echo $user['Email'] ?></li>
                                
                                
                                <li><span>Address:<?php echo $user['Address'] ?></span></li> 
                                
                                 <li class="social-share"><span>Social:</span>
                                    <span><div class="social-icon">
                                        <a href="<?php echo $user['FacebookLink'] ?>"><i class="fa fa-facebook"></i><?php echo $user['FacebookLink'] ?></a>
                                        <a href="<?php echo $user['TwitterLink'] ?>"><i class="fa fa-twitter"></i><?php echo $user['TwitterLink'] ?></a>
                                        <a href="<?php echo $user['SnapchatLink'] ?>"><i class="fa fa-snapchat"></i><?php echo $user['SnapchatLink'] ?></a>
                                        <a href="<?php echo $user['LinkedinLink'] ?>"><i class="fa fa-linkedin"></i><?php echo $user['LinkedinLink'] ?></a>
                                        <a href="<?php echo $user['InstagramLink'] ?>"><i class="fa fa-instagram"></i><?php echo $user['InstagramLink'] ?></a>
                                        <a href="<?php echo $user['TiktokLink'] ?>"><i class="fa fa-tiktok"></i><?php echo $user['TiktokLink'] ?></a>

                                    </div></span>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

<?php include 'layouts/footer.php'; ?>