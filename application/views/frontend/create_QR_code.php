<?php include 'layouts/header.php'; ?>

  <div class="d-flex align-items-center justify-content-center text-center">
    <div class="container pb-5">
      <div class="topBar d-flex align-items-center justify-content-between">
        <h2 class="text-white my-2">Create a Wedding Invitation</h2>
        <img src="<?php echo base_url()?>assets/frontend/images/logoQR.png" class="img-fluid" alt="">
      </div>
      <div class="card p-4 mb-5">
        <h1 class="headingEdCard">Create QR Code</h1>
        <div class="row">
          <div class="col-md-6">
            <div class="radioButtonsSelected d-flex align-itens-center justify-content-between">
              <label class="RDO_container">
                <input type="radio" checked="checked" name="radio"><i class="position-relative">Free Version</i>
                <span class="checkmark"></span>
              </label>
              <label class="RDO_container">
                <input type="radio" name="radio"><i class="position-relative">Paid (15 SAR)</i>
                <span class="checkmark"></span>
              </label>
            </div>
            <div class="paymentDetails text-center border-0">
              Design the QR Code
            </div>
            <div class="text-center">
              <div class="imgBox my-3"><img src="<?php echo base_url()?>assets/frontend/images/qrCode.png" alt=""></div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-md-6">
            <div class="form-group mt-3 position-relative">
              <label for="fileColor"
                class="form-control px-3 rounded-pill pointer shadhow m-0 text-left iconInBg">Select QR Color</label>
              <input type="color" class="invisible position-absolute" id="fileColor" name="fileColor" placeholder=""
                style="top: 0; ">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group mt-3 position-relative">
              <label for="txtFile" class="form-control px-3 rounded-pill pointer shadhow m-0 text-left ">Upload logo in
                middle(Optional) </label>
              <input type="file" class="invisible position-absolute" id="txtFile" name="txtFile" placeholder=""
                style="top: 0; ">
            </div>
            <button type="button" class="btn edBtn rounded-pill w-100 mt-3">Total 200.00 SAR - Create QR</button>
          </div>
        </div>
      </div>

    </div>
  </div>

<?php include 'layouts/footer.php'; ?>
