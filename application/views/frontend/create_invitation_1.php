<?php include 'layouts/header.php'; ?>
        <div class="d-flex align-items-center justify-content-center text-center" >
            <div class="container pb-5">
                <div class="topBar d-flex align-items-center justify-content-between">
                    <h2 class="text-white my-2">Create a Wedding Invitation</h2>
                    <img src="<?php echo base_url(); ?>assets/frontend/images/logoQR.png" class="img-fluid" alt="">
                </div>
                <div class="card p-4 mb-5">
                    <h1 class="headingEdCard">Select Background</h1>
                    <div class="showingImgBox shadow" style="background-color: #fff; background-image: url('<?php echo base_url(); ?>assets/frontend/images/vector.jpg');">
                        <!-- <div class="logoArea"><img src="images/logo.png" alt="" height="" width=""></div>
                        <h1>Ahmad Alharbi</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
                    </div>
                    <div class="selectImgs d-flex align-items-start justify-content-start my-4">
                        <div class="singleImgBox mr-2">
                            <div class="imgBox shadow d-flex align-items-center justify-content-center" style="background: linear-gradient(340deg, #008280, #004339);">+</div>
                            <p class="m-0">Add Yours</p>
                        </div>
                        <div class="singleImgBox mr-2">
                            <div class="imgBox shadow active" style="background-color: #fff; background-image: url('<?php echo base_url(); ?>assets/frontend/images/vector.jpg');"></div>
                            <p class="m-0">Solid Color</p>
                        </div>
                        <div class="singleImgBox mr-2">
                            <div class="imgBox shadow" style="background-color: #fff; background-image: url('<?php echo base_url(); ?>assets/frontend/images/vector.jpg');"></div>
                            <p class="m-0">Silk</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <label for="fileColor" class="form-control px-3 rounded-pill pointer shadhow m-0 text-left iconInBg">Select Background Color</label>
                                <input type="color" class="invisible position-absolute" id="fileColor" name="fileColor" placeholder="" style="top: 0; ">
                            </div>
                            <button type="button" class="btn edBtn rounded-pill w-100 mt-3">200.00 SAR - Proceed to next</button>
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
<?php include 'layouts/footer.php'; ?>
