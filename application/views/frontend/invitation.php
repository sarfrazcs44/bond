<!DOCTYPE html>
<html>
    <head>
        <title>QR Code</title>
        <link rel="icon" href="<?php echo base_url(); ?>assets/frontend/images/logoQR.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frontend/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frontend/css/style.css') ?>">

    </head>
    <?php $result = $results[0]; ?>
    <body class="bodyPage showingImgBox" style="<?php echo($result['IsBackgroundImage']=0? 'background-color:'.$result['Background']:'background-image: url('.ImageExist($result['Background']).')' ) ?>; background-repeat: no-repeat; background-position: center; background-size: cover;">
        <div class="min-100vh d-flex align-items-center" >
            <div class="container">
                <div class="col-md-6 mx-auto text-center">
                    <div class="textBox_one mt-5 mb-3 mx-auto bg-transparent rounded-0">
                        <img src="<?php echo $result['QRUrl']?>" class="img-fluid" alt="" width="200">
                    </div>
                    <h1 class="headingEdCard text-dark"><?php echo $result['Title'] ?></h1>
                    <p class="font-weight-bold mb-3"><?php echo $result['Description']; ?></p>
                    <p class="font-weight-bold"><?php echo $result['ContactInfo']; ?></p>
                </div>
            </div>
        </div>
        <!-- <div class="locationIcon"><a href="<?php echo ($result['GoogleLink']==''? 'https://www.google.com/maps/place/Saudi+Arabia/@24.0230563,40.5702127,6z/data=!3m1!4b1!4m5!3m4!1s0x15e7b33fe7952a41:0x5960504bc21ab69b!8m2!3d23.885942!4d45.079162' : "'".$result['GoogleLink']."'")?>" target="_blank"><img src="<?php echo base_url(); ?>assets/frontend/images/map.png" alt=""></a></div> -->

        <div class="locationIcon"><a href="<?php echo $result['GoogleLink']?>" target="_blank"><img src="<?php echo base_url(); ?>assets/frontend/images/map.png" alt=""></a></div>

        <div class="playIcon">
            <a href="javascript:void(0);" id="playAudio"><img src='<?php echo base_url(); ?>assets/frontend/images/play.png' alt=''></a>
            <audio id="testAudio" hidden src="<?php echo $result['AudioLink']; ?>" type="audio/mpeg">
            </audio>
            <!-- <button>Play</button> -->
        </div>

        <div class="youtubeIcon"><a href="<?php echo $result['YoutubeLink']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/frontend/images/youtube.png" alt=""></a></div>
    <script>
document.getElementById("playAudio").addEventListener("click", function(){
	var audio = document.getElementById('testAudio');
  if(this.className == 'is-playing'){
    this.className = "";
    this.innerHTML = "<img src='<?php echo base_url(); ?>assets/frontend/images/play.png' alt=''>"
    audio.pause();
  }else{
    this.className = "is-playing";
    this.innerHTML = "<img src='<?php echo base_url(); ?>assets/frontend/images/pause.png' alt=''>";
    audio.play();
  }

});
    </script>
    </body>
</html>