<?php include 'layouts/header.php'; ?>

        <div class="d-flex align-items-center justify-content-center text-center" >
            <div class="container pb-5">
                <div class="topBar d-flex align-items-center justify-content-between">
                    <h2 class="text-white my-2">Create a Wedding Invitation</h2>
                    <img src="<?php echo base_url(); ?>assets/frontend/images/logoQR.png" class="img-fluid" alt="">
                </div>
                <div class="card p-4 mb-5">
                    <h1 class="headingEdCard">Enter Details</h1>
                    <div class="showingImgBox shadow px-5 mb-5" style="background-color: #fff; background-image: url('<?php echo base_url(); ?>assets/frontend/images/vector.jpg');">
                        <div class="textBox_one mt-5 mb-3 mx-auto"></div>
                        <div class="textBox_two d-flex align-items-center justify-content-center">Front View</div>
                        <div class="textBox_three mb-5 mt-3 mx-auto"></div>
                    </div>
                  
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <label for="txtLogo" class="form-control px-3 rounded-pill pointer shadhow m-0 text-left">Upload Wedding Logo (Optional)</label>
                                <input type="file" class="invisible position-absolute" id="txtLogo" name="txtLogo" placeholder="" style="top: 0; ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtTitle" name="txtTitle" placeholder="Title of invitation" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <label for="fileColor" class="form-control px-3 rounded-pill pointer shadhow m-0 text-left iconInBg">Select Background Color</label>
                                <input type="color" class="invisible position-absolute" id="fileColor" name="fileColor" placeholder="" style="top: 0; ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtHostName" name="txtHostName" placeholder="Enter Host Name" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                               <textarea class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtDescp" name="txtDescp" placeholder="Enter invitation Description and Venue" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtAAAAAAAAAA" name="txtAAAAAAAAAA" placeholder="Enter Contact Information" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <label for="txtLink" class="form-control px-3 rounded-pill pointer shadhow m-0 text-left iconInBg">Audio mp3 Youtube Link (optional)</label>
                                <input type="file" class="invisible position-absolute" id="txtLink" name="txtLink" placeholder="" style="top: 0; ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">



                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtGoogleMap" name="txtGoogleMap" placeholder="Google map Location Share link" >
                            </div>
                        </div>
                    </div>


                    <button type="button" class="btn edBtn rounded-pill w-100 mt-3">Proceed to next</button>
                </div>
                
            </div>
        </div>
<?php include 'layouts/footer.php'; ?>