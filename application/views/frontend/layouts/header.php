<!DOCTYPE html>
<html>
    <head>
        <title>QR Code</title>
        <link rel="icon" href="<?php echo base_url(); ?>assets/frontend/images/logoQR.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frontend/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('/assets/frontend/css/style.css') ?>">

    </head>
    <body class="bodyPage">