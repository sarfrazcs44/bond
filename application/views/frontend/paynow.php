<?php include 'layouts/header.php'; ?>



        <div class="d-flex align-items-center justify-content-center text-center" >
            <div class="container pb-5">
                <div class="topBar d-flex align-items-center justify-content-between">
                    <h2 class="text-white my-2">Create a Wedding Invitation</h2>
                    <img src="images/logoQR.png" class="img-fluid" alt="">
                </div>
                <div class="card p-4 mb-5 px-5">
                  
                    <div class="paymentDetails pb-4 mb-5 px-3">
                        <div class="d-flex align-items-center justify-content-between">
                            <p class="m-0">Card Design</p>
                            <p class="m-0">2.00 SAR</p>
                        </div>
                        <div class="d-flex align-items-center justify-content-between">
                            <p class="m-0">Privacy</p>
                            <p class="m-0">2.00 SAR</p>
                        </div>
                        <div class="d-flex align-items-center justify-content-between">
                            <p class="m-0">QR Design</p>
                            <p class="m-0">2.00 SAR</p>
                        </div>
                        <div class="d-flex align-items-center justify-content-between">
                            <p class="m-0 headingEdCard">Total Amount</p>
                            <p class="m-0 headingEdCard">2.00 SAR</p>
                        </div>
                    </div>
                    <h1 class="headingEdCard mb-5 text-left">Pay Now</h1>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtCardHolderName" name="txtCardHolderName" placeholder="CardHolderName" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtCardNumber" name="txtCardNumber" placeholder="Card Number" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mt-3 position-relative">
                                <div class="row">
                                    <div class="col-md-6"><input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtExpiry" name="txtExpiry" placeholder="Expiry MM/YY" ></div>
                                    <div class="col-md-6"><input type="text" class="form-control px-3 rounded-pill  shadhow m-0 text-left" id="txtCVV" name="txtCVV" placeholder="CVV" ></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row my-5">
                        <div class="col-md-6">
                            <button type="button" class="btn edBtn rounded-pill w-100 mt-3">Pay Now</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
<?php include 'layouts/footer.php'; ?>

    