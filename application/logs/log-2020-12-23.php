<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2020-12-23 06:14:00 --> Query error: Unknown column 'bu.BoothUserName' in 'field list' - Invalid query: SELECT `orders_requests`.*, `bu`.`UserID` as `BoothID`, `bu`.`CompressedBoothImage` as `BoothImage`, `bu`.`BoothUserName` as `BoothUserName`, `bu`.`Email` as `BoothEmail`, `bu`.`Mobile` as `BoothMobile`, `but`.`BoothName`, `u`.`UserID` as `UserID`, `u`.`CompressedImage` as `UserImage`, `u`.`UserName` as `UserName`, `u`.`Email` as `UserEmail`, `u`.`Mobile` as `UserMobile`, `u`.`Gender` as `UserGender`, `u`.`IsEmailVerified`, `u`.`IsMobileVerified`, `u`.`OnlineStatus`, `ut`.`FullName`, `bu`.`BoothType`, IF('EN' = 'AR', `order_statuses`.`OrderStatusAr`, order_statuses.OrderStatusEn) as OrderStatus, IF('EN' = 'AR', `order_cancellation_reasons`.`CancellationReasonAr`, order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason, `buct`.`Title` as `BoothCityTitle`, `uct`.`Title` as `UserCityTitle`, `orders`.`CreatedAt` as `OrderReceivedAt`, `user_addresses`.`AddressTitle`, `user_addresses`.`RecipientName`, `user_addresses`.`Email` as `AddressEmail`, `user_addresses`.`Mobile` as `AddressMobile`, `user_addresses`.`Gender` as `AddressGender`, `user_addresses`.`ApartmentNo`, `user_addresses`.`Address1`, `user_addresses`.`Address2`, `user_addresses`.`City` as `AddressCity`, `user_addresses`.`Latitude` as `AddressLatitude`, `user_addresses`.`Longitude` as `AddressLongitude`, `user_addresses`.`IsDefault` as `AddressIsDefault`, `user_customization`.`VatPercentage`, `bucc`.`CountryShortName` as `BoothCountryShortName`, `bucc`.`Currency` as `BoothCurrency`, `bucct`.`Title` as `BoothCountryTitle`
FROM `orders_requests`
JOIN `order_statuses` ON `orders_requests`.`OrderStatusID` = `order_statuses`.`OrderStatusID`
LEFT JOIN `order_cancellation_reasons` ON `orders_requests`.`OrderCancellationReasonID` = `order_cancellation_reasons`.`OrderCancellationReasonID`
JOIN `users` `bu` ON `orders_requests`.`BoothID` = `bu`.`UserID`
JOIN `users_text` `but` ON `bu`.`UserID` = `but`.`UserID` AND `but`.`SystemLanguageID` = 1
JOIN `orders` ON `orders_requests`.`OrderID` = `orders`.`OrderID`
JOIN `users` `u` ON `orders`.`UserID` = `u`.`UserID`
JOIN `users_text` `ut` ON `u`.`UserID` = `ut`.`UserID` AND `ut`.`SystemLanguageID` = 1
LEFT JOIN `user_addresses` ON `orders`.`AddressID` = `user_addresses`.`AddressID`
JOIN `user_customization` ON `bu`.`UserID` = `user_customization`.`UserID`
LEFT JOIN `cities` `buc` ON `bu`.`CityID` = `buc`.`CityID`
JOIN `cities_text` `buct` ON `buc`.`CityID` = `buct`.`CityID`
LEFT JOIN `cities` `uc` ON `u`.`CityID` = `uc`.`CityID`
JOIN `cities_text` `uct` ON `uc`.`CityID` = `uct`.`CityID`
LEFT JOIN `countries` `bucc` ON `buc`.`CountryID` = `bucc`.`CountryID`
JOIN `countries_text` `bucct` ON `bucc`.`CountryID` = `bucct`.`CountryID`
JOIN `system_languages` `slbuct` ON `buct`.`SystemLanguageID` = `slbuct`.`SystemLanguageID`
JOIN `system_languages` `slbucct` ON `bucct`.`SystemLanguageID` = `slbucct`.`SystemLanguageID`
JOIN `system_languages` `sluct` ON `uct`.`SystemLanguageID` = `sluct`.`SystemLanguageID`
WHERE `slbuct`.`ShortCode` = 'EN'
AND `slbucct`.`ShortCode` = 'EN'
AND `sluct`.`ShortCode` = 'EN'
AND `orders_requests`.`OrderStatusID` = 1
GROUP BY `orders_requests`.`OrderRequestID`
ORDER BY `orders_requests`.`OrderRequestID` DESC
ERROR - 2020-12-23 06:15:05 --> Query error: Unknown column 'bu.BoothUserName' in 'field list' - Invalid query: SELECT `orders_requests`.*, `bu`.`UserID` as `BoothID`, `bu`.`CompressedBoothImage` as `BoothImage`, `bu`.`BoothUserName` as `BoothUserName`, `bu`.`Email` as `BoothEmail`, `bu`.`Mobile` as `BoothMobile`, `but`.`BoothName`, `u`.`UserID` as `UserID`, `u`.`CompressedImage` as `UserImage`, `u`.`UserName` as `UserName`, `u`.`Email` as `UserEmail`, `u`.`Mobile` as `UserMobile`, `u`.`Gender` as `UserGender`, `u`.`IsEmailVerified`, `u`.`IsMobileVerified`, `u`.`OnlineStatus`, `ut`.`FullName`, `bu`.`BoothType`, IF('EN' = 'AR', `order_statuses`.`OrderStatusAr`, order_statuses.OrderStatusEn) as OrderStatus, IF('EN' = 'AR', `order_cancellation_reasons`.`CancellationReasonAr`, order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason, `buct`.`Title` as `BoothCityTitle`, `uct`.`Title` as `UserCityTitle`, `orders`.`CreatedAt` as `OrderReceivedAt`, `user_addresses`.`AddressTitle`, `user_addresses`.`RecipientName`, `user_addresses`.`Email` as `AddressEmail`, `user_addresses`.`Mobile` as `AddressMobile`, `user_addresses`.`Gender` as `AddressGender`, `user_addresses`.`ApartmentNo`, `user_addresses`.`Address1`, `user_addresses`.`Address2`, `user_addresses`.`City` as `AddressCity`, `user_addresses`.`Latitude` as `AddressLatitude`, `user_addresses`.`Longitude` as `AddressLongitude`, `user_addresses`.`IsDefault` as `AddressIsDefault`, `user_customization`.`VatPercentage`, `bucc`.`CountryShortName` as `BoothCountryShortName`, `bucc`.`Currency` as `BoothCurrency`, `bucct`.`Title` as `BoothCountryTitle`
FROM `orders_requests`
JOIN `order_statuses` ON `orders_requests`.`OrderStatusID` = `order_statuses`.`OrderStatusID`
LEFT JOIN `order_cancellation_reasons` ON `orders_requests`.`OrderCancellationReasonID` = `order_cancellation_reasons`.`OrderCancellationReasonID`
JOIN `users` `bu` ON `orders_requests`.`BoothID` = `bu`.`UserID`
JOIN `users_text` `but` ON `bu`.`UserID` = `but`.`UserID` AND `but`.`SystemLanguageID` = 1
JOIN `orders` ON `orders_requests`.`OrderID` = `orders`.`OrderID`
JOIN `users` `u` ON `orders`.`UserID` = `u`.`UserID`
JOIN `users_text` `ut` ON `u`.`UserID` = `ut`.`UserID` AND `ut`.`SystemLanguageID` = 1
LEFT JOIN `user_addresses` ON `orders`.`AddressID` = `user_addresses`.`AddressID`
JOIN `user_customization` ON `bu`.`UserID` = `user_customization`.`UserID`
LEFT JOIN `cities` `buc` ON `bu`.`CityID` = `buc`.`CityID`
JOIN `cities_text` `buct` ON `buc`.`CityID` = `buct`.`CityID`
LEFT JOIN `cities` `uc` ON `u`.`CityID` = `uc`.`CityID`
JOIN `cities_text` `uct` ON `uc`.`CityID` = `uct`.`CityID`
LEFT JOIN `countries` `bucc` ON `buc`.`CountryID` = `bucc`.`CountryID`
JOIN `countries_text` `bucct` ON `bucc`.`CountryID` = `bucct`.`CountryID`
JOIN `system_languages` `slbuct` ON `buct`.`SystemLanguageID` = `slbuct`.`SystemLanguageID`
JOIN `system_languages` `slbucct` ON `bucct`.`SystemLanguageID` = `slbucct`.`SystemLanguageID`
JOIN `system_languages` `sluct` ON `uct`.`SystemLanguageID` = `sluct`.`SystemLanguageID`
WHERE `slbuct`.`ShortCode` = 'EN'
AND `slbucct`.`ShortCode` = 'EN'
AND `sluct`.`ShortCode` = 'EN'
AND `orders_requests`.`OrderStatusID` = 1
GROUP BY `orders_requests`.`OrderRequestID`
ORDER BY `orders_requests`.`OrderRequestID` DESC
ERROR - 2020-12-23 06:15:07 --> Query error: Unknown column 'bu.BoothUserName' in 'field list' - Invalid query: SELECT `orders_requests`.*, `bu`.`UserID` as `BoothID`, `bu`.`CompressedBoothImage` as `BoothImage`, `bu`.`BoothUserName` as `BoothUserName`, `bu`.`Email` as `BoothEmail`, `bu`.`Mobile` as `BoothMobile`, `but`.`BoothName`, `u`.`UserID` as `UserID`, `u`.`CompressedImage` as `UserImage`, `u`.`UserName` as `UserName`, `u`.`Email` as `UserEmail`, `u`.`Mobile` as `UserMobile`, `u`.`Gender` as `UserGender`, `u`.`IsEmailVerified`, `u`.`IsMobileVerified`, `u`.`OnlineStatus`, `ut`.`FullName`, `bu`.`BoothType`, IF('EN' = 'AR', `order_statuses`.`OrderStatusAr`, order_statuses.OrderStatusEn) as OrderStatus, IF('EN' = 'AR', `order_cancellation_reasons`.`CancellationReasonAr`, order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason, `buct`.`Title` as `BoothCityTitle`, `uct`.`Title` as `UserCityTitle`, `orders`.`CreatedAt` as `OrderReceivedAt`, `user_addresses`.`AddressTitle`, `user_addresses`.`RecipientName`, `user_addresses`.`Email` as `AddressEmail`, `user_addresses`.`Mobile` as `AddressMobile`, `user_addresses`.`Gender` as `AddressGender`, `user_addresses`.`ApartmentNo`, `user_addresses`.`Address1`, `user_addresses`.`Address2`, `user_addresses`.`City` as `AddressCity`, `user_addresses`.`Latitude` as `AddressLatitude`, `user_addresses`.`Longitude` as `AddressLongitude`, `user_addresses`.`IsDefault` as `AddressIsDefault`, `user_customization`.`VatPercentage`, `bucc`.`CountryShortName` as `BoothCountryShortName`, `bucc`.`Currency` as `BoothCurrency`, `bucct`.`Title` as `BoothCountryTitle`
FROM `orders_requests`
JOIN `order_statuses` ON `orders_requests`.`OrderStatusID` = `order_statuses`.`OrderStatusID`
LEFT JOIN `order_cancellation_reasons` ON `orders_requests`.`OrderCancellationReasonID` = `order_cancellation_reasons`.`OrderCancellationReasonID`
JOIN `users` `bu` ON `orders_requests`.`BoothID` = `bu`.`UserID`
JOIN `users_text` `but` ON `bu`.`UserID` = `but`.`UserID` AND `but`.`SystemLanguageID` = 1
JOIN `orders` ON `orders_requests`.`OrderID` = `orders`.`OrderID`
JOIN `users` `u` ON `orders`.`UserID` = `u`.`UserID`
JOIN `users_text` `ut` ON `u`.`UserID` = `ut`.`UserID` AND `ut`.`SystemLanguageID` = 1
LEFT JOIN `user_addresses` ON `orders`.`AddressID` = `user_addresses`.`AddressID`
JOIN `user_customization` ON `bu`.`UserID` = `user_customization`.`UserID`
LEFT JOIN `cities` `buc` ON `bu`.`CityID` = `buc`.`CityID`
JOIN `cities_text` `buct` ON `buc`.`CityID` = `buct`.`CityID`
LEFT JOIN `cities` `uc` ON `u`.`CityID` = `uc`.`CityID`
JOIN `cities_text` `uct` ON `uc`.`CityID` = `uct`.`CityID`
LEFT JOIN `countries` `bucc` ON `buc`.`CountryID` = `bucc`.`CountryID`
JOIN `countries_text` `bucct` ON `bucc`.`CountryID` = `bucct`.`CountryID`
JOIN `system_languages` `slbuct` ON `buct`.`SystemLanguageID` = `slbuct`.`SystemLanguageID`
JOIN `system_languages` `slbucct` ON `bucct`.`SystemLanguageID` = `slbucct`.`SystemLanguageID`
JOIN `system_languages` `sluct` ON `uct`.`SystemLanguageID` = `sluct`.`SystemLanguageID`
WHERE `slbuct`.`ShortCode` = 'EN'
AND `slbucct`.`ShortCode` = 'EN'
AND `sluct`.`ShortCode` = 'EN'
AND `orders_requests`.`OrderStatusID` = 1
GROUP BY `orders_requests`.`OrderRequestID`
ORDER BY `orders_requests`.`OrderRequestID` DESC
ERROR - 2020-12-23 06:15:16 --> Query error: Unknown column 'bu.BoothUserName' in 'field list' - Invalid query: SELECT `orders_requests`.*, `bu`.`UserID` as `BoothID`, `bu`.`CompressedBoothImage` as `BoothImage`, `bu`.`BoothUserName` as `BoothUserName`, `bu`.`Email` as `BoothEmail`, `bu`.`Mobile` as `BoothMobile`, `but`.`BoothName`, `u`.`UserID` as `UserID`, `u`.`CompressedImage` as `UserImage`, `u`.`UserName` as `UserName`, `u`.`Email` as `UserEmail`, `u`.`Mobile` as `UserMobile`, `u`.`Gender` as `UserGender`, `u`.`IsEmailVerified`, `u`.`IsMobileVerified`, `u`.`OnlineStatus`, `ut`.`FullName`, `bu`.`BoothType`, IF('EN' = 'AR', `order_statuses`.`OrderStatusAr`, order_statuses.OrderStatusEn) as OrderStatus, IF('EN' = 'AR', `order_cancellation_reasons`.`CancellationReasonAr`, order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason, `buct`.`Title` as `BoothCityTitle`, `uct`.`Title` as `UserCityTitle`, `orders`.`CreatedAt` as `OrderReceivedAt`, `user_addresses`.`AddressTitle`, `user_addresses`.`RecipientName`, `user_addresses`.`Email` as `AddressEmail`, `user_addresses`.`Mobile` as `AddressMobile`, `user_addresses`.`Gender` as `AddressGender`, `user_addresses`.`ApartmentNo`, `user_addresses`.`Address1`, `user_addresses`.`Address2`, `user_addresses`.`City` as `AddressCity`, `user_addresses`.`Latitude` as `AddressLatitude`, `user_addresses`.`Longitude` as `AddressLongitude`, `user_addresses`.`IsDefault` as `AddressIsDefault`, `user_customization`.`VatPercentage`, `bucc`.`CountryShortName` as `BoothCountryShortName`, `bucc`.`Currency` as `BoothCurrency`, `bucct`.`Title` as `BoothCountryTitle`
FROM `orders_requests`
JOIN `order_statuses` ON `orders_requests`.`OrderStatusID` = `order_statuses`.`OrderStatusID`
LEFT JOIN `order_cancellation_reasons` ON `orders_requests`.`OrderCancellationReasonID` = `order_cancellation_reasons`.`OrderCancellationReasonID`
JOIN `users` `bu` ON `orders_requests`.`BoothID` = `bu`.`UserID`
JOIN `users_text` `but` ON `bu`.`UserID` = `but`.`UserID` AND `but`.`SystemLanguageID` = 1
JOIN `orders` ON `orders_requests`.`OrderID` = `orders`.`OrderID`
JOIN `users` `u` ON `orders`.`UserID` = `u`.`UserID`
JOIN `users_text` `ut` ON `u`.`UserID` = `ut`.`UserID` AND `ut`.`SystemLanguageID` = 1
LEFT JOIN `user_addresses` ON `orders`.`AddressID` = `user_addresses`.`AddressID`
JOIN `user_customization` ON `bu`.`UserID` = `user_customization`.`UserID`
LEFT JOIN `cities` `buc` ON `bu`.`CityID` = `buc`.`CityID`
JOIN `cities_text` `buct` ON `buc`.`CityID` = `buct`.`CityID`
LEFT JOIN `cities` `uc` ON `u`.`CityID` = `uc`.`CityID`
JOIN `cities_text` `uct` ON `uc`.`CityID` = `uct`.`CityID`
LEFT JOIN `countries` `bucc` ON `buc`.`CountryID` = `bucc`.`CountryID`
JOIN `countries_text` `bucct` ON `bucc`.`CountryID` = `bucct`.`CountryID`
JOIN `system_languages` `slbuct` ON `buct`.`SystemLanguageID` = `slbuct`.`SystemLanguageID`
JOIN `system_languages` `slbucct` ON `bucct`.`SystemLanguageID` = `slbucct`.`SystemLanguageID`
JOIN `system_languages` `sluct` ON `uct`.`SystemLanguageID` = `sluct`.`SystemLanguageID`
WHERE `slbuct`.`ShortCode` = 'EN'
AND `slbucct`.`ShortCode` = 'EN'
AND `sluct`.`ShortCode` = 'EN'
AND `orders_requests`.`OrderStatusID` = 1
GROUP BY `orders_requests`.`OrderRequestID`
ORDER BY `orders_requests`.`OrderRequestID` DESC
ERROR - 2020-12-23 06:17:00 --> Query error: Unknown column 'bu.CompressedBoothImage as BoothImage bu.Email' in 'field list' - Invalid query: SELECT `orders_requests`.*, `bu`.`UserID` as `BoothID`, `bu`.`CompressedBoothImage as BoothImage bu`.`Email` as `BoothEmail`, `bu`.`Mobile` as `BoothMobile`, `but`.`BoothName`, `u`.`UserID` as `UserID`, `u`.`CompressedImage` as `UserImage`, `u`.`UserName` as `UserName`, `u`.`Email` as `UserEmail`, `u`.`Mobile` as `UserMobile`, `u`.`Gender` as `UserGender`, `u`.`IsEmailVerified`, `u`.`IsMobileVerified`, `u`.`OnlineStatus`, `u`.`FullName`, `bu`.`BoothType`, IF('EN' = 'AR', `order_statuses`.`OrderStatusAr`, order_statuses.OrderStatusEn) as OrderStatus, IF('EN' = 'AR', `order_cancellation_reasons`.`CancellationReasonAr`, order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason, `buct`.`Title` as `BoothCityTitle`, `uct`.`Title` as `UserCityTitle`, `orders`.`CreatedAt` as `OrderReceivedAt`, `user_addresses`.`AddressTitle`, `user_addresses`.`RecipientName`, `user_addresses`.`Email` as `AddressEmail`, `user_addresses`.`Mobile` as `AddressMobile`, `user_addresses`.`Gender` as `AddressGender`, `user_addresses`.`ApartmentNo`, `user_addresses`.`Address1`, `user_addresses`.`Address2`, `user_addresses`.`City` as `AddressCity`, `user_addresses`.`Latitude` as `AddressLatitude`, `user_addresses`.`Longitude` as `AddressLongitude`, `user_addresses`.`IsDefault` as `AddressIsDefault`, `user_customization`.`VatPercentage`, `bucc`.`CountryShortName` as `BoothCountryShortName`, `bucc`.`Currency` as `BoothCurrency`, `bucct`.`Title` as `BoothCountryTitle`
FROM `orders_requests`
JOIN `order_statuses` ON `orders_requests`.`OrderStatusID` = `order_statuses`.`OrderStatusID`
LEFT JOIN `order_cancellation_reasons` ON `orders_requests`.`OrderCancellationReasonID` = `order_cancellation_reasons`.`OrderCancellationReasonID`
JOIN `users` `bu` ON `orders_requests`.`BoothID` = `bu`.`UserID`
JOIN `users_text` `but` ON `bu`.`UserID` = `but`.`UserID` AND `but`.`SystemLanguageID` = 1
JOIN `orders` ON `orders_requests`.`OrderID` = `orders`.`OrderID`
JOIN `users` `u` ON `orders`.`UserID` = `u`.`UserID`
JOIN `users_text` `ut` ON `u`.`UserID` = `ut`.`UserID` AND `ut`.`SystemLanguageID` = 1
LEFT JOIN `user_addresses` ON `orders`.`AddressID` = `user_addresses`.`AddressID`
JOIN `user_customization` ON `bu`.`UserID` = `user_customization`.`UserID`
LEFT JOIN `cities` `buc` ON `bu`.`CityID` = `buc`.`CityID`
JOIN `cities_text` `buct` ON `buc`.`CityID` = `buct`.`CityID`
LEFT JOIN `cities` `uc` ON `u`.`CityID` = `uc`.`CityID`
JOIN `cities_text` `uct` ON `uc`.`CityID` = `uct`.`CityID`
LEFT JOIN `countries` `bucc` ON `buc`.`CountryID` = `bucc`.`CountryID`
JOIN `countries_text` `bucct` ON `bucc`.`CountryID` = `bucct`.`CountryID`
JOIN `system_languages` `slbuct` ON `buct`.`SystemLanguageID` = `slbuct`.`SystemLanguageID`
JOIN `system_languages` `slbucct` ON `bucct`.`SystemLanguageID` = `slbucct`.`SystemLanguageID`
JOIN `system_languages` `sluct` ON `uct`.`SystemLanguageID` = `sluct`.`SystemLanguageID`
WHERE `slbuct`.`ShortCode` = 'EN'
AND `slbucct`.`ShortCode` = 'EN'
AND `sluct`.`ShortCode` = 'EN'
AND `orders_requests`.`OrderStatusID` = 1
GROUP BY `orders_requests`.`OrderRequestID`
ORDER BY `orders_requests`.`OrderRequestID` DESC
ERROR - 2020-12-23 06:18:49 --> Query error: Unknown column 'bu.CompressedBoothImage as BoothImage bu.Email' in 'field list' - Invalid query: SELECT `orders_requests`.*, `bu`.`UserID` as `BoothID`, `bu`.`CompressedBoothImage as BoothImage bu`.`Email` as `BoothEmail`, `bu`.`Mobile` as `BoothMobile`, `but`.`BoothName`, `u`.`UserID` as `UserID`, `u`.`CompressedImage` as `UserImage`, `u`.`UserName` as `UserName`, `u`.`Email` as `UserEmail`, `u`.`Mobile` as `UserMobile`, `u`.`Gender` as `UserGender`, `u`.`IsEmailVerified`, `u`.`IsMobileVerified`, `u`.`OnlineStatus`, `u`.`FullName`, IF('EN' = 'AR', `order_statuses`.`OrderStatusAr`, order_statuses.OrderStatusEn) as OrderStatus, IF('EN' = 'AR', `order_cancellation_reasons`.`CancellationReasonAr`, order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason, `buct`.`Title` as `BoothCityTitle`, `uct`.`Title` as `UserCityTitle`, `orders`.`CreatedAt` as `OrderReceivedAt`, `user_addresses`.`AddressTitle`, `user_addresses`.`RecipientName`, `user_addresses`.`Email` as `AddressEmail`, `user_addresses`.`Mobile` as `AddressMobile`, `user_addresses`.`Gender` as `AddressGender`, `user_addresses`.`ApartmentNo`, `user_addresses`.`Address1`, `user_addresses`.`Address2`, `user_addresses`.`City` as `AddressCity`, `user_addresses`.`Latitude` as `AddressLatitude`, `user_addresses`.`Longitude` as `AddressLongitude`, `user_addresses`.`IsDefault` as `AddressIsDefault`, `user_customization`.`VatPercentage`, `bucc`.`CountryShortName` as `BoothCountryShortName`, `bucc`.`Currency` as `BoothCurrency`, `bucct`.`Title` as `BoothCountryTitle`
FROM `orders_requests`
JOIN `order_statuses` ON `orders_requests`.`OrderStatusID` = `order_statuses`.`OrderStatusID`
LEFT JOIN `order_cancellation_reasons` ON `orders_requests`.`OrderCancellationReasonID` = `order_cancellation_reasons`.`OrderCancellationReasonID`
JOIN `users` `bu` ON `orders_requests`.`BoothID` = `bu`.`UserID`
JOIN `users_text` `but` ON `bu`.`UserID` = `but`.`UserID` AND `but`.`SystemLanguageID` = 1
JOIN `orders` ON `orders_requests`.`OrderID` = `orders`.`OrderID`
JOIN `users` `u` ON `orders`.`UserID` = `u`.`UserID`
JOIN `users_text` `ut` ON `u`.`UserID` = `ut`.`UserID` AND `ut`.`SystemLanguageID` = 1
LEFT JOIN `user_addresses` ON `orders`.`AddressID` = `user_addresses`.`AddressID`
JOIN `user_customization` ON `bu`.`UserID` = `user_customization`.`UserID`
LEFT JOIN `cities` `buc` ON `bu`.`CityID` = `buc`.`CityID`
JOIN `cities_text` `buct` ON `buc`.`CityID` = `buct`.`CityID`
LEFT JOIN `cities` `uc` ON `u`.`CityID` = `uc`.`CityID`
JOIN `cities_text` `uct` ON `uc`.`CityID` = `uct`.`CityID`
LEFT JOIN `countries` `bucc` ON `buc`.`CountryID` = `bucc`.`CountryID`
JOIN `countries_text` `bucct` ON `bucc`.`CountryID` = `bucct`.`CountryID`
JOIN `system_languages` `slbuct` ON `buct`.`SystemLanguageID` = `slbuct`.`SystemLanguageID`
JOIN `system_languages` `slbucct` ON `bucct`.`SystemLanguageID` = `slbucct`.`SystemLanguageID`
JOIN `system_languages` `sluct` ON `uct`.`SystemLanguageID` = `sluct`.`SystemLanguageID`
WHERE `slbuct`.`ShortCode` = 'EN'
AND `slbucct`.`ShortCode` = 'EN'
AND `sluct`.`ShortCode` = 'EN'
AND `orders_requests`.`OrderStatusID` = 1
GROUP BY `orders_requests`.`OrderRequestID`
ORDER BY `orders_requests`.`OrderRequestID` DESC
ERROR - 2020-12-23 06:20:16 --> Query error: Unknown column 'bu.CompressedBoothImage as BoothImage bu.Email' in 'field list' - Invalid query: SELECT `orders_requests`.*, `bu`.`UserID` as `BoothID`, `bu`.`CompressedBoothImage as BoothImage bu`.`Email` as `BoothEmail`, `bu`.`Mobile` as `BoothMobile`, `but`.`BoothName`, `u`.`UserID` as `UserID`, `u`.`CompressedImage` as `UserImage`, `u`.`UserName` as `UserName`, `u`.`Email` as `UserEmail`, `u`.`Mobile` as `UserMobile`, `u`.`Gender` as `UserGender`, `u`.`IsEmailVerified`, `u`.`IsMobileVerified`, `u`.`OnlineStatus`, `u`.`FullName`, IF('EN' = 'AR', `order_statuses`.`OrderStatusAr`, order_statuses.OrderStatusEn) as OrderStatus, IF('EN' = 'AR', `order_cancellation_reasons`.`CancellationReasonAr`, order_cancellation_reasons.CancellationReasonEn) as OrderCancellationReason, `buct`.`Title` as `BoothCityTitle`, `uct`.`Title` as `UserCityTitle`, `orders`.`CreatedAt` as `OrderReceivedAt`, `user_addresses`.`AddressTitle`, `user_addresses`.`RecipientName`, `user_addresses`.`Email` as `AddressEmail`, `user_addresses`.`Mobile` as `AddressMobile`, `user_addresses`.`Gender` as `AddressGender`, `user_addresses`.`ApartmentNo`, `user_addresses`.`Address1`, `user_addresses`.`Address2`, `user_addresses`.`City` as `AddressCity`, `user_addresses`.`Latitude` as `AddressLatitude`, `user_addresses`.`Longitude` as `AddressLongitude`, `user_addresses`.`IsDefault` as `AddressIsDefault`, `user_customization`.`VatPercentage`, `bucc`.`CountryShortName` as `BoothCountryShortName`, `bucc`.`Currency` as `BoothCurrency`, `bucct`.`Title` as `BoothCountryTitle`
FROM `orders_requests`
JOIN `order_statuses` ON `orders_requests`.`OrderStatusID` = `order_statuses`.`OrderStatusID`
LEFT JOIN `order_cancellation_reasons` ON `orders_requests`.`OrderCancellationReasonID` = `order_cancellation_reasons`.`OrderCancellationReasonID`
LEFT JOIN `users` `bu` ON `orders_requests`.`BoothID` = `bu`.`UserID`
JOIN `users_text` `but` ON `bu`.`UserID` = `but`.`UserID` AND `but`.`SystemLanguageID` = 1
JOIN `orders` ON `orders_requests`.`OrderID` = `orders`.`OrderID`
JOIN `users` `u` ON `orders`.`UserID` = `u`.`UserID`
JOIN `users_text` `ut` ON `u`.`UserID` = `ut`.`UserID` AND `ut`.`SystemLanguageID` = 1
LEFT JOIN `user_addresses` ON `orders`.`AddressID` = `user_addresses`.`AddressID`
JOIN `user_customization` ON `bu`.`UserID` = `user_customization`.`UserID`
LEFT JOIN `cities` `buc` ON `bu`.`CityID` = `buc`.`CityID`
JOIN `cities_text` `buct` ON `buc`.`CityID` = `buct`.`CityID`
LEFT JOIN `cities` `uc` ON `u`.`CityID` = `uc`.`CityID`
JOIN `cities_text` `uct` ON `uc`.`CityID` = `uct`.`CityID`
LEFT JOIN `countries` `bucc` ON `buc`.`CountryID` = `bucc`.`CountryID`
JOIN `countries_text` `bucct` ON `bucc`.`CountryID` = `bucct`.`CountryID`
JOIN `system_languages` `slbuct` ON `buct`.`SystemLanguageID` = `slbuct`.`SystemLanguageID`
JOIN `system_languages` `slbucct` ON `bucct`.`SystemLanguageID` = `slbucct`.`SystemLanguageID`
JOIN `system_languages` `sluct` ON `uct`.`SystemLanguageID` = `sluct`.`SystemLanguageID`
WHERE `slbuct`.`ShortCode` = 'EN'
AND `slbucct`.`ShortCode` = 'EN'
AND `sluct`.`ShortCode` = 'EN'
AND `orders_requests`.`OrderStatusID` = 1
GROUP BY `orders_requests`.`OrderRequestID`
ORDER BY `orders_requests`.`OrderRequestID` DESC
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Trying to get property 'ModuleID' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 22
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Trying to get property 'Title' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 43
ERROR - 2020-12-23 07:01:12 --> Could not find the language line "title_ar"
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Trying to get property 'TitleAr' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 49
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Trying to get property 'Slug' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 63
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Trying to get property 'IconClass' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 69
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Undefined variable: key /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 82
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Undefined variable: key /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 93
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Undefined variable: key /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 103
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Undefined variable: key /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 113
ERROR - 2020-12-23 07:01:12 --> Severity: Notice --> Undefined variable: key /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 123
ERROR - 2020-12-23 07:02:51 --> Could not find the language line "title_ar"
ERROR - 2020-12-23 07:02:51 --> Severity: Notice --> Undefined variable: key /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 82
ERROR - 2020-12-23 07:02:51 --> Severity: error --> Exception: Cannot use object of type Module_model as array /Applications/MAMP/htdocs/boaty/application/views/backend/module/edit.php 82
ERROR - 2020-12-23 07:03:49 --> Could not find the language line "title_ar"
ERROR - 2020-12-23 07:04:03 --> Could not find the language line "title_ar"
ERROR - 2020-12-23 07:04:32 --> Could not find the language line "title_ar"
ERROR - 2020-12-23 07:05:07 --> Could not find the language line "title_ar"
ERROR - 2020-12-23 07:36:10 --> Query error: Table 'boaty.modules_text' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_text`
ERROR - 2020-12-23 07:36:45 --> Query error: Table 'boaty.modules_text' doesn't exist - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules_text`.`Title` as `ModuleTitle`, `users`.`UserID`, `users_text`.`FullName` as `UserTitle`, `modules_users_rights`.*
FROM `modules`
JOIN `modules_text` ON `modules_text`.`ModuleID` = `modules`.`ModuleID`
JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `modules_text`.`SystemLanguageID`
JOIN `modules_users_rights` ON `modules`.`ModuleID` = `modules_users_rights`.`ModuleID`
JOIN `users` ON `users`.`UserID` = `modules_users_rights`.`UserID`
JOIN `users_text` ON `users`.`UserID` = `users_text`.`UserID`
WHERE `system_languages`.`ShortCode` = 'EN'
AND `modules`.`Hide` = '0'
AND `users`.`UserID` = '1'
AND `modules`.`ParentID` =0 AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:39:46 --> Query error: Unknown column 'modules_text.Title' in 'field list' - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules_text`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `system_languages`.`ShortCode` = 'EN'
AND `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `modules`.`ParentID` =0 AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:40:11 --> Query error: Unknown column 'system_languages.ShortCode' in 'where clause' - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `system_languages`.`ShortCode` = 'EN'
AND `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `modules`.`ParentID` =0 AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:41:29 --> Query error: Unknown column 'EN' in 'where clause' - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `EN` IS NULL
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:42:05 --> Query error: Unknown column 'EN' in 'where clause' - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `EN` IS NULL
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:42:06 --> Query error: Unknown column 'EN' in 'where clause' - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `EN` IS NULL
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:42:07 --> Query error: Unknown column 'EN' in 'where clause' - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `EN` IS NULL
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:42:17 --> Query error: Unknown column 'EN' in 'where clause' - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `EN` IS NULL
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:44:16 --> Severity: error --> Exception: Cannot use object of type stdClass as array /Applications/MAMP/htdocs/boaty/application/views/backend/layouts/sidebar.php 64
ERROR - 2020-12-23 07:44:21 --> Severity: error --> Exception: Cannot use object of type stdClass as array /Applications/MAMP/htdocs/boaty/application/views/backend/layouts/sidebar.php 64
ERROR - 2020-12-23 07:45:18 --> Severity: Notice --> Trying to get property 'ModuleID' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/layouts/sidebar.php 68
ERROR - 2020-12-23 07:45:18 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`S' at line 7 - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `modules`.`ParentID` =  AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:46:07 --> Severity: Notice --> Trying to get property 'ModuleID' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/layouts/sidebar.php 68
ERROR - 2020-12-23 07:46:07 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`S' at line 7 - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `modules`.`ParentID` =  AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:46:08 --> Severity: Notice --> Trying to get property 'ModuleID' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/layouts/sidebar.php 68
ERROR - 2020-12-23 07:46:08 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`S' at line 7 - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `modules`.`ParentID` =  AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:46:10 --> Severity: Notice --> Trying to get property 'ModuleID' of non-object /Applications/MAMP/htdocs/boaty/application/views/backend/layouts/sidebar.php 68
ERROR - 2020-12-23 07:46:10 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`S' at line 7 - Invalid query: SELECT `modules`.`ModuleID`, `modules`.`Slug`, `modules`.`IconClass`, `modules`.`ParentID`, `modules`.`Title` as `ModuleTitle`, `roles`.`RoleID`, `roles`.`Title` as `RoleTitle`, `modules_rights`.*
FROM `modules`
JOIN `modules_rights` ON `modules`.`ModuleID` = `modules_rights`.`ModuleID`
JOIN `roles` ON `roles`.`RoleID` = `modules_rights`.`RoleID`
WHERE `modules`.`Hide` = '0'
AND `roles`.`RoleID` = '1'
AND `modules`.`ParentID` =  AND `modules`.`IsActive` = 1
GROUP BY `modules`.`ModuleID`
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:47:58 --> Query error: Table 'boaty.modules_text' doesn't exist - Invalid query: SELECT *
FROM `modules_text`
WHERE `Title` = 'Orders'
 LIMIT 1
ERROR - 2020-12-23 07:48:03 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 07:48:06 --> Query error: Table 'boaty.modules_text' doesn't exist - Invalid query: SELECT *
FROM `modules_text`
WHERE `Title` = 'Orders'
 LIMIT 1
ERROR - 2020-12-23 07:49:58 --> Query error: Table 'boaty.modules_text' doesn't exist - Invalid query: SELECT `modules`.*, `modules_text`.*
FROM `modules`
JOIN `modules_text` ON `modules`.`ModuleID` = `modules_text`.`ModuleID`
LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `modules_text`.`SystemLanguageID`
WHERE `modules`.`ModuleID` = 46
AND `system_languages`.`Hide` = 0
ORDER BY `system_languages`.`IsDefault` DESC
ERROR - 2020-12-23 07:50:02 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 07:50:04 --> Query error: Table 'boaty.modules_text' doesn't exist - Invalid query: SELECT `modules`.*, `modules_text`.*
FROM `modules`
JOIN `modules_text` ON `modules`.`ModuleID` = `modules_text`.`ModuleID`
LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `modules_text`.`SystemLanguageID`
WHERE `modules`.`ModuleID` = 46
AND `system_languages`.`Hide` = 0
ORDER BY `system_languages`.`IsDefault` DESC
ERROR - 2020-12-23 07:56:24 --> Query error: Table 'boaty.modules_text' doesn't exist - Invalid query: SELECT `modules`.*, `modules_text`.*
FROM `modules`
JOIN `modules_text` ON `modules`.`ModuleID` = `modules_text`.`ModuleID`
JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `modules_text`.`SystemLanguageID`
WHERE `system_languages`.`ShortCode` = 'EN'
AND `modules`.`Hide` = '0'
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:57:10 --> Query error: Table 'boaty.modules_text' doesn't exist - Invalid query: SELECT `modules`.*, `modules_text`.*
FROM `modules`
JOIN `modules_text` ON `modules`.`ModuleID` = `modules_text`.`ModuleID`
JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `modules_text`.`SystemLanguageID`
WHERE `system_languages`.`ShortCode` = 'EN'
AND `modules`.`ParentID` =0
AND `modules`.`Hide` = '0'
ORDER BY `modules`.`SortOrder` ASC
ERROR - 2020-12-23 07:58:28 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 07:58:33 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 07:58:38 --> Severity: Notice --> Undefined index: Child_model /Applications/MAMP/htdocs/boaty/application/controllers/cms/Test.php 33
ERROR - 2020-12-23 07:59:52 --> 404 Page Not Found: cms/Test/manage
ERROR - 2020-12-23 08:00:58 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:01:55 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:03:26 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:03:36 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:03:39 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:07:21 --> Severity: Notice --> Undefined variable: result /Applications/MAMP/htdocs/boaty/application/controllers/cms/Module.php 821
ERROR - 2020-12-23 08:07:21 --> Severity: Notice --> Trying to get property 'DescriptionAr' of non-object /Applications/MAMP/htdocs/boaty/application/controllers/cms/Module.php 821
ERROR - 2020-12-23 08:07:25 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:07:42 --> Severity: Notice --> Undefined variable: result /Applications/MAMP/htdocs/boaty/application/controllers/cms/Module.php 821
ERROR - 2020-12-23 08:07:42 --> Severity: Notice --> Trying to get property 'DescriptionAr' of non-object /Applications/MAMP/htdocs/boaty/application/controllers/cms/Module.php 821
ERROR - 2020-12-23 08:08:05 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:08:35 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:09:28 --> 404 Page Not Found: cms/Mode/index
ERROR - 2020-12-23 08:09:33 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:09:36 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:09:56 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:10:17 --> Query error: Unknown column 'MQ=' in 'where clause' - Invalid query: SELECT *
FROM `stors`
WHERE `MQ=` = '1'
ERROR - 2020-12-23 08:10:20 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:10:24 --> Query error: Unknown column 'MQ=' in 'where clause' - Invalid query: SELECT *
FROM `stors`
WHERE `MQ=` = '1'
ERROR - 2020-12-23 08:11:16 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:11:26 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:11:29 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:11:42 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:11:47 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:13:11 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:13:18 --> Query error: Unknown column 'Mw=' in 'where clause' - Invalid query: SELECT *
FROM `tests`
WHERE `Mw=` = '3'
ERROR - 2020-12-23 08:15:54 --> Query error: Unknown column 'Mw=' in 'where clause' - Invalid query: SELECT *
FROM `tests`
WHERE `Mw=` = '3'
ERROR - 2020-12-23 08:16:46 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:17:08 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:17:20 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:17:23 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:17:25 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:17:32 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:25:30 --> Query error: Table 'boaty.system_languages' doesn't exist - Invalid query: SHOW COLUMNS FROM `system_languages`
ERROR - 2020-12-23 08:25:31 --> Query error: Table 'boaty.system_languages' doesn't exist - Invalid query: SHOW COLUMNS FROM `system_languages`
ERROR - 2020-12-23 08:25:35 --> Query error: Table 'boaty.system_languages' doesn't exist - Invalid query: SHOW COLUMNS FROM `system_languages`
ERROR - 2020-12-23 08:25:36 --> Query error: Table 'boaty.system_languages' doesn't exist - Invalid query: SHOW COLUMNS FROM `system_languages`
ERROR - 2020-12-23 08:30:17 --> Severity: error --> Exception: Unable to locate the model you have specified: System_language_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:30:52 --> Severity: error --> Exception: Unable to locate the model you have specified: System_language_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:32:17 --> Severity: error --> Exception: Unable to locate the model you have specified: System_language_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:33:58 --> Severity: error --> Exception: Unable to locate the model you have specified: System_language_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:35:24 --> Severity: error --> Exception: Unable to locate the model you have specified: System_language_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:36:12 --> Severity: error --> Exception: Unable to locate the model you have specified: System_language_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:36:50 --> Severity: error --> Exception: Unable to locate the model you have specified: Dashboard_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:37:24 --> Severity: error --> Exception: Unable to locate the model you have specified: User_text_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:41:28 --> Severity: Notice --> Undefined index: Child_model /Applications/MAMP/htdocs/boaty/application/controllers/cms/User.php 37
ERROR - 2020-12-23 08:41:28 --> Severity: error --> Exception: Unable to locate the model you have specified: Booking_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:44:54 --> Severity: error --> Exception: Unable to locate the model you have specified: Booking_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:46:44 --> Severity: error --> Exception: Unable to locate the model you have specified: Booking_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 08:47:50 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:48:24 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:48:32 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:48:45 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:48:46 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:49:44 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:49:49 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:50:29 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:50:53 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:53:48 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 08:54:00 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:56:28 --> Query error: Table 'boaty.modules_users_rights' doesn't exist - Invalid query: SHOW COLUMNS FROM `modules_users_rights`
ERROR - 2020-12-23 08:57:20 --> Severity: error --> Exception: Cannot use object of type User_model as array /Applications/MAMP/htdocs/boaty/application/views/backend/user/edit.php 5
ERROR - 2020-12-23 08:59:52 --> Severity: Notice --> Undefined property: User::$Title /Applications/MAMP/htdocs/boaty/system/core/Model.php 73
ERROR - 2020-12-23 08:59:53 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:00:06 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:00:50 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:00:52 --> Severity: Notice --> Undefined index: Child_model /Applications/MAMP/htdocs/boaty/application/controllers/cms/User.php 234
ERROR - 2020-12-23 09:00:52 --> Query error: Table 'boaty.users_text' doesn't exist - Invalid query: SELECT `users`.*, `users_text`.*
FROM `users`
JOIN `users_text` ON `users`.`UserID` = `users_text`.`UserID`
LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `users_text`.`SystemLanguageID`
WHERE `users`.`UserID` = 132
AND `system_languages`.`Hide` = 0
ORDER BY `system_languages`.`IsDefault` DESC
ERROR - 2020-12-23 09:01:48 --> Query error: Table 'boaty.users_text' doesn't exist - Invalid query: SELECT `users`.*, `users_text`.*
FROM `users`
JOIN `users_text` ON `users`.`UserID` = `users_text`.`UserID`
LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `users_text`.`SystemLanguageID`
WHERE `users`.`UserID` = 132
AND `system_languages`.`Hide` = 0
ORDER BY `system_languages`.`IsDefault` DESC
ERROR - 2020-12-23 09:02:20 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:02:24 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:02:43 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:03:24 --> Severity: error --> Exception: Unable to locate the model you have specified: Booking_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
ERROR - 2020-12-23 09:03:37 --> Severity: Notice --> Undefined index: result_count /Applications/MAMP/htdocs/boaty/application/views/backend/layouts/sidebar.php 194
ERROR - 2020-12-23 09:03:37 --> Severity: Notice --> Undefined index: result /Applications/MAMP/htdocs/boaty/application/views/backend/layouts/sidebar.php 206
ERROR - 2020-12-23 09:03:38 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:04:36 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:04:45 --> 404 Page Not Found: Assets/backend
ERROR - 2020-12-23 09:05:00 --> Severity: error --> Exception: Unable to locate the model you have specified: Page_text_model /Applications/MAMP/htdocs/boaty/system/core/Loader.php 348
